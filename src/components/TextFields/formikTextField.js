import React from 'react';
import {
    Formik,
    ErrorMessage,
    Field,
    Form,
    useField,
    FieldAttributes,
    FieldArray
} from "formik";


export const MyTextField = ({ label, ...props }) => {
    const [field, meta] = useField(props);
    return (
        <div className="mb-2">
            <label className="label" htmlFor={field.name}>{label}</label>
            <input className={`form-control shadow-none  ${meta.touched && meta.error && 'is-invalid'}`}
            {...field}
            {...props}
            autoComplete="off"></input>
            <ErrorMessage component="div" className="error" name={field.name}></ErrorMessage>
        </div >
    );
}
