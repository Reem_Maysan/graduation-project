import React from 'react';
import {
    Formik,
    ErrorMessage,
    Field,
    Form,
    useField,
    FieldAttributes,
    FieldArray
} from "formik"; 

export const CustomTextField = ({ placeholder,as, ...props }) => {
    const [field, meta] = useField(props);
    const errorText = meta.error && meta.touched ? meta.error : "";
//  console.log('CustomTextField')
  //  console.log(props)
 //   console.log(meta)
    return (
        <div >
            <Field
                autoComplete="off"
                className={`form-control shadow-none ${meta.touched && meta.error && 'is-invalid'}`}
                placeholder={placeholder}
                
                {...field}
                as={as}
            //helperText={errorText}
            // error={!!errorText} 
            />
            <div className="form-row r">
            <ErrorMessage component="div" name={field.name} className="error" />
            </div>
        </div>
    )
}
/* <label className="label-dis" htmlFor={field.name}>{label}</label>
            <input
                className={`form-control shadow-none ${meta.touched && meta.error && 'is-invalid'}`}
                {...field} {...props}
                autoComplete="off"
                defaultValue={field.value}
            />
            <ErrorMessage component="div" name={field.name} className="error" />
         */