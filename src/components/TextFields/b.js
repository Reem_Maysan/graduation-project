export const Basic_Url = "https://thawing-everglades-11191.herokuapp.com/api/v1/"


//////////////////////////ADD///////////////////////////
export const ADD_PersonalInfo = Basic_Url + "addForm/personalInfo";
export const ADD_HealthyInfo = Basic_Url + "addForm/healthyInfo";
export const ADD_MonthlySalaryInfo = Basic_Url + "addForm/monthlySalary";
export const ADD_ResidenceInfo = Basic_Url + "addForm/residenceInfo";
export const ADD_DeptsInfo = Basic_Url + "addForm/deptsInfo";
//export const ADD_ExpensesInfo = Basic_Url + "getForm/getExpensesInfo/";
//export const ADD_RebuildingInfo = Basic_Url + "getForm/editRebuildingInfo/";
export const ADD_ChildInfo = Basic_Url + "addForm/childrenInfo";
export const ADD_WifeInfo = Basic_Url + "addForm/wifeInfo";
export const ADD_WareHouses = Basic_Url + "warehouseDep/warehouses/addWarehouse";


//////////////////////////GET///////////////////////////
export const GET_PersonalInfo = Basic_Url + "getForm/getPersonalInfo/";
export const GET_HealthyInfo = Basic_Url + "getForm/getHealthyInfo/";
export const GET_MonthlySalaryInfo = Basic_Url + "getForm/getMonthlySalary/";
export const GET_ResidenceInfo = Basic_Url + "getForm/getResidenceInfo/";
export const GET_DeptsInfo = Basic_Url + "getForm/getDeptsInfo/";
export const GET_ExpensesInfo = Basic_Url + "getForm/getExpensesInfo/";
export const GET_RebuildingInfo = Basic_Url + "getForm/getRebuildingInfo/";
export const GET_ChildInfo = Basic_Url + "getForm/getChildrenInfo/";
export const GET_WifeInfo = Basic_Url + "getForm//getWifeInfo/";
export const GET_WivesInfo = Basic_Url + "getForm/getWives/";
export const GET_RejectedForms = Basic_Url + "getForm/getRejectedForms";
export const GET_AssociationForms = Basic_Url + "getForm/getAssociationForms";
export const GET_ALL_Warehouses = Basic_Url + "warehouseDep/warehouses/getAllWarehouses";
export const GET_WareHousesDeatailes = Basic_Url + "warehouseDep/warehouses/getWarehouseDetails/";

//////////////////////////EDIT///////////////////////////
export const Edit_PersonalInfo = Basic_Url + "editInfo/editPersonalInfo/";
export const Edit_HealthyInfo = Basic_Url + "editInfo/editHealthyInfo/";
export const Edit_MonthlySalaryInfo = Basic_Url + "editInfo/editMonthlySalaryInfo/";
export const Edit_ResidenceInfo = Basic_Url + "editInfo/editResidenceInfo/";
export const Edit_DeptsInfo = Basic_Url + "editInfo/editDeptsInfo/";
export const Edit_ExpensesInfo = Basic_Url + "editInfo/editExpensesInfo/";
export const Edit_RebuildingInfo = Basic_Url + "editInfo/editRebuildingInfo/";
export const Edit_ChildInfo = Basic_Url + "editInfo/editChildInfo/";
export const Edit_WifeInfo = Basic_Url + "editInfo/editWifeInfo/";