import react, { Component } from 'react';
import './LogInForm.css';
import axios from 'axios';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
class LogInForm extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            userName: '',
            password: ''
        }
    }
 
    changeHandler = e => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    checkRole(body) {
        console.log('hi from checkRole')
        console.log(body.data)
        console.log(body.data['role'])

        switch (body.data['role'][0]) {
            case 'المدير التنفيذي':
                {
                    this.props.history.push({
                        pathname: '/displayAssociation',
                        state: {
                            token: body.data['token'],
                            name: this.state.userName,
                            role: body.data['role'],
                            rolePermissions: body.data['rolePermissions'],
                            directPermissions: body.data['directPermissions'],
                        }
                    })
                    break; 
                }

            case 'مدير القسم التعليمي':
                {
                    this.props.history.push({
                        pathname: '/content',
                        state: {
                            token: body.data['token'],
                            name: this.state.userName,
                            role: body.data['role'],
                            rolePermissions: body.data['rolePermissions'],
                            directPermissions: body.data['directPermissions'],
                        }
                    })
                    break;
                    //  return '/content';
                }
            case 'مدير الصندوق':
                {
                
                    this.props.history.push({
                        pathname: '/getFundHomePage',
                        state: {
                            token: body.data['token'],
                            name: this.state.userName,
                            role: body.data['role'],
                            rolePermissions: body.data['rolePermissions'],
                            directPermissions: body.data['directPermissions'],
                        }
                    })
                    break;
                    //     return '/getFundsMainList';
                }
            case "مدير المستودع":
                {
                    this.props.history.push({
                        pathname: '/displayStoreDetails/4',
                        state: {
                            token: body.data['token'],
                            name: this.state.userName,
                            role: body.data['role'],
                            rolePermissions: body.data['rolePermissions'],
                            directPermissions: body.data['directPermissions'],
                        }
                    })
                    break;
                    // return '/Store';
                }
            case 'موظف استقبال':
                {
                    this.props.history.push({
                        pathname: '/StudiesSection',
                        state: {
                            token: body.data['token'],
                            name: this.state.userName,
                            role: body.data['role'],
                            rolePermissions: body.data['rolePermissions'],
                            directPermissions: body.data['directPermissions'],
                        }
                    })
                    break;
                    //return '/StudiesSection';
                }
            default:
                {
                    // SideBar(this.state.userName)
                    this.props.history.push({
                        pathname: '/content',
                        state: {
                            token: body.data['token'],
                            name: this.state.userName,
                            role: body.data['role'],
                            rolePermissions: body.data['rolePermissions'],
                            directPermissions: body.data['directPermissions'],
                        }
                    })
                    break;
                    //return '/login';
                }
        }
    }

    submitHandler = e => {
        e.preventDefault();
        const data = {
            userName: this.state.userName,
            password: this.state.password
        }
        this.setState({
            loading: true
        })
        axios.post('login', data).then(
            res => {
                const body = res.data;
                //console.log(body.data['token'])
                localStorage.setItem('token', body.data['token'])
                localStorage.setItem('name', this.state.userName)
                localStorage.setItem('role', body.data['role'])
                localStorage.setItem('rolePermissions', body.data['rolePermissions'])
                localStorage.setItem('directPermissions', body.data['directPermissions'])
                localStorage.setItem('user_id', body.data['user_id'])
                localStorage.setItem('departmentID', body.data['departmentID'])

                
                console.log(body.data);
                if (res = 200) {
                    /* 
                    const history = useHistory();
                    history.push("/Home"); */
                    this.setState({
                        loading: false
                    })
                    console.log(this.checkRole(body))
                    this.checkRole(body)
                }
            }
        ).catch(
            error => {
                console.log(error)
            }
        )
    }
    render() {
        // const { name, password } = this.state;
        return (
            <div>

                <br />
                <br />
                <br />


                {
                    this.state.loading === true ?
                        <Backdrop
                            open>
                            <CircularProgress color="inherit" />
                        </Backdrop> :

                        <center>
                            <div class="col-md-7 col-lg-5">
                                <div class="login-wrap p-4 p-md-5">
                                    <h3 class="text-center mb-4 background-color-address"> تسجيل الدخول</h3>
                                    <form onSubmit={this.submitHandler} >
                                        <div  >
                                            <br />
                                            <br />
                                            <label className="login-form label-l" >اسم المستخدم  :</label>
                                            <input type="text" id="userName" className="form-control rounded-left"
                                                value={this.state.userName}
                                                placeholder={this.state.userName}
                                                onChange={(e) => this.changeHandler(e)}
                                            //    onChange={this.changeHandler}
                                            />
                                        </div>
                                        <br />
                                        <div >
                                            <label htmlFor="password" class="login-form label-l">كلمة المرور :</label>
                                            <input type="password" id="password" class="form-control rounded-left"
                                                value={this.state.password}
                                                placeholder={this.state.password}
                                                onChange={(e) => this.changeHandler(e)}
                                            />
                                        </div>
                                        <br />
                                        <br />


                                        <button className="btn-b" type='submit' >
                                            تسجيل دخول
                      </button>
                                        {/* <center>
                            <p className="forgot-password text-right">
                                <a href=""> هل نسيت كلمة السر؟
                       </a>
                            </p>
                        </center> */}
                                    </form>
                                </div>
                            </div>

                        </center>
                }

            </div>
        )
    }
}
export default LogInForm;