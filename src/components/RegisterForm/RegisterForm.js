import react from 'react';
import {Formik , Form } from 'formik';
import * as Yup from 'yup';
import FormikControl from '../FormikControl';
import { FieldArray } from 'formik';
function RegisterForm(){
  
   
    const initialValues = {
        firstName : '',
        lastName : '',
        phoneNumbers : '',
        telNumber : '' ,
        email : '', 
        passWord : '',
        confirmPassword : '',
        address : '' ,
        workingHours : ''

      

        
    }
      const validationSchema = Yup.object({
        firstName : Yup.string().required('مطلوب'),
        lastName : Yup.string().required('مطلوب'),
        phoneNumbers: Yup.string().matches( 'رقم هاتف غير صالح'),
        telNumber: Yup.string().matches( 'رقم هاتف غير صالح'),
        email : Yup.string().email('ايميل غير صالح').required('مطلوب'),
         password : Yup.string().required('الرجاء ادخال كلمة المرور'),
         confirmPassword : Yup.string().oneOf([Yup.ref('password') , ''] , 'لا يوجد تطابق').required('الرجاء تأكيد كلمة المرور'),
         address:  Yup.string().required('  مطلوب'),
         workingHours:  Yup.string().required('مطلوب'),

    })
    const onSubmit = values => {
        console.log('Form Data')
    }


    return(
        <Formik
        initialValues ={initialValues}
        validationSchema = {validationSchema}
        onSubmit ={onSubmit}
        >
            {
                formik =>{
                    return <Form >
                        <FormikControl 
                         control = 'input'
                         type = 'text'
                         label =' الاسم : '
                          name ='firstName'
                        />
                        <FormikControl 
                         control = 'input'
                         type = 'text'
                         label =' الكنية : '
                         name ='lastName'
                        />
                         <FormikControl 
                         control = 'input'
                         type = 'text'
                         label ='موبايل : '
                          name ='phoneNumbers'
                          maxLength="10"
                        />
                         <FormikControl 
                         control = 'input'
                         type = 'text'
                         label ='أرضي  : '
                          name ='phoneNumbers'
                          maxLength="7"
                        />
                      <FormikControl 
                       control = 'input'
                       type = 'email'
                       label =' الإيميل : '
                        name ='email'
                      />
                       <FormikControl 
                       control = 'input'
                       type = 'password'
                       label =' كلمة المرور : '
                        name ='passWord'
                      />
                        <FormikControl 
                       control = 'input'
                       type = 'password'
                       label ='  تأكيد كلمة المرور : '
                        name ='confirmPassword'
                      />
                        <FormikControl 
                         control = 'input'
                         type = 'text'
                         label ='العنوان  : '
                          name ='address'
                        
                        />
                          <FormikControl 
                         control = 'input'
                         type = 'text'
                         label ='ساعات العمل  : '
                          name ='workingHours'
                          
                        />
                        <br/>
                    <button  type = 'submit' disabled = {!formik.isValid}> تسجيل </button>
                       
                    </Form>
                }
            }
  
        </Formik>
    )
  }




export default RegisterForm;
