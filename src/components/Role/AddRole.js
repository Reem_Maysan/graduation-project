import react, { Component } from 'react';
import SideBar from '../../components/SideBar/SideBar';
import "./Role.css";
import EditIcon from '@material-ui/icons/Edit';



class AddRole extends Component{
   
    render(){
        return(
           
            <div>
                 <SideBar />
                 <div className="role-cont">
            <h6>إضافة دور</h6>
            <div className="form-row r">
             <label className="label" > اسم الدور  :</label>
             <input className="input" />
              </div>
             
             <label className="label" > صلاحيات الدور  :</label>
             <br/>
             <div className="form-row r">
             <div class="form-check form-check-inline">
             <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" />
          <label class="form-check-label" for="inlineCheckbox1">قبول استمارة</label>
           </div>

            <div class="form-check form-check-inline">
             <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
            <label class="form-check-label" for="inlineCheckbox2">إضافة استمارة عوائل جمعية</label>
            </div>
            <div class="form-check form-check-inline">
             <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
            <label class="form-check-label" for="inlineCheckbox2">إضافة استمارة عائلة عامة </label>
            </div>
            <div class="form-check form-check-inline">
             <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
            <label class="form-check-label" for="inlineCheckbox2">إضافة رواتب خارجية   </label>
            </div>
            <div class="form-check form-check-inline">
             <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2" />
            <label class="form-check-label" for="inlineCheckbox2">إضافة  دور   </label>
            </div>
           

              </div>
              <center>
            <button className="add-butt"> إضافة </button>
            </center>
            </div>
           
                
            </div>
        )
    }
}

export default AddRole;