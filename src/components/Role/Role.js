import react, { Component } from 'react';
import SideBar from '../../components/SideBar/SideBar';
import "./Role.css";
import EditIcon from '@material-ui/icons/Edit';



class Role extends Component{
    routeChange = () =>{ 
    
        window.location.href = '/addrole';
      }
    render(){
        return(
           
            <div>
                 <SideBar />
                 <div className="role-cont">
            
         <center> <span className="sp-role"> <b> إدارة الأدوار </b></span></center>  
         <br/>

            <div className="role-1">
            <table className="show-table-role" size ="sm" >
                        <thead  >
                          <tr>
                            <th>اسم الدور  :</th>
                            <th>الصلاحيات  :</th>
                            <th>  </th>
                          </tr>
                        </thead>
                        <tbody>
                        <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                
                                <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                
                      
                              </tbody>
                      </table>

            </div>
            <br  />
            <br  />
            <div className="role-1">
            <table className="show-table-role" size ="sm" >
                        <thead  >
                          <tr>
                            <th>المستخدمون   :</th>
                            <th>الدور  :</th>
                            <th>الصلاحيات الإضافية   :</th>
                           
                            <th>  </th>
                          </tr>
                        </thead>
                        <tbody>
                        <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                
                                <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                <tr>
                                <td> null</td>
                                <td> null</td>
                                <td> null</td>
                                <td> <a   className="Edit" title="Edit" data-toggle="tooltip" > 
                            <EditIcon  />
                            </a></td>
                                </tr>
                                
                      
                              </tbody>
                      </table>
       </div>
       
              <center>
            <button className="add-butt" onClick={this.routeChange}>إضافة دور جديد  </button>
            </center>
            </div>
           
                
            </div>
        )
    }
}

export default Role;

