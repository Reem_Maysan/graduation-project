import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Checkbox from "@material-ui/core/Checkbox";
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../constants';
import '../../Routes';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Button } from "@material-ui/core";
import { TrainRounded } from "@material-ui/icons";
import EditIcon from '@material-ui/icons/Edit';
import './Role.css';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';

import SideBar from '../../SideBar/SideBar';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div

            item="tabpanel"
            hidden={value !== index}
            id={`nav-tabpanel-${index}`}
            aria-labelledby={`nav-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    };
}

function LinkTab(props) {
    return (
        <Tab

            component="a"
            onClick={(event) => {
                event.preventDefault();
            }}
            {...props}
        />
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));


const useRowStyles = makeStyles({
    insideHeads: {
        background: 'linear-gradient(45deg, #82E0AA 30%, #82E0AA 90%)',
        border: 0,
        borderRadius: 3,
        color: 'white',
    },
    head: {
        background: 'linear-gradient(45deg, #FFFFFF 30%, #FFFFFF 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px ',
        color: 'white',
    },
    root: {
        background: 'linear-gradient(45deg, #C4DBFF 30%,  #66A2FF 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px ',
        color: 'white',
        height: 48,
        padding: '0 30px',
        '& > *': {
            borderBottom: 'unset',
        },
    },
});

function Row(props) {
    const { row } = props;
    const [open, setOpen] = React.useState(false);
    const classes = useRowStyles();

    const [checked, setChecked] = useState({});
    const handleTabChange = (event, row) => {
        setChecked({ id: row.id, checked: event.target.checked });
    };
    ////////////////////////////// 
    const [checkedState, setCheckedState] = useState(
        new Array(row.permissions.length).fill(false)
    );
    const handleOnChange = (position) => {
        const updatedCheckedState = checkedState.map((item, index) =>
            index === position ? !item : item
        );
        setCheckedState(updatedCheckedState);
        console.log('tttttttttttttttttttttggggggggggggggggg')
        console.log(checkedState)

    }
    ////////////////////////////////

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('\\\\\\\\\\\\handleSubmit//////////')

        checkedState.map((item, index) => {
            console.log(item)
            if (item == true) {
                const perName = row.permissions[index].name;
                const newJson = editRolePer.jsonBody;
                const newList = newJson.permissions;
                const list = [...newList];
                list.push(perName)
                console.log('new list: ' + list)
                newJson['permissions'] = list
                newJson['roleID'] = row.id
                console.log('newJson: ' + newJson.permissions)
                setEditRolePer({ ...editRolePer, jsonBody: newJson, });
            }
        })
        console.log(editRolePer)

        setEditRolePer({
            ...editRolePer,
            loading: true
        });
        fetch(myConstClass.Edit_RolePer
            , {
                method: 'POST',
                body: JSON.stringify(
                    editRolePer.jsonBody
                ),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.log(json)
                console.log('wohooooooooooo')
            });

    }

    useEffect(() => {
        console.log('TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT')
        console.log(row.id)
        console.log(row.name)

    }, []);


    const [editRole, setEditRole] = useState({
        empID: "",
        roleName: "",
        directPermissions: [
        ]
    })

    const [editRolePer, setEditRolePer] = useState({
        loading: false,
        jsonBody: {
            roleID: 0,
            permissions: [
            ]
        }
    })


    return (
        <React.Fragment >
            <TableRow className={classes.root} >
                <TableCell align="center">{row.name}</TableCell>
                <TableCell align="center">
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon className="rowButtonReem" /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
            </TableRow>
            <TableRow className={classes.head}>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <br />
                        <Typography variant="h6" className="headReem"
                            gutterBottom component="div" align="center">
                            استعراض الصلاحيات الخاصة بالدور الحالي
              </Typography>
                        <br />
                        <Box marginBottom={2}  >
                            <Table size="small" aria-label="purchases" >
                                <TableHead>
                                    <TableRow className={classes.insideHeads}>
                                        <TableCell align="center" >الرقم</TableCell>
                                        <TableCell align="center"  >الصلاحية</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody align="center"  >
                                    {row.permissions.map((permissionRow, index) => (
                                        <TableRow key={permissionRow.id}>
                                            <TableCell component="th" scope="row" align="center">
                                                {index + 1}
                                            </TableCell>
                                            <TableCell align="center">{permissionRow.name}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Box>
                        <br />
                        <Typography variant="h6" gutterBottom component="div" align="center" margin={1} >
                            تعديل الصلاحيات الخاصة بالدور:
                        </Typography>
                        <br />
                        <Box margin={1}>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center" >اختيار</TableCell>
                                        <TableCell align="center"  >الصلاحية</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody align="right" >
                                    {row.permissions.map((permissionRow, index) => (
                                        <TableRow key={permissionRow.id}>
                                            <TableCell align="center" component="th" scope="row">

                                                <input
                                                    type="checkbox"
                                                    id={`custom-checkbox-${index}`}
                                                    name={row.name}
                                                    value={row.name}
                                                    checked={checkedState[index]}
                                                    onChange={() => handleOnChange(index)}
                                                />
                                            </TableCell>
                                            <TableCell align="center">{permissionRow.name}</TableCell>
                                        </TableRow>
                                    ))
                                    }
                                </TableBody>
                            </Table>
                        </Box>
                        <button margin={1} color='grey' type="submit" className="ReemButton"
                            onClick={(event) => handleSubmit(event)}>تأكيد</button>
                        <br /> <br />

                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

function UserRoleRow(props) {
    const row = props.user;
    const roles = props.roleState.data.roles;
    const permissions = props.permissionState.data;
    const [open, setOpen] = React.useState(false);
    const classes = useRowStyles();


    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('\\\\\\\\\\\\handleSubmit//////////')

        checkedState2.map((item, index) => {
            console.log(item)
            if (item == true) {
                const perName = permissions[index];
                const newJson = editRolePer.jsonBody;
                const newList = newJson.directPermissions;
                const list = [...newList];
                list.push(perName)
                console.log('new list: ' + list)
                newJson['directPermissions'] = list
                console.log('newJson: ' + newJson.permissions)
                setEditRolePer({ ...editRolePer, jsonBody: newJson, });
            }
        })
        console.log('\\\\\\\\\\\\check permissions//////////')
        console.log(editRolePer)

        checkedState.map((item, index) => {
            console.log(item)
            if (item == true) {
                const perName = roles[index].name;
                const newJson = editRolePer.jsonBody;
                newJson['roleName'] = perName
                newJson['empID'] = row.id
                console.log('newJson: ' + newJson.permissions)
                setEditRolePer({ ...editRolePer, jsonBody: newJson, });
            }
        })

        console.log('\\\\\\\\\\\\check role//////////')
        console.log(editRolePer)

        setEditRolePer({
            ...editRolePer,
            loading: true
        });
        fetch(myConstClass.Edit_UserRole
            , {
                method: 'POST',
                body: JSON.stringify(
                    editRolePer.jsonBody
                ),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.log(json)
                console.log('wohooooooooooo')
            });
    }

    useEffect(() => {
        console.log('GGGGGGGGGGGGGAAAAAAAAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLAAAAAAAAAAAAAA')
        console.log(props.user)
    });
    ////////////////////////////// 
    const [editRolePer, setEditRolePer] = useState({
        loading: false,
        jsonBody: {
            empID: "",
            roleName: "",
            directPermissions: []
        }
    })
    const [checkedState, setCheckedState] = useState(
        new Array(roles.length).fill(false)
    );

    const handleOnChange = (position) => {
        const updatedCheckedState =
            checkedState.map((item, index) =>
                index === position ? !item : item);

        setCheckedState(updatedCheckedState);
        console.log('tttttttttttttttttttttggggggggggggggggg')
        console.log(checkedState)
    }
    const [checkedState2, setCheckedState2] = useState(
        new Array(permissions.length).fill(false)
    );

    const handleOnChange2 = (position) => {
        const updatedCheckedState2 =
            checkedState2.map((item, index) =>
                index === position ? !item : item);

        setCheckedState2(updatedCheckedState2);
        console.log('tttttttttttttttttttttggggggggggggggggg')
        console.log(checkedState2)
    }


    return (
        <React.Fragment >
            <TableRow >
                <TableCell align="center">{row.firstName + ' ' + row.lastName}</TableCell>
                <TableCell align="center">{row.roles[0] ? row.roles[0].name : "لا يوجد"} </TableCell>
                {/* {
                    row.permissions && row.permissions.length > 0 ?
                        row.permissions.map((permissionRow) => (
                            <TableCell align="left">{permissionRow.name}</TableCell>
                        )) : <TableCell align="left">لا يوجد</TableCell>
                } */}
                <TableCell align="center">
                    <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
            </TableRow>
            <TableRow className={classes.head}>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Typography variant="h6" gutterBottom component="div" align="right">
                            صلاحيات المستخدم:
                        </Typography>
                        <Box margin={1}>
                            <Table size="small" aria-label="purchases" >
                                <TableHead>
                                    <TableRow className={classes.insideHeads}>
                                        <TableCell align="center" >الرقم</TableCell>
                                        <TableCell align="center"  >الصلاحية</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody align="right"  >
                                    {row.permissions.map((permissionRow, index) => (
                                        <TableRow key={index + 1}>
                                            <TableCell align="center">{index + 1}</TableCell>
                                            <TableCell align="center">{permissionRow.name}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Box>


                        <Typography variant="h6" gutterBottom component="div" align="right">
                            تعديل دور المستخدم:
                        </Typography>
                        <Box margin={1}>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center" >اختيار</TableCell>
                                        <TableCell align="center"  >الصلاحية</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody align="right" >
                                    {roles.map((permissionRow, index) => (
                                        <TableRow key={permissionRow.id}>
                                            <TableCell align="center" component="th" scope="row">

                                                <input
                                                    type="checkbox"
                                                    id={`custom-checkbox-${index}`}
                                                    name={row.name}
                                                    value={row.name}
                                                    checked={checkedState[index]}
                                                    onChange={() => handleOnChange(index)}
                                                />
                                            </TableCell>
                                            <TableCell align="center">{permissionRow.name}</TableCell>
                                        </TableRow>
                                    ))
                                    }
                                </TableBody>
                            </Table>
                        </Box>

                        <Typography variant="h6" gutterBottom component="div" align="right">
                            اختيار الصلاحيات الاضافية للدور الجديد:
                        </Typography>
                        <Box margin={1}>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="center" >اختيار</TableCell>
                                        <TableCell align="center"  >الصلاحية</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody align="right" >
                                    {permissions.map((permissionRow, index) => (
                                        <TableRow key={permissionRow.id}>
                                            <TableCell align="center" component="th" scope="row">
                                                <input
                                                    type="checkbox"
                                                    id={`custom-checkbox-${index}`}
                                                    name={row.name}
                                                    value={row.name}
                                                    checked={checkedState2[index]}
                                                    onChange={() => handleOnChange2(index)}
                                                />
                                            </TableCell>
                                            <TableCell align="center">{permissionRow.name}</TableCell>
                                        </TableRow>
                                    ))
                                    }
                                </TableBody>
                            </Table>
                        </Box>
                        <button margin={1} color='grey' type="submit" className="ReemButton"
                            onClick={(event) => handleSubmit(event)}>تأكيد</button>
                        <br /> <br />
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

function RolesTable() {

    // const { myProps } = props.params.location.state['token'];
    const [pageInfo, setPageInfo] = useState({
        loading: false,
        // token: myProps,
        jsonBody: {
            roleName: "",
            permissions: []
        }
    });

    const classes = useStyles();
    const [value, setValue] = useState(0);


    const handleTabChange = (event, newValue) => {
        setValue(newValue);
    };

    const [roleInfo, setRoleInfo] = useState({
        loading: false,
        data: {
            roles: [
                {
                    id: 0,
                    name: '',
                    guard_name: '',
                    created_at: '',
                    updated_at: '',
                    permissions: [
                        {
                            id: 0,
                            name: '',
                            guard_name: '',
                            created_at: '',
                            updated_at: '',
                            pivot: {
                                role_id: 0,
                                permission_id: 0
                            },
                        }
                    ]
                }
            ]
        }
    });

    const [userRoleoleInfo, setuserRoleoleInfo] = useState({
        loading: false,
        data: {
            users: [
                {
                    id: 0,
                    firstName: "",
                    lastName: "",
                    fatherName: "",
                    motherName: "",
                    nationality: "",
                    dataBirth: "",
                    placeBirth: "",
                    IDN: "",
                    IDNPlace: "",
                    IDNDate: "",
                    homePhone: 0,
                    education: "",
                    lastCertificate: "",
                    certificateSource: "",
                    familyStatus: "",
                    address: "",
                    healthStatus: "",
                    cvDescription: "",
                    requiredJob: "",
                    currentWork: "",
                    externalSalary: "",
                    created_at: "",
                    updated_at: "",
                    userName: "",
                    roles: [],
                    permissions: []
                }
            ]
        }
    });

    const [permissionInfo, setPermissionInfo] = useState({
        loading: false,
        data: [
            {
                id: 0,
                name: "",
            }
        ]

    });

    const userRolesInfoGet = async () => {
        console.log('userRolesInfoGet userRolesInfoGet userRolesInfoGet')
        setPageInfo({ ...pageInfo, loading: true });
        const res = await axios.get(myConstClass.GET_UsersRoles)
        console.log('json data')
        console.log(res.data['data'])
        setPageInfo({ ...pageInfo, loading: false });
        setuserRoleoleInfo({ ...userRoleoleInfo, data: res.data['data'] });
    }

    const roleInfoGet = async () => {
        console.log('roleInfoGet roleInfoGet roleInfoGet')
        setPageInfo({ ...pageInfo, loading: true });
        const res = await axios.get(myConstClass.GET_AllRoles)
        console.log('json data')
        console.log(res.data['data'])
        setPageInfo({ ...pageInfo, loading: false });
        setRoleInfo({ ...roleInfo, data: res.data['data'] });
    }

    const permissionInfoGet = async () => {
        console.log('permissionInfo permissionInfo permissionInfo')
        setPageInfo({ ...pageInfo, loading: true });
        const res = await axios.get(myConstClass.GET_AllPermissions)
        console.log('json data')
        console.log(res.data['data'])
        setPageInfo({ ...pageInfo, loading: false });
        setPermissionInfo({ ...permissionInfo, data: res.data['data'] });
    }

    useEffect(() => {
        console.log('useEffect')
        roleInfoGet();
        userRolesInfoGet();
        permissionInfoGet();
    }, []);

    const handleSubmit = (e) => {
        console.log('hi addRole')
        e.preventDefault();
        setPageInfo({
            ...pageInfo,
            loading: true
        }, () => {
            fetch(myConstClass.ADD_AddRole
                , {
                    method: 'POST',
                    body: JSON.stringify(
                        pageInfo.jsonBody
                    ),
                    headers: {
                        'Accept': 'application/json',
                        'Content-type': 'application/json'
                    },
                })
                .then((response) => response.json())
                .then((json) => {
                    console.log(json)
                    setPageInfo({
                        ...pageInfo,
                        loading: false,
                        addRole: true
                    });
                });
        })
    };

    // handle input change
    const handleInputChange = (event) => {
        console.log('hi handleInputChange')
        const newRolePermissionsInfo = pageInfo.jsonBody.permissions;
        const list = [...newRolePermissionsInfo];
        const newJsonBody = {
            roleName: event.target.value,
            permissions: list
        }
        setPageInfo({ ...pageInfo, jsonBody: newJsonBody });
        console.log(pageInfo);
    };
    ///////////
    const [checkedState, setCheckedState] = useState(
        new Array(permissionInfo.data.length).fill(false)
    );

    const handleOnChange = (position) => {
        const updatedCheckedState = checkedState.map((item, index) =>
            index === position ? !item : item
        );
        setCheckedState(updatedCheckedState);
        console.log('add role check')
        console.log(checkedState)
    }
    const handleAddRole = (e) => {
        e.preventDefault();
        console.log('\\\\\\\\\\\\handleAddRole//////////')

        checkedState.map((item, index) => {
            console.log(item)
            if (item == true) {
                const perName = permissionInfo.data[index].name;
                const newJson = pageInfo.jsonBody;
                const newList = newJson.permissions;
                const list = [...newList];
                list.push(perName)
                console.log('new list: ' + list)
                newJson['permissions'] = list
                newJson['roleName'] = pageInfo.jsonBody.roleName
                console.log('newJson: ' + newJson.permissions)
                setPageInfo({ ...pageInfo, jsonBody: newJson, });
            }
        })

        console.log(pageInfo)
        setPageInfo({
            ...pageInfo,
            loading: true
        });
        fetch(myConstClass.ADD_AddRole
            , {
                method: 'POST',
                body: JSON.stringify(
                    pageInfo.jsonBody
                ),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.log(json)
                console.log('wohooooooooooo')
            });
        setPageInfo({
            ...pageInfo,
            loading: false
        });

    }
    ///////////

    return (
        <div>
            <SideBar />
            {
                pageInfo.loading === true ?
                    <Backdrop open>
                        <CircularProgress color="inherit" />
                    </Backdrop> :
                    <div className={classes.root}>

                        <AppBar position="static">
                            <Tabs
                                variant="fullWidth"
                                value={value}
                                onChange={handleTabChange}
                                aria-label="nav tabs example"
                            >
                                <LinkTab label="أدوار الموظفين" href="/drafts" {...a11yProps(0)} />
                                <LinkTab label="استعراض الحسابات" href="/trash" {...a11yProps(1)} />
                            </Tabs>
                        </AppBar>
                        <div className="container_rectangle">
                            <TabPanel value={value} index={0}>
                                <center>
                                    {pageInfo.loading === true ?
                                        <Backdrop open>
                                            <CircularProgress color="inherit" />
                                        </Backdrop> :
                                        <div>
                                            <Box marginLeft={10} marginRight={10} marginTop={5}>
                                                <TableContainer component={Paper} >
                                                    <Table aria-label="collapsible table" >
                                                        <TableHead >
                                                            <TableRow >
                                                                <TableCell align="center">أدوار الموظفين في النظام</TableCell>
                                                                <TableCell align="center">استعراض وتعديل الصلاحيات</TableCell>
                                                            </TableRow>
                                                        </TableHead>
                                                        <TableBody>
                                                            {
                                                                roleInfo.data.roles.map((role, index) => (
                                                                    <Row key={index} row={role} />
                                                                ))
                                                            }
                                                        </TableBody>
                                                    </Table>
                                                </TableContainer>
                                            </Box>
                                            <br /> <br />
                                            <button
                                                align="center"
                                                onClick={
                                                    () => {
                                                        setPageInfo({
                                                            ...pageInfo,
                                                            addRole: true
                                                        });
                                                    }
                                                }
                                                className="ReemButton"
                                                type="submit"
                                            >إضافة دور</button>
                                            <br /> <br />
                                            {
                                                pageInfo.addRole === true ?
                                                    <div align="center">
                                                        <Box marginLeft={4} marginRight={0} marginTop={5}>
                                                            <input
                                                                className="input-f"
                                                                name="roleName"
                                                                placeholder={pageInfo.jsonBody.roleName === '' ?
                                                                    "اسم الدور" :
                                                                    pageInfo.jsonBody.roleName}
                                                                value={pageInfo.jsonBody.roleName}
                                                                onChange={e => handleInputChange(e)}
                                                            /></Box>
                                                        <Box marginRight={10} marginTop={5}>
                                                            <Typography variant="h6" gutterBottom component="div" align="center">
                                                                إضافة صلاحيات الدور الجديد: </Typography>
                                                        </Box>
                                                        <Box margin={1}>
                                                            <Table size="small" aria-label="purchases">
                                                                <TableHead>
                                                                    <TableRow>
                                                                        <TableCell align="center" >اختيار</TableCell>
                                                                        <TableCell align="center"  >الصلاحية</TableCell>
                                                                    </TableRow>
                                                                </TableHead>
                                                                <TableBody align="right" >

                                                                    {
                                                                        permissionInfo.data.map((permissionRow, index) => (
                                                                            <TableRow key={permissionRow.id}>
                                                                                <TableCell align="center" component="th" scope="row">
                                                                                    <input
                                                                                        type="checkbox"
                                                                                        id={`custom-checkbox-${index}`}
                                                                                        name={permissionRow.name}
                                                                                        value={permissionRow.name}
                                                                                        checked={checkedState[index]}
                                                                                        onChange={() => handleOnChange(index)}
                                                                                    />
                                                                                </TableCell>
                                                                                <TableCell align="center">{permissionRow.name}</TableCell>
                                                                            </TableRow>
                                                                        ))
                                                                    }
                                                                </TableBody>
                                                            </Table>
                                                        </Box>
                                                        <button
                                                            align="center"
                                                            onClick={(e) => handleAddRole(e)}
                                                            className="ReemButton"
                                                            type="submit"
                                                        >تأكيد</button>
                                                    </div> : null
                                            }
                                        </div>}
                                </center>
                            </TabPanel>
                        </div>
                        <TabPanel value={value} index={1}>
                            {pageInfo.loading === true ?
                                <Backdrop open>
                                    <CircularProgress color="inherit" />
                                </Backdrop> :
                                <div className="container_rectangle">
                                    <TableContainer component={Paper}>
                                        <Table aria-label="collapsible table" >
                                            <TableHead >
                                                <TableRow >
                                                    <TableCell align="center">المستخدم</TableCell>
                                                    <TableCell align="center">دوره في النظام</TableCell>
                                                    {/* <TableCell align="center">الصلاحيات الإضافية</TableCell> */}
                                                    <TableCell align="center">استعراض</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    userRoleoleInfo.data.users.map((user, index) => (
                                                        <UserRoleRow key={index} user={user}
                                                            roleState={roleInfo} permissionState={permissionInfo} />
                                                    ))
                                                }
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </div>
                            }
                        </TabPanel>
                    </div>
            }
        </div>

    );
}

export default RolesTable;