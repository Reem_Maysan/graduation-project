import React, { Component } from 'react';
import axios from 'axios';
import { IconButton } from '@material-ui/core';
import { Table, Button } from 'react-bootstrap';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import '../../../Routes';

class CityList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cities: [],
      id: '',
      name: '',

    }
  }


  changeHandler = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }



  handleSubmit = e => {

    e.preventDefault();

    const data = {
      name: this.state.name
    }
    console.log("User name : " + this.state.name);
    axios.post('https://86f29bd3c253.ngrok.io/api/city', this.state).then(
      res => {

        console.log(res);
        if (res = 200) {
          this.refreshList();
          //   const history = useHistory();
          //      history.push("/Home");
          console.log("good");
        }
      }
    ).catch(
      error => {
        console.log(error)
      }
    )
  }


  async delete(id) {
    const requestOptions = {
      method: 'DELETE'
    };
    fetch("https://c0d2a9b30ad7.ngrok.io/api/city/" + id, requestOptions).then((response) => {
      return response.json();
    }).then((response) => {
      if (response != 404) {
        console.log("the list after deleting...");
        this.refreshList();
      } else {
        console.log('error');
      }

    });
  }

  componentDidMount() {
    this.refreshList();
  }
  async refreshList() {
    const res = await axios.get('https://c0d2a9b30ad7.ngrok.io/api/city')
    console.log(res.data)
    this.setState({
      loading: false,
      cities: res.data,
      name: ''
    })
  }

  render() {
    /*{users.id}*/
    const { cities, id, name } = this.state;
    return (
      <form onSubmit={this.handleSubmit}>
        <Table className="table table-bordered" size="sm" >
          <thead className="thead-dark">
            <tr>
              <th>city_id</th>
              <th>name</th>
              <th>created_at</th>
              <th>updated_at</th>
              <th></th>
            </tr>




            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>
              <a href="/add" role="button" class="btn btn-primary"  >
                {/*  {this.state.name == "" ? "Add" : "Save"}*/}
                     Add City
                     </a>

            </td>

          </thead>
          <tbody>
            {(cities.length > 0) ? cities.map((city, index) => {
              return (
                <tr>
                  <td> {city.id} </td>
                  <td> {city.name}</td>
                  <td> {city.created_at} </td>
                  <td> {city.updated_at}</td>


                  <td>
                    <a className="add" title="Add" data-toggle="tooltip"
                      onClick={() => { this.delete(city.id) }} >
                      <DeleteIcon />
                    </a>
                    {' '} {' '} {' | '}
                    <a href={'/Edit/' + city.id} className="Edit" title="Edit" data-toggle="tooltip"
                    >
                      <EditIcon />
                    </a>
                  </td>
                  <td>


                  </td>


                </tr>

              )
            }) : <tr><td colSpan="5">Loading...</td></tr>}
          </tbody>
        </Table>
      </form>

    );



  }
}
export default CityList;
