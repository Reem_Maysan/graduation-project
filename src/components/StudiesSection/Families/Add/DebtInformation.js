import React, { useState, useEffect } from "react";
import './PersonalInformation.css';
import '../../../Routes';

import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

///////

import { AddBox, AddBoxRounded, AddCircleRounded, AddRounded, Cancel, CancelOutlined, CancelScheduleSend, CancelSharp, DeleteSharp, LocalSee, RemoveCircleOutline } from '@material-ui/icons';

import IconButton from '@material-ui/core/IconButton';

import SideBar from '../../../SideBar/SideBar';
import {
  Formik,
  Field,
  Form,
  useField,
  FieldAttributes,
  FieldArray
} from "formik";
import {
  TextField,
  Button,
  Checkbox,
  Radio,
  FormControlLabel,
  Select,
  MenuItem, InputBase,
  InputLabel,
  TextareaAutosize, FormControl
} from "@material-ui/core";
import { CustomTextField } from '../../../TextFields/TextField';
/////

//import { MyTextField } from '../../TextFields/TextField';
import * as Yup from 'yup';
import { Box, Divider } from '@material-ui/core';
import { createStyles, makeStyles, withStyles, Theme } from '@material-ui/core/styles';
function DebtInformation(props) {

  const [debtsState, setDebts] = useState({
    id: props.location.state['id'],
    edit: props.location.state['edit'],
    //token: props.location.state['token'],

    jsonBody: {
      IDN: props.location.state['IDN'],
      depts: [{
        source: "",
        deptValue: 0
      }]
    }
  });


  const debtsInfoGet = async () => {
    setDebts({ ...debtsState, loading: true });
    const res = await axios.get(myConstClass.GET_DeptsInfo + debtsState.id)
    const newjsonBody = {
      IDN: debtsState.jsonBody.IDN,
      depts: res.data['data']
    }
    setDebts({ ...debtsState, jsonBody: newjsonBody, loading: false });
    console.log('debtsState')
    console.log(debtsState.jsonBody)
  }

  useEffect(() => {
    console.log('useEffect')
    console.log(debtsState)
    if (debtsState.edit) {
      console.log('------------EDIT MODE------------')
      debtsInfoGet();
    }
    else {
      console.log('------------ADD MODE------------')
    }
  });

  // handle input change
  const handleInputChange = (e, index) => {
    console.log('hi handleInputChange')
    const { name, value } = e.target;
    const newDebtsInfo = debtsState.jsonBody.depts;
    const list = [...newDebtsInfo];
    list[index][name] = value;
    const newjsonBody = {
      IDN: debtsState.jsonBody.IDN,
      depts: list
    }
    setDebts({ ...debtsState, jsonBody: newjsonBody });
    console.log(debtsState);
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    console.log('hi handleRemoveClick')
    const newDebtsInfo = debtsState.jsonBody.depts;
    const list = [...newDebtsInfo];
    list.splice(index, 1);
    const newjsonBody = {
      IDN: debtsState.jsonBody.IDN,
      depts: list
    }
    setDebts({ ...debtsState, jsonBody: newjsonBody });
    console.log(debtsState);
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    console.log('hi handleAddClick')
    const newDebtsInfo = debtsState.jsonBody.depts;
    const list = [...newDebtsInfo];
    list.push({ source: "", deptValue: "" });
    const newjsonBody = {
      IDN: debtsState.jsonBody.IDN,
      depts: list
    }
    setDebts({ ...debtsState, jsonBody: newjsonBody });
    console.log(debtsState);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('\\\\\\\\\\\\handleSubmit//////////')
    console.log(debtsState)
    // const currentUrl = debtsState.edit ? myConstClass.Edit_DeptsInfo + debtsState.id : myConstClass.ADD_DeptsInfo;
    // console.log('currentUrl')
    // console.log(currentUrl)
    // setDebts({
    //   ...debtsState,
    //   loading: true
    // }, () => {
    //   fetch(currentUrl
    //     , {
    //       method: 'POST',
    //       body: JSON.stringify(
    //         debtsState.jsonBody
    //       ),
    //       headers: {
    //         'Accept': 'application/json',
    //         'Content-type': 'application/json'
    //       },
    //     })
    //     .then((response) => response.json())
    //     .then((json) => {
    //       console.log(json)
    props.history.push({
      pathname: '/addPartnerList',
      state: {
        //id: debtsState.id,
        //edit: debtsState.edit,
        // IDN: debtsState.jsonBody.IDN,
        //token: debtsState.token
      }
    })
    // });
    // })
  }

  const validate = Yup.object({
    depts: Yup.array().of(
      Yup.object({
        source: Yup.string()
          //  .max(10, myConstClass.Less_10)
          .required(myConstClass.Required),
        deptValue: Yup.number().positive().integer()
          //  .max(10, myConstClass.Less_10)
          .required(myConstClass.Required),
      })
    )
  })

  return (
    <div>
      <SideBar />
      {
        debtsState.loading === true ?
          <Backdrop
            open>
            <CircularProgress color="inherit" />
          </Backdrop> :
          <Formik
            validateOnChange={true}
            initialValues={
              {
                IDN: props.location.state['IDN'],
                depts: [{
                  source: "",
                  deptValue: 0
                }]
              }
            }
            validationSchema={validate}
            onSubmit={values => {
              console.log('\\\\\\\\\\\\values//////////')
              console.log(values)
              const currentUrl = debtsState.edit ? myConstClass.Edit_DeptsInfo + debtsState.id : myConstClass.ADD_DeptsInfo;
              console.log('currentUrl')
              console.log(currentUrl)
              setDebts({
                ...debtsState,
                loading: true,
                depts: values
              });
              fetch(currentUrl
                , {
                  method: 'POST',
                  body: JSON.stringify(
                    values
                  ),
                  headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                  },
                })
                .then((response) => response.json())
                .then((json) => {
                  console.log(json)
                  props.history.push({
                    pathname: '/addPartnerList',
                    state: {
                      id: debtsState.id,
                      edit: debtsState.edit,
                      IDN: values.IDN,
                      //token: debtsState.token
                    }
                  })
                });
            }}
          >
            {formik => (
              <div className="rec-contain">
                <h4 className="h4-Reem"> إضافة دين </h4>
                {console.log(formik.values)}
                <Form>
                  <div className="rec-contain">
                    <div className="form-row r">
                      <FieldArray name="depts">
                        {
                          arrayHelpers => (
                            <div>
                              {formik.values.depts.map((salaryItem, index) => {
                                return (
                                  <div className="form-row r" key={index}>
                                    <CustomTextField
                                      placeholder="المصدر"
                                      name={`depts.${index}.source`}
                                    /> &nbsp;
                                    <CustomTextField
                                      placeholder="القيمة"
                                      name={`depts.${index}.deptValue`}
                                    />
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                    <div  >
                                      {formik.values.depts.length - 1 === index &&
                                        //             <button
                                        //               className="btn btn-primary btn-rounded mr"
                                        //               onClick={() =>
                                        //                 arrayHelpers.push({
                                        //                   source: "",
                                        //                   deptValue: 0
                                        //                   // id: "" + Math.random()
                                        //                 })
                                        //               }
                                        //             >
                                        //               إضافة
                                        // </button>
                                        <IconButton aria-label="delete" >
                                          <AddCircleRounded fontSize="large"
                                            onClick={() =>
                                              arrayHelpers.push({
                                                source: "",
                                                deptValue: "",
                                              })
                                            }
                                          />
                                        </IconButton>
                                      }
                            &nbsp;
                            {formik.values.depts.length !== 1 &&
                                        <IconButton aria-label="delete" >
                                          <CancelOutlined fontSize="large"
                                            onClick={() => arrayHelpers.remove(index)}></CancelOutlined>
                                        </IconButton>
                                        // < button
                                        //   className="btn btn-primary btn-rounded mr"
                                        //   onClick={() => arrayHelpers.remove(index)}>
                                        //   حذف  </button>

                                      }
                                    </div>
                                  </div>
                                );
                              })}
                            </div>
                          )
                        }
                      </FieldArray>
                    </div>
                    <button
                      className="ReemButton"
                      type="submit">
                      تأكيد
            </button>
                  </div>
                </Form>
              </div>
            )}
          </Formik>

      }
    </div >
  );
}

export default DebtInformation;

/*   <div className="contain">
      <center>
        <h6 className="h6-h2"> إضافة دين  </h6>
        <div className="p-t" >
          <form >
            {debtsState.jsonBody.depts.map((dept, index) => {
              return (
                <div className="box" key={index}>
                  <input className="input-f"

                    placeholder={dept.source === '' ?
                      "المصدر" :
                      dept.source}
                      name="source"
                    value={dept.source}
                    onChange={e => handleInputChange(e, index)}
                  />
                  <input
                    className="input-f"
                    name="deptValue"
                    placeholder={dept.deptValue === 0 ?
                      "القيمة" :
                      dept.deptValue}
                    value={dept.deptValue}
                    onChange={e => handleInputChange(e, index)}
                  />

                  {debtsState.jsonBody.depts.length !== 1 &&
                    <button className="btn btn-primary btn-rounded mr"
                      onClick={() => handleRemoveClick(index)}>حذف</button>}
                  {debtsState.jsonBody.depts.length - 1 === index &&
                    <button className="btn btn-primary btn-rounded mr"
                      onClick={handleAddClick}>إضافة</button>}

                </div>
              );
            })}

          </form>

        </div>

      </center>
      <br />
      <br />
      <button
        onClick={(event) => handleSubmit(event)}
        className="next"
        type="submit"
      /* to={{
        pathname: '/addPartnerList',
        state: {
          id: debtsState.id,
          edit: debtsState.edit,
          IDN: debtsState.IDN
        }
      }}
      > التالي </button>
      <br />
      <br />
      <Link
        className="next"
        type="submit"
        to={{
          pathname: '/addPartnerList',
          state: {
            id: debtsState.id,
            edit: debtsState.edit,
            IDN: debtsState.IDN
          }
        }}> تخطي </Link>
      {/* <a href="/partners" className="next" type="submit">التالي  &raquo; </a>
      <br />
      <br />
      <a href="" className="next" type="submit">تخطي  &raquo; </a>

    </div>
 */