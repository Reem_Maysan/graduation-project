import React from 'react';
import { Row, Form, Col, Button } from 'react-bootstrap';
import axios from 'axios';

class AddProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      name: '',
    
    }

    this.handleChange = this.changeHandler.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  changeHandler = e =>{
    this.setState({
        [e.target.name ] : e.target.value
    })
  }

  handleSubmit = e =>{
    e.preventDefault();
     const data = {
       name : this.state.name 
     }
     console.log("User name : " + this.state.name);
     axios.post('https://c0d2a9b30ad7.ngrok.io/api/city', this.state).then(
       res =>{
          
           console.log(res);
           if(res = 200){
             this.refreshList();
     //   const history = useHistory();
     //      history.push("/Home");
     console.log("good");
           }
       }
   ).catch(
       error =>{
           console.log(error)
       } 
   )}
  

  render() {

    
    

    return(
      <div>
         <h2>Add City</h2>
      
            <Form onSubmit={this.handleSubmit}>
          
              <Form.Group controlId="name">
                <Form.Label>City Name</Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  value={this.state.name}
                  onChange={this.changeHandler}
                  placeholder="City Name"/>
              </Form.Group>
              <Form.Group>
              
                <Button variant="success" type="submit">Save</Button>
              </Form.Group>
            </Form>
         
      </div>
    )
  }
}

export default AddProduct;
