import React, { useState, useEffect } from "react";
import './PersonalInformation.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import SideBar from '../../../SideBar/SideBar';
function PartnerList(props) {

  const [partnerListInfo, setpartnerListInfo] = useState(
    {
      id: props.location.state['id'],
      edit: props.location.state['edit'],
      IDN: props.location.state['IDN'],
      // token: props.location.state['token'],
      loading: false,
      patrners: [
        /*  {
            firstName: "",
            fatherName: "",
            lastName: "",
            motherName: "",
            birthPlace: "",
            birthDate: "",
            nationality: "",
            wifeIDN: 0,
            IDNPlace: "",
            address: "",
            education: "",
            lastCertificate: "",
            certificateSource: "",
            currentWork: "",
            currentWorkAddress: "",
            currentWorkPhone: 0,
            healthProblem: "",
            physician: "",
            therapy: "",
            monthlyCost: "",
            medicineCost: "",
            notes: "" 
          }*/
      ]
    }
  );

  const partnersInfoGet = async () => {
    setpartnerListInfo({ ...partnerListInfo, loading: true });
    const res = await axios.get(myConstClass.GET_WivesInfo + partnerListInfo.IDN)
    setpartnerListInfo({ ...partnerListInfo, patrners: res.data['data'], loading: false });
    console.log('partnerListInfo')
    console.log(partnerListInfo.patrners)
  }

  useEffect(() => {
    console.log('useEffect')
    if (partnerListInfo.patrners.length > 0) {
      console.log('---------------')
      partnersInfoGet();
    }
  });

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    console.log('hi handleRemoveClick')
    const newpartnerListInfo = partnerListInfo.patrners;
    const list = [...newpartnerListInfo];
    list.splice(index, 1);
    setpartnerListInfo({ ...partnerListInfo, patrners: list });
    console.log(partnerListInfo);
  };

  return (
    <div>
      <SideBar />
      <div>
        <center>
          <table className="show-table" >
            <thead  >
              <tr>
                <th>الرقم</th>
                <th>اسم الشريك</th>
                <th>العملية</th>
              </tr>
            </thead>
            <tbody>
              {
                Array.isArray(partnerListInfo.patrners) ?
                  (partnerListInfo.patrners.length > 0) ? partnerListInfo.patrners.map((person, index) => {
                    return (
                      <tr key={index}>
                        <td> {index+1} </td>
                        <td> {person.name}</td>
                        <td>  <Link
                          onClick={(index) => handleRemoveClick(index)}> حذف </Link>
                          <Link to={{
                            pathname: '/addPartnerInformation',
                            state: {
                              id: partnerListInfo.id,
                              edit: true,
                              IDN: partnerListInfo.IDN,
                              patrners: partnerListInfo.patrners,
                            }
                          }}> تعديل </Link>
                        </td>
                      </tr>
                    )
                  }) : <tr><td colSpan="5">Loading...</td></tr>
                  : <tr><td colSpan="5">Loading...</td></tr>}
            </tbody>
          </table>

        </center>
        <Link
          className="ReemButton"
          type="submit"
          to={{
            pathname: '/addPartnerInformation',
            state: {
              id: partnerListInfo.id,
              // add a partner is always in add mode
              edit: false,
              IDN: partnerListInfo.IDN,
              // I send all partners bcause the api request is there not here
              patrners: partnerListInfo.patrners,
              //token: partnerListInfo.token
            }
          }}> إضافة شريك </Link>
        <Link
          className="ReemButton"
          type="submit"
          to={{
            pathname: '/addChildList',
            state: {
              id: partnerListInfo.id,
              edit: partnerListInfo.edit,
              IDN: partnerListInfo.IDN,
              token: partnerListInfo.token
            }
          }}
        > التالي </Link>
        {/* <button
          className="ReemButton"
          onClick={() => {
            this.props.history.push({
              pathname: '/addPersonalInformation',
              state: {
                id: -1,
                edit: false,
                // token: dataState.token
              }
            })
          }} >
          إضافة عائلة</button> */}
        <br />
        <br />
      </div>
    </div>
  );
}

export default PartnerList;

/*  <h6> قائمة الشركاء </h6>
      <br />
      <table className="table-t table-bordered">
        <thead>
          <tr>
            <th scope="col">الرقم</th>
            <th scope="col"> اسم الشريك</th>
            <th scope="col">العملية</th>
          </tr>
        </thead>
        <tbody>
          {(partnerListInfo.patrners.length > 0) ? partnerListInfo.patrners.map((partner, index) => {
            return (
              <tr key={index}>
                <td> {partner.id} </td>
                <td> {partner.name}</td>
                <td>  <Link
                  onClick={(index) => handleRemoveClick(index)}> حذف </Link>
                  <Link to={{
                    pathname: '/addPartnerInformation',
                    state: {
                      id: partnerListInfo.id,
                      edit: true,
                      IDN: partnerListInfo.IDN,
                      patrners: partnerListInfo.patrners,
                    }
                  }}> تعديل </Link>
                </td>
              </tr>
            )
          }) : <tr><td colSpan="5">الرجاء الانتظار...</td></tr>}
        </tbody>
      </table>
      <center>
        <Link
          className="next"
          type="submit"
          to={{
            pathname: '/addPartnerInformation',
            state: {
              id: partnerListInfo.id,
              // add a partner is always in add mode
              edit: false,
              IDN: partnerListInfo.IDN,
              // I send all partners bcause the api request is there not here
              patrners: partnerListInfo.patrners,
              //token: partnerListInfo.token
            }
          }}> إضافة شريك </Link>
      </center>
      <br />
      <Link
        className="next"
        type="submit"
        to={{
          pathname: '/addChildList',
          state: {
            id: partnerListInfo.id,
            edit: partnerListInfo.edit,
            IDN: partnerListInfo.IDN,
            token: partnerListInfo.token
          }
        }}
      > التالي </Link>
    */