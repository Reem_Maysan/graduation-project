import React, { useState, useEffect } from "react";
import './PersonalInformation.css';
import '../../../Routes';

import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

///////

import {
  Formik,
  Field,
  Form,
  useField,
  FieldAttributes,
  FieldArray
} from "formik";

import { AddBox, AddBoxRounded, AddCircleRounded, AddRounded, Cancel, CancelOutlined, CancelScheduleSend, CancelSharp, DeleteSharp, LocalSee, RemoveCircleOutline } from '@material-ui/icons';

import IconButton from '@material-ui/core/IconButton';

import SideBar from '../../../SideBar/SideBar';

import {
  TextField,
  Button,
  Checkbox,
  Radio,
  FormControlLabel,
  Select,
  MenuItem, InputBase,
  InputLabel,
  TextareaAutosize, FormControl
} from "@material-ui/core";
import { CustomTextField } from '../../../TextFields/TextField';
/////

//import { MyTextField } from '../../TextFields/TextField';
import * as Yup from 'yup';
import { Box, Divider } from '@material-ui/core';
import { createStyles, makeStyles, withStyles, Theme } from '@material-ui/core/styles';

function OutSalaryInforamation(props) {

  const [externalSalaryInfo, setExternalSalaryInfo] = useState({
    loading: false,
    id: props.location.state['id'],
    edit: props.location.state['edit'],
    //  token: props.location.state['token'],
    jsonBody: {
      IDN: props.location.state['IDN'],
      monthlySalaries: [
        {
          source: "",
          salaryValue: 0
        }],
    }
  });

  const externalSalaryInfoGet = async () => {
    setExternalSalaryInfo({ ...externalSalaryInfo, loading: true });
    const res = await axios.get(myConstClass.GET_MonthlySalaryInfo + externalSalaryInfo.id)
    const newjsonBody = {
      IDN: externalSalaryInfo.jsonBody.IDN,
      monthlySalaries: res.data['data']
    }
    setExternalSalaryInfo({ ...externalSalaryInfo, jsonBody: newjsonBody, loading: false });
    console.log('externalSalaryInfo')
    console.log(externalSalaryInfo.jsonBody)
  }

  useEffect(() => {
    console.log('useEffect')
    console.log(externalSalaryInfo)
    if (externalSalaryInfo.edit) {
      console.log('------------EDIT MODE------------')
      externalSalaryInfoGet();
    }
    else {
      console.log('------------ADD MODE------------')
    }
  });


  // handle input change
  const handleInputChange = (e, index) => {
    console.log('hi handleInputChange')
    const { name, value } = e.target;
    const newSalaryInfo = externalSalaryInfo.jsonBody.monthlySalaries;
    const list = [...newSalaryInfo];
    list[index][name] = value;
    const newJsonBody = {
      IDN: externalSalaryInfo.jsonBody.IDN,
      monthlySalaries: list
    }
    setExternalSalaryInfo({ ...externalSalaryInfo, jsonBody: newJsonBody });
    console.log(externalSalaryInfo);
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    console.log('hi handleRemoveClick')
    const newSalaryInfo = externalSalaryInfo.jsonBody.monthlySalaries;
    const list = [...newSalaryInfo];
    list.splice(index, 1);
    const newJsonBody = {
      IDN: externalSalaryInfo.jsonBody.IDN,
      monthlySalaries: list
    }
    setExternalSalaryInfo({ ...externalSalaryInfo, jsonBody: newJsonBody });
    console.log(externalSalaryInfo);
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    console.log('hi handleAddClick')
    const newSalaryInfo = externalSalaryInfo.jsonBody.monthlySalaries;
    const list = [...newSalaryInfo];
    list.push({ source: "", deptSalary: "" });
    const newJsonBody = {
      IDN: externalSalaryInfo.jsonBody.IDN,
      monthlySalaries: list
    }
    setExternalSalaryInfo({ ...externalSalaryInfo, jsonBody: newJsonBody });
    console.log(externalSalaryInfo);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('\\\\\\\\\\\\handleSubmit//////////')
    console.log(externalSalaryInfo)
    const currentUrl = externalSalaryInfo.edit ? myConstClass.Edit_MonthlySalaryInfo + externalSalaryInfo.id : myConstClass.ADD_MonthlySalaryInfo;
    console.log('currentUrl')
    console.log(currentUrl)
    // setExternalSalaryInfo({
    //   ...externalSalaryInfo,
    //   loading: true
    // }, () => {
    //   fetch(currentUrl
    //     , {
    //       method: 'POST',
    //       body: JSON.stringify(
    //         externalSalaryInfo.jsonBody
    //       ),
    //       headers: {
    //         'Accept': 'application/json',
    //         'Content-type': 'application/json'
    //       },
    //     })
    //     .then((response) => response.json())
    //     .then((json) => {
    //       console.log(json)
    //       props.history.push({
    //         pathname: '/addDebtInformation',
    //         state: {
    //           id: externalSalaryInfo.id,
    //           edit: externalSalaryInfo.edit,
    //           IDN: externalSalaryInfo.jsonBody.IDN,
    //           //  token: externalSalaryInfo.token
    //         }
    //       })
    //     });
    // })
    props.history.push({
      pathname: '/addDebtInformation',
      state: {
        id: externalSalaryInfo.id,
        edit: externalSalaryInfo.edit,
        IDN: externalSalaryInfo.jsonBody.IDN,
        //  token: externalSalaryInfo.token
      }
    })
  }

  const validate = Yup.object({
    health: Yup.array().of(
      Yup.object({
        source: Yup.string()
          //  .max(10, myConstClass.Less_10)
          .required(myConstClass.Required),
        salaryValue: Yup.number().positive().integer()
          //  .max(10, myConstClass.Less_10)
          .required(myConstClass.Required),
      })
    )
  })

  return (
    <div>
      <SideBar />
      {externalSalaryInfo.loading === true ?
        <Backdrop
          open>
          <CircularProgress color="inherit" />
        </Backdrop> :
        <Formik
          validateOnChange={true}
          initialValues={
            {
              IDN: props.location.state['IDN'],
              monthlySalaries: [
                {
                  source: "",
                  salaryValue: 0
                }],
            }
          }
          validationSchema={validate}
          onSubmit={values => {
            console.log('\\\\\\\\\\\\values//////////')
            console.log(values)
            const currentUrl = externalSalaryInfo.edit ? myConstClass.Edit_MonthlySalaryInfo + externalSalaryInfo.id : myConstClass.ADD_MonthlySalaryInfo;
            console.log('currentUrl')
            console.log(currentUrl)
            setExternalSalaryInfo({
              ...externalSalaryInfo,
              loading: true,
              jsonBody: values
            });
            fetch(currentUrl
              , {
                method: 'POST',
                body: JSON.stringify(
                  values
                ),
                headers: {
                  'Accept': 'application/json',
                  'Content-type': 'application/json'
                },
              })
              .then((response) => response.json())
              .then((json) => {
                console.log(json)
                props.history.push({
                  pathname: '/addDebtInformation',
                  state: {
                    id: externalSalaryInfo.id,
                    edit: externalSalaryInfo.edit,
                    IDN: values.IDN,
                    // token: healthyInfo.token
                  }
                })
              });
          }}
        >
          {formik => (
            <div className="rec-contain">
              <h4 className="h4-Reem"> إضافة راتب خارجي </h4>
              {console.log(formik.values)}
              <Form>
                <div className="rec-contain">
                  <div className="form-row r">
                    <FieldArray name="monthlySalaries">
                      {
                        arrayHelpers => (
                          <div>
                            {formik.values.monthlySalaries.map((salaryItem, index) => {
                              return (
                                <div className="form-row r" key={index}>
                                  <CustomTextField
                                    placeholder="المصدر"
                                    name={`monthlySalaries.${index}.source`}
                                  /> &nbsp;
                                  <CustomTextField
                                    placeholder="القيمة"
                                    name={`monthlySalaries.${index}.salaryValue`}
                                  />
                                      &nbsp;&nbsp;&nbsp;&nbsp;
                                      <div>
                                    {formik.values.monthlySalaries.length - 1 === index &&
                                      <IconButton aria-label="delete" >
                                        <AddCircleRounded fontSize="large"
                                          onClick={() =>
                                            arrayHelpers.push({
                                              source: "",
                                              salaryValue: "",
                                            })
                                          }
                                        />
                                      </IconButton>

                                      
                                      //           <button
                                      //             className="ReemButton" 
                                      //             onClick={() =>
                                      //               arrayHelpers.push({
                                      //                 source: "",
                                      //                 salaryValue: "",
                                      //                 // id: "" + Math.random()
                                      //               })
                                      //             }
                                      //           >
                                      //             إضافة
                                      // </button>
                                    } 
                            {formik.values.monthlySalaries.length !== 1 &&
                                      <IconButton aria-label="delete" >
                                        <CancelOutlined fontSize="large"
                                          onClick={() => arrayHelpers.remove(index)}></CancelOutlined>
                                      </IconButton>
                                      // < button
                                      //   className="btn btn-primary btn-rounded mr"
                                      //   onClick={() => arrayHelpers.remove(index)}>
                                      //   حذف  </button>
                                    }
                                  </div>
                                </div>
                              );
                            })}
                          </div>
                        )
                      }
                    </FieldArray>
                  </div>
                  <button
                    className="ReemButton" 
                    type="submit">
                    تأكيد
            </button>
                </div>
              </Form>
            </div>
          )}
        </Formik>
      }
    </div>
  );
}

export default OutSalaryInforamation;

/*  <div className="contain">
      <center>
        <form>
          <h6 className="h6-h2"> إضافة راتب خارجي  </h6>
          <div className="p-t" >
            {externalSalaryInfo.jsonBody.monthlySalaries.map((salary, index) => {
              return (
                <div className="box" key={index}>
                  <input className="input-f"
                    name="source"
                    placeholder={salary.source === '' ?
                      "المصدر" :
                      salary.source}
                    value={salary.source}
                    onChange={e => handleInputChange(e, index)}
                  />
                  <input
                    className="input-f"
                    name="salaryValue"
                    placeholder={salary.salaryValue === 0 ?
                      "القيمة" :
                      salary.salaryValue}
                    type="text" pattern="[0-9]*"
                    value={salary.salaryValue}
                    onChange={e => handleInputChange(e, index)}
                  />

                  {externalSalaryInfo.jsonBody.monthlySalaries.length !== 1 &&
                    <button className="btn btn-primary btn-rounded mr"
                      onClick={() => handleRemoveClick(index)}>حذف</button>}
                  {externalSalaryInfo.jsonBody.monthlySalaries.length - 1 === index &&
                    <button className="btn btn-primary btn-rounded mr"
                      onClick={handleAddClick}>إضافة</button>}

                </div>
              );
            })}
          </div>

        </form>
      </center>
      <br />
      <br />
      <button
        onClick={(event) => handleSubmit(event)}
        className="next"
        type="submit"
      /*  to={{
         pathname: '/addDebtInformation',
         state: {
           id: externalSalaryInfo.id,
           edit: externalSalaryInfo.edit,
           IDN: externalSalaryInfo.IDN
         }
       }}
       > التالي </button>
       <br />
       <br />
       <Link
         className="next"
         type="submit"
         to={{
           pathname: '/addDebtInformation',
           state: {
             id: externalSalaryInfo.id,
             edit: externalSalaryInfo.edit,
             IDN: externalSalaryInfo.jsonBody.IDN
           }
         }}> تخطي </Link>
       {/*  <a href="/dept" className="next" type="submit">التالي  &raquo; </a>
       <br />
       <br />
       <a href="/dept" className="next" type="submit">تخطي  &raquo; </a>
     </div>
    */