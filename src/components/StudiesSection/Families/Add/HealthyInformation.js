import React, { useState, useEffect } from "react";
import { Container } from 'react-bootstrap';
import './PersonalInformation.css';
import DatePicker from 'react-date-picker';
import '../../../Routes';
import { AddBox, AddBoxRounded, AddCircleRounded, AddRounded, Cancel, CancelOutlined, CancelScheduleSend, CancelSharp, DeleteSharp, LocalSee, RemoveCircleOutline } from '@material-ui/icons';

import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';


import IconButton from '@material-ui/core/IconButton';

import SideBar from '../../../SideBar/SideBar';
///////

import {
  Formik,
  Field,
  Form,
  useField,
  FieldAttributes,
  FieldArray
} from "formik";
import {
  TextField,
  Button,
  Checkbox,
  Radio,
  FormControlLabel,
  Select,
  MenuItem, InputBase,
  InputLabel,
  TextareaAutosize, FormControl
} from "@material-ui/core";
import { CustomTextField } from '../../../TextFields/TextField';
/////

//import { MyTextField } from '../../TextFields/TextField';
import * as Yup from 'yup';
import { Box, Divider } from '@material-ui/core';
import { createStyles, makeStyles, withStyles, Theme } from '@material-ui/core/styles';

function HealthyInformation(props) {


  const [healthyInfo, setHealthyInfo] = useState(
    {
      loading: false,
      id: props.location.state['id'],
      edit: props.location.state['edit'],
      // token: props.location.state['token'],
      jsonBody: {
        IDN: props.location.state['IDN'],
        health: [{
          healthProblem: '',
          physician: '',
          therapy: '',
          monthlyCost: '',
          medicineCost: '',
          notes: ''
        }]
      }
    }
  );

  const healthyInfoGet = async () => {
    setHealthyInfo({ ...healthyInfo, loading: true });
    const res = await axios.get(myConstClass.GET_HealthyInfo + healthyInfo.id)
    const newjsonBody = {
      IDN: healthyInfo.jsonBody.IDN,
      health: res.data['data']
    }
    setHealthyInfo({ ...healthyInfo, jsonBody: newjsonBody, loading: false });
    console.log('healthyInfo')
    console.log(healthyInfo.jsonBody)
  }

  useEffect(() => {
    console.log('useEffect')
    console.log(healthyInfo)
    if (healthyInfo.edit) {
      console.log('------------EDIT MODE------------')
      healthyInfoGet();
    }
    else {
      console.log('------------ADD MODE------------')
    }
  });

  // handle input change
  const handleInputChange = (e, index) => {
    console.log('hi handleInputChange')
    const { name, value } = e.target;
    const newHealthyInfo = healthyInfo.jsonBody.health;
    const list = [...newHealthyInfo];
    list[index][name] = value;
    const newJsonBody = {
      IDN: healthyInfo.jsonBody.IDN,
      health: list
    }
    setHealthyInfo({ ...healthyInfo, jsonBody: newJsonBody });
    console.log(healthyInfo);
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    console.log('hi handleRemoveClick')
    const newHealthyInfo = healthyInfo.jsonBody.health;
    const list = [...newHealthyInfo];
    list.splice(index, 1);
    const newJsonBody = {
      IDN: healthyInfo.jsonBody.IDN,
      health: list
    }
    setHealthyInfo({ ...healthyInfo, jsonBody: newJsonBody });
    console.log(healthyInfo);
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    console.log('hi handleAddClick')
    const newHealthyInfo = healthyInfo.jsonBody.health;
    const list = [...newHealthyInfo];
    list.push({
      healthProblem: "", physician: "",
      therapy: "", monthlyCost: "", medicineCost: "", notes: ""
    });
    const newJsonBody = {
      IDN: healthyInfo.jsonBody.IDN,
      health: list
    }
    setHealthyInfo({ ...healthyInfo, jsonBody: newJsonBody });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('\\\\\\\\\\\\handleSubmit//////////')
    console.log(healthyInfo)
    // const currentUrl = healthyInfo.edit ? myConstClass.Edit_HealthyInfo + healthyInfo.id : myConstClass.ADD_HealthyInfo;
    // console.log('currentUrl')
    // console.log(currentUrl)
    // setHealthyInfo({
    //   ...healthyInfo,
    //   loading: true
    // }, () => {
    //   fetch(currentUrl
    //     , {
    //       method: 'POST',
    //       body: JSON.stringify(
    //         healthyInfo.jsonBody
    //       ),
    //       headers: {
    //         'Accept': 'application/json',
    //         'Content-type': 'application/json'
    //       },
    //     })
    //     .then((response) => response.json())
    //     .then((json) => {
    //       console.log(json)
    props.history.push({
      pathname: '/addOutSalaryInforamation',
      state: {
        ///      id: healthyInfo.id,
        //   edit: healthyInfo.edit,
        // IDN: healthyInfo.jsonBody.IDN,
        //    token: healthyInfo.token
      }
    })
    //});
    // })
  }

  const validate = Yup.object({
    health: Yup.array().of(
      Yup.object({
        healthProblem: Yup.string()
          //  .max(10, myConstClass.Less_10)
          .required(myConstClass.Required),
        physician: Yup.string()
          //  .max(10, myConstClass.Less_10)
          .required(myConstClass.Required),
        therapy: Yup.string()
          //  .max(10, myConstClass.Less_10)
          .required(myConstClass.Required),
        monthlyCost: Yup.string()
          //  .max(10, myConstClass.Less_10)
          .required(myConstClass.Required),
        medicineCost: Yup.string()
          //  .max(10, myConstClass.Less_10)
          .required(myConstClass.Required),
        notes: Yup.string()
          //     .max(30, myConstClass.Less_30)
          .required(myConstClass.Required),
      })
    )
  })

  return (
    <div>
      <SideBar />
      {healthyInfo.loading === true ?
        <Backdrop
          open>
          <CircularProgress color="inherit" />
        </Backdrop> :
        <Formik
          initialValues={
            {
              IDN: props.location.state['IDN'],
              health: [{
                healthProblem: '',
                physician: '',
                therapy: '',
                monthlyCost: '',
                medicineCost: '',
                notes: ''
              }]
            }
          }
          validationSchema={validate}
          onSubmit={values => {
            console.log('\\\\\\\\\\\\values//////////')
            console.log(values)
            const currentUrl = healthyInfo.edit ? myConstClass.Edit_HealthyInfo + healthyInfo.id : myConstClass.ADD_HealthyInfo;
            console.log('currentUrl')
            console.log(currentUrl)
            setHealthyInfo({
              ...healthyInfo,
              loading: true,
              jsonBody: values
            });
            fetch(currentUrl
              , {
                method: 'POST',
                body: JSON.stringify(
                  values
                ),
                headers: {
                  'Accept': 'application/json',
                  'Content-type': 'application/json'
                },
              })
              .then((response) => response.json())
              .then((json) => {
                console.log(json)
                props.history.push({
                  pathname: '/addOutSalaryInforamation',
                  state: {
                    id: healthyInfo.id,
                    edit: healthyInfo.edit,
                    IDN: values.IDN,
                    // token: healthyInfo.token
                  }
                })

              });

          }}
        >
          {formik => (
            <div className="rec-contain" >
              <h4 className="h4-Reem">إضافة المعلومات الصحية</h4>
              {console.log(formik.values)}
              <Form>
                <div className="rec-contain">
                  <Box marginRight={2} marginBottom={5} >
                    <div className="form-row r">
                      <FieldArray name="health">
                        {
                          arrayHelpers => (
                            <div>
                              {formik.values.health.map((healthItem, index) => {
                                return (
                                  <div className="form-row r" key={index}>
                                    <CustomTextField
                                      placeholder="المشكلة الصحية"
                                      name={`health.${index}.healthProblem`}
                                    /> &nbsp;
                                    <CustomTextField
                                      placeholder="الطبيب المعالج"
                                      name={`health.${index}.physician`}
                                    /> &nbsp;
                                    <CustomTextField
                                      placeholder="العلاج"
                                      name={`health.${index}.therapy`}
                                    /> &nbsp;
                                    <CustomTextField
                                      placeholder="التكلفة الشهرية"
                                      name={`health.${index}.monthlyCost`}
                                    /> &nbsp;
                                    <CustomTextField
                                      placeholder="كلفة الدواء"
                                      name={`health.${index}.medicineCost`}
                                    /> &nbsp;
                                    <CustomTextField
                                      placeholder="ملاحظات"
                                      name={`health.${index}.notes`}
                                      type="input"
                                      as={TextareaAutosize}
                                    /> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <div  >
                                      {formik.values.health.length - 1 === index &&
                                        <IconButton aria-label="delete" >
                                          <AddCircleRounded fontSize="large"
                                            onClick={() =>
                                              arrayHelpers.push({
                                                medicineCost: 0,
                                                monthlyCost: 0,
                                                therapy: "",
                                                physician: "",
                                                healthProblem: "",
                                                notes: "",
                                              })
                                            }
                                          />
                                        </IconButton>
                                        //           <button
                                        //             className="btn btn-primary btn-rounded mr"
                                        //             onClick={() =>
                                        //               arrayHelpers.push({
                                        //                 medicineCost: 0,
                                        //                 monthlyCost: 0,
                                        //                 therapy: "",
                                        //                 physician: "",
                                        //                 healthProblem: "",
                                        //                 notes: "",
                                        //                 // id: "" + Math.random()
                                        //               })
                                        //             }
                                        //           >
                                        //             إضافة
                                        // </button>
                                      }
                              &nbsp;
                              {formik.values.health.length !== 1 &&
                                        <IconButton aria-label="delete" >
                                          <CancelOutlined fontSize="large"
                                            onClick={() => arrayHelpers.remove(index)}></CancelOutlined>
                                        </IconButton>
                                        // < button
                                        //   className="btn btn-primary btn-rounded mr"
                                        //   onClick={() => arrayHelpers.remove(index)}>
                                        //   حذف  </button>
                                      }
                                    </div>
                                  </div>
                                );
                              })}
                            </div>
                          )
                        }
                      </FieldArray>
                    </div>
                    <button className="ReemButton"
                      type="submit">
                      تأكيد
              </button>

                  </Box>
                </div>
              </Form>
            </div>
          )}
        </Formik>
      }
    </div>
  );
}
export default HealthyInformation; 