import React, { Component } from 'react';
import './PersonalInformation.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';
import { useState, useEffect } from "react";

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import SideBar from '../../../SideBar/SideBar';
function ChildList(props) {

  const [childListInfo, setChildListInfoInfo] = useState(
    {
      id: props.location.state['id'],
      edit: props.location.state['edit'],
      IDN: props.location.state['IDN'],
      // token: props.location.state['token'],
      children: [
        /*  {
             firstName: "",
            fatherName: "",
            lastName: "",
            motherName: "",
            birthPlace: "",
            birthDate: "",
            nationality: "",
            wifeIDN: 0,
            IDNPlace: "",
            address: "",
            education: "",
            lastCertificate: "",
            certificateSource: "",
            currentWork: "",
            currentWorkAddress: "",
            currentWorkPhone: 0,
            healthProblem: "",
            physician: "",
            therapy: "",
            monthlyCost: "",
            medicineCost: "",
            notes: "" 
          }*/
      ]
    }
  );

  const childrenInfoGet = async () => {
    setChildListInfoInfo({ ...childListInfo, loading: true });
    const res = await axios.get(myConstClass.GET_ChildInfo + childListInfo.IDN)
    setChildListInfoInfo({ ...childListInfo, children: res.data['data'], loading: false });
    console.log('childListInfo')
    console.log(childListInfo.children)
  }

  useEffect(() => {
    console.log('useEffect')
    if (childListInfo.children.length > 0) {
      console.log('---------------')
      childrenInfoGet();
    }
  });

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    console.log('hi handleRemoveClick')
    const newchildListInfo = childListInfo.children;
    const list = [...newchildListInfo];
    list.splice(index, 1);
    setChildListInfoInfo({ ...childListInfo, children: list });
    console.log(childListInfo);
  };

  return (
    <div>
      <SideBar />
      <div>
        <center>
          <table className="show-table" >
            <thead  >
              <tr>
                <th>الرقم</th>
                <th>اسم الطفل</th>
                <th>العملية</th>
              </tr>
            </thead>
            <tbody>
              {
                Array.isArray(childListInfo.children) ?
                  (childListInfo.children.length > 0) ? childListInfo.children.map((person, index) => {
                    return (
                      <tr key={index}>
                        <td> {index+1} </td>
                        <td> {person.name}</td>
                        <td>  <Link
                          onClick={(index) => handleRemoveClick(index)}> حذف </Link>
                          <Link to={{
                            pathname: '/addChildrenInformation',
                            state: {
                              id: childListInfo.id,
                              edit: true,
                              IDN: childListInfo.IDN,
                              patrners: childListInfo.children,
                            }
                          }}> تعديل </Link>
                        </td>
                      </tr>
                    )
                  }) : <tr><td colSpan="5">Loading...</td></tr>
                  : <tr><td colSpan="5">Loading...</td></tr>}
            </tbody>
          </table>

        </center>
        <Link
          className="ReemButton"
          type="submit"
          to={{
            pathname: '/addChildrenInformation',
            state: {
              id: childListInfo.id,
              // add a partner is always in add mode
              edit: false,
              IDN: childListInfo.IDN,
              // I send all partners bcause the api request is there not here
              patrners: childListInfo.children,
              //token: childListInfo.token
            }
          }}> إضافة طفل </Link>
        <Link
          className="ReemButton"
          type="submit"
          to={{
            pathname: '/homePage',
            state: {
              // id: childListInfo.id,
              // edit: childListInfo.edit,
              // IDN: childListInfo.IDN,
              // token: childListInfo.token
            }
          }}
        > التالي </Link> 
        <br />
        <br />
      </div>
    </div>
  );
}
export default ChildList;
/*  <center>
      <h6> قائمة الشركاء </h6>
      <br />
      <table className="table-t table-bordered">
        <thead>
          <tr>
            <th scope="col">الرقم</th>
            <th scope="col"> اسم الطفل</th>
            <th scope="col">العملية</th>
          </tr>
        </thead>
        <tbody>
          {(childListInfo.children.length > 0) ? childListInfo.children.map((child, index) => {
            return (
              <tr key={index}>
                <td> {child.id} </td>
                <td> {child.name}</td>
                <td>  <Link
                  onClick={(index) => handleRemoveClick(index)}> حذف </Link>
                  <Link to={{
                    pathname: '/addChildrenInformation',
                    state: {
                      id: childListInfo.id,
                      edit: true,
                      IDN: childListInfo.IDN,
                      children: childListInfo.children,
                      childIndex: index,
                      token: childListInfo.token
                    }
                  }}> تعديل </Link>
                </td>
              </tr>
            )
          }) : <tr><td colSpan="5">الرجاء الانتظار...</td></tr>}
        </tbody>
      </table>
      <center>
        <Link
          className="next"
          type="submit"
          to={{
            pathname: '/addChildrenInformation',
            state: {
              id: childListInfo.id,
              // add a partner is always in add mode
              edit: false,
              IDN: childListInfo.IDN,
              // I send all partners bcause the api request is there not here
              children: childListInfo.children,
            //  token: childListInfo.token
            }
          }}> إضافة طفل </Link>
      </center>
      <br />
      <Link
        className="next"
        type="submit"
        to={{
          pathname: '/homePage',
          state: {
          }
        }}
      > العودة للصفحة الرئيسية </Link>
    </center>
  */