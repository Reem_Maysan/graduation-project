import { Container } from 'react-bootstrap';
//import './wifermation.css';
import DatePicker from 'react-date-picker';
import HealthyInformation from './HealthyInformation';
import { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { createStyles, makeStyles, withStyles, Theme } from '@material-ui/core/styles';

import {
  Formik,
  Field,
  Form,
  useField,
  FieldAttributes,
  FieldArray
} from "formik";
import {
  TextField,
  Button,
  Checkbox,
  Radio,
  FormControlLabel,
  Select,
  Box, Divider,
  MenuItem, InputBase,
  InputLabel,
  TextareaAutosize, FormControl
} from "@material-ui/core";


import { AddBox, AddBoxRounded, AddCircleRounded, AddRounded, Cancel, CancelOutlined, CancelScheduleSend, CancelSharp, DeleteSharp, LocalSee, RemoveCircleOutline } from '@material-ui/icons';

import IconButton from '@material-ui/core/IconButton';

import SideBar from '../../../SideBar/SideBar';

import { CustomTextField } from '../../../TextFields/TextField';

import * as Yup from 'yup';
import { date } from 'yup/lib/locale';

function PartnerInformation(props) {

  const [partnerState, setPartnerState] = useState(
    {
      loading: false,
      id: props.location.state['id'],
      edit: props.location.state['edit'],
      wifeIDN: props.location.state['wifeIDN'],
      token: props.location.state['token'],
      wife:
      {
        firstName: "",
        fatherName: "",
        lastName: "",
        motherName: "",
        birthPlace: "",
        birthDate: "",
        nationality: "",
        wifeIDN: 0,
        IDNPlace: "",
        address: "",
        education: "",

        lastCertificate: "",
        certificateSource: "",
        currentWork: "",
        currentWorkAddress: "",
        currentWorkPhone: 0,

        healthProblem: "",
        physician: "",
        therapy: "",
        monthlyCost: "",
        medicineCost: "",
        notes: ""
      },
      jsonBody: {
        IDN: props.location.state['IDN'],
        wifes: props.location.state['patrners'],
      }
    });

  const partnerInfoGet = () => {
    // to view wife's info in edit mode
    setPartnerState({ ...partnerState, loading: true });
    partnerState.jsonBody.wifes.forEach(element => {
      if (element.wifeIDN === partnerState.wifeIDN) {
        console.log('WE FOUND IT')
        console.log(element.wifeIDN)
        setPartnerState({ ...partnerState, wife: element, loading: false });
      }
    });
    console.log('partnerState')
    console.log(partnerState.jsonBody)
  }

  useEffect(() => {
    console.log('useEffect')
    console.log(partnerState)
    if (partnerState.edit) {
      console.log('------------EDIT MODE------------')
      partnerInfoGet();
    }
    else {
      console.log('------------ADD MODE------------')
    }
  });

  const handleChange = (event) => {
    console.log('hiiiiii from handleChange');
    const newPartnerState = partnerState.wife;
    newPartnerState[event.target.id] = event.target.value;
    setPartnerState({ ...partnerState, wife: newPartnerState })
    console.log(partnerState)
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('\\\\\\\\\\\\handleSubmit//////////')
    console.log(partnerState)
    const newWife = partnerState.wife;
    const allWives = partnerState.jsonBody.wifes;
    const list = [...allWives]
    list.push(newWife);
    console.log('000000000000000list00000000000000')
    console.log('newWife / allWives / list/ newJson')
    console.log(newWife)
    console.log(allWives)
    console.log(list)
    const newJson = {
      IDN: partnerState.jsonBody.IDN,
      wifes: list
    };
    console.log(newJson)
    setPartnerState({ ...partnerState, jsonBody: newJson });
    console.log('partnerState')
    console.log(partnerState)
    const currentUrl = partnerState.edit ? myConstClass.Edit_WifeInfo + partnerState.id : myConstClass.ADD_WifeInfo;
    console.log('jsonBody')
    console.log(partnerState.jsonBody)

    console.log('currentUrl')
    console.log(currentUrl)
    setPartnerState({ ...partnerState, loading: true }, () => {
      fetch(currentUrl
        , {
          method: 'POST',
          body: JSON.stringify(
            partnerState.jsonBody
          ),
          headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
          },
        })
        .then((response) => response.json())
        .then((json) => {
          setPartnerState({ ...partnerState, loading: false })
          console.log(json)
          props.history.push({
            pathname: '/addPartnerList',
            state: {
              //    id: partnerState.id,
              //  edit: partnerState.edit,
              //IDN: partnerState.jsonBody.IDN,
              // token: partnerState.token
            }
          })
        });
    });
  }
   
  const validate = Yup.object({

    firstName: Yup.string()
      // .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),

    fatherName: Yup.string()
      //  .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),

    lastName: Yup.string()
      //   .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    motherName: Yup.string()
      // .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),

    birthPlace: Yup.string()
      // .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),

    birthDate: Yup.string()
      //  .max(10, myConstClass.Date_Format)
      .required(myConstClass.Required),

    nationality: Yup.string()
      // .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),

    IDNPlace: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    wifeIDN: Yup.number().integer().positive()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    education: Yup.string()
      // .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),

    address: Yup.string()
      //     .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

    lastCertificate: Yup.string()
      // .max(10, myConstClass.Less_10)
      .required(myConstClass.Required),

    certificateSource: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    currentWork: Yup.string()
      // .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

    currentWorkAddress: Yup.string()
      // .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

    currentWorkPhone: Yup.number().integer().positive()
      //.min(10, myConstClass.Less_10)
      .required(myConstClass.Required),

    healthProblem: Yup.string()
      //     .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

    physician: Yup.string()
      // .max(10, myConstClass.Less_10)
      .required(myConstClass.Required),

    therapy: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    monthlyCost: Yup.string()
      // .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

    medicineCost: Yup.string()
      // .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

    notes: Yup.string()
      //.min(10, myConstClass.Less_10)
      .required(myConstClass.Required),
  })

  return (
    <div>
      <SideBar />
      {partnerState.loading === true ?
        <Backdrop
          open>
          <CircularProgress color="inherit" />
        </Backdrop> :
        <Formik
          validateOnChange={true}
          initialValues={
            {
              firstName: "",
              fatherName: "",
              lastName: "",
              motherName: "",
              birthPlace: "",
              birthDate: "",
              nationality: "",
              wifeIDN: 0,
              IDNPlace: "",
              address: "",
              education: "",
              lastCertificate: "",
              certificateSource: "",
              currentWork: "",
              currentWorkAddress: "",
              currentWorkPhone: 0,

              healthProblem: "",
              physician: "",
              therapy: "",
              monthlyCost: "",
              medicineCost: "",
              notes: ""
            }
          }
          validationSchema={validate}
          onSubmit={values => {
            console.log('\\\\\\\\\\\\values//////////')
            console.log(values)
            const currentUrl = partnerState.edit ? myConstClass.Edit_WifeInfo + partnerState.id : myConstClass.ADD_WifeInfo;
            console.log('currentUrl')
            console.log(currentUrl)
            setPartnerState({
              ...partnerState,
              loading: true,
              wife: values
            });
            fetch(currentUrl
              , {
                method: 'POST',
                body: JSON.stringify(
                  values
                ),
                headers: {
                  'Accept': 'application/json',
                  'Content-type': 'application/json'
                },
              })
              .then((response) => response.json())
              .then((json) => {
                console.log(json)
                props.history.push({
                  pathname: '/addPartnerList',
                  state: {
                    //id: partnerState.id,
                    // edit: partnerState.edit,
                    // IDN: partnerState.IDN,
                    //token: partnerState.token
                  }
                })
              });
          }}
        >
          {formik => (
            <div>
              {console.log(formik.values)}
              <Form>
                <div className="rec-contain">
                  <h4 className="h4-Reem">المعلومات الشخصية للشريك</h4>
                  <div className="column border-left">
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الاسم الأول :</label>
                &nbsp;
                <CustomTextField
                          name="firstName" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >اسم الأم :</label>&nbsp;
                <CustomTextField

                          name="motherName" />
                      </div></Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >اسم الأب :</label>&nbsp;
                <CustomTextField

                          name="fatherName" />
                      </div></Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الكنية :</label>&nbsp;
                <CustomTextField

                          name="lastName" />
                      </div> </Box>

                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >اسم الأم :</label>&nbsp;
                <CustomTextField

                          name="motherFirstName" />
                      </div></Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >مكان الولادة :</label>&nbsp;
                <CustomTextField

                          name="birthPlace" />
                      </div>  </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >العمل الحالي :</label>&nbsp;
                <CustomTextField

                          name="currentWork" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >هاتف العمل الحالي :</label>&nbsp;
                <CustomTextField

                          name="currentWorkPhone" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >عنوان العمل الحالي :</label>&nbsp;
                <CustomTextField

                          name="currentWorkAddress" />
                      </div>
                    </Box>

                  </div>
                  <div className="column border-left">
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >رقم الهوية :</label>&nbsp;
                <CustomTextField

                          name="wifeIDN" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >مكان الهوية :</label>&nbsp;
                <CustomTextField

                          name="IDNPlace" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >تاريخ الميلاد :</label>&nbsp;
                <CustomTextField

                          name="birthDate" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >العنوان :</label>&nbsp;
                <CustomTextField

                          name="address" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >التعليم :</label>&nbsp;
                <CustomTextField

                          name="education" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" > الشهادة :</label>&nbsp;
                <CustomTextField

                          name="lastCertificate" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" > مصدر الشهادة :</label>&nbsp;
                <CustomTextField

                          name="certificateSource" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >ملاحظات :</label>&nbsp;
                <CustomTextField 
                          name="notes" /> 
                      </div> </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الجنسية :</label>&nbsp;
                <CustomTextField 
                          name="nationality" /> 
                      </div> </Box>
                  </div>
                </div>
                <div className="rec-contain">
                  <h4 className="h4-Reem"> المعلومات الصحية للشريك
                  </h4>
                  <div className="column border-left">
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >المشكلة الصحية :</label>&nbsp;
                <CustomTextField

                          name="healthProblem" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الطبيب المعالج :</label>&nbsp;
                <CustomTextField

                          name="physician" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >العلاج :</label>&nbsp;
                <CustomTextField

                          name="therapy" />
                      </div>
                    </Box>
                  </div>
                  <div className="column border-left">
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >التكلفة الشهرية :</label>&nbsp;
                <CustomTextField

                          name="monthlyCost" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >كلفة الدواء :</label>&nbsp;
                <CustomTextField

                          name="medicineCost" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >ملاحظات :</label>&nbsp;
                <CustomTextField

                          name="description" />
                      </div>
                    </Box>
                  </div>

                </div>
                <button
                  className="ReemButton"
                  type="submit">
                  تأكيد
      </button>

              </Form>
            </div>
          )}
        </Formik>
      }
    </div>
  )
}
export default PartnerInformation;