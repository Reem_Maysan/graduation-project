import { Container } from 'react-bootstrap';
import './PersonalInformation.css';
import DatePicker from 'react-date-picker';
import React, { useState, useEffect } from "react";
//import useForm from 'react-hook-form';
import '../../../Routes';
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';
import { AddBox, AddBoxRounded, AddCircleRounded, AddRounded, Cancel, CancelOutlined, CancelScheduleSend, CancelSharp, DeleteSharp, LocalSee, RemoveCircleOutline } from '@material-ui/icons';


import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';


import DeleteIcon from '@material-ui/icons/Delete';
import { MyTextField } from '../../../TextFields/formikTextField';

import SideBar from '../../../SideBar/SideBar';

import {
  Formik,
  Field,
  Form,
  useField,
  FieldAttributes,
  FieldArray
} from "formik";
import {
  TextField,
  Button,
  Checkbox,
  Radio,
  FormControlLabel,
  Select,
  MenuItem, InputBase,
  InputLabel,
  TextareaAutosize, FormControl
} from "@material-ui/core";
import { CustomTextField } from '../../../TextFields/TextField';
/////


import Typography from '@material-ui/core/Typography';

import * as Yup from 'yup';
import { Box, Divider } from '@material-ui/core';
import { createStyles, makeStyles, withStyles, Theme } from '@material-ui/core/styles';


import IconButton from '@material-ui/core/IconButton';

function ResidentInformation(props) {

  const BootstrapInput = withStyles((theme) => ({
    root: {
      'label + &': {
        marginTop: theme.spacing(0),
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 16,
      textAlign: 'center',
      padding: '10px 26px 10px 12px',
      width: 120,
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase);

  const useStyles = makeStyles((theme) => ({
    margin: {
      margin: theme.spacing(0),
    },
  }));

  const classes = useStyles();


  // reemy
  const [dataState, setDataState] = useState({
    loading: false,
    id: props.location.state['id'],
    edit: props.location.state['edit'],
    // token: props.location.state['token'],
    residentInfo: {
      residentStatus: "ملك",
      monthlyRent: '',
      houseArea: '',
      roomsNumber: '',
      residentsNumber: '',
      houseType: '',
      humidity: '',
      upholsteryStatus: '',
      calddingStatus: '',
      residencePhone: '',
      residenceOwner: '',
      notes: '',
      IDN: props.location.state['IDN'],
      rebuilding: [{
        title: "",
        description: ""
      }]
    }
  });

  const residentInfoGet = async () => {
    console.log('hi from residentInfoGet')
    console.log(dataState.id, dataState.edit)
    setDataState({ ...dataState, loading: true });
    const res = await axios.get(myConstClass.GET_ResidenceInfo + dataState.id)
    setDataState({ ...dataState, residentInfo: res.data['data'], loading: false });
    console.log('residentInfo')
    console.log(dataState.residentInfo)
  }

  useEffect(() => {
    console.log('useEffect')
    console.log(dataState)
    if (dataState.edit) {
      console.log('------------EDIT MODE------------')
      residentInfoGet();
    }
    else {
      console.log('------------ADD MODE------------')
    }
  }, []);

  const handleChange = (event) => {
    console.log('hiiiiii from handleChange');
    const newResidentInfo = dataState.residentInfo;
    newResidentInfo[event.target.name] = event.target.value;
    setDataState({ ...dataState, residentInfo: newResidentInfo });
    console.log(dataState)
  }

  const handleInputChange = (e, index) => {
    console.log('hi handleInputChange')
    const { name, value } = e.target;
    const newResidentInfo = dataState.residentInfo;
    const newRebuilding = newResidentInfo.rebuilding;
    const list = [...newRebuilding];
    list[index][name] = value;
    newResidentInfo.rebuilding = list;
    console.log("******************After_Rebuilding********************");
    setDataState({ ...dataState, residentInfo: newResidentInfo });
    console.log(dataState)
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    console.log('hi handleRemoveClick')
    console.log('index', index)
    //const list = [...rebuildingState];
    const newResidentInfo = dataState.residentInfo;
    const newRebuilding = newResidentInfo.rebuilding;
    const list = [...newRebuilding];
    list.splice(index, 1);
    newResidentInfo.rebuilding = list;
    setDataState({ ...dataState, residentInfo: newResidentInfo });
    console.log(dataState)
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    console.log('hi handleAddClick')
    const newResidentInfo = dataState.residentInfo;
    const newRebuilding = newResidentInfo.rebuilding;
    const list = [...newRebuilding];
    list.push({ title: "", description: "" });
    newResidentInfo.rebuilding = list;
    setDataState({ ...dataState, residentInfo: newResidentInfo });
    console.log(dataState)
    console.log(dataState)

  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('\\\\\\\\\\\\handleSubmit//////////')
    console.log(dataState)
    const currentUrl = dataState.edit ? myConstClass.Edit_ResidenceInfo + dataState.id : myConstClass.ADD_ResidenceInfo;
    console.log('currentUrl')
    console.log(currentUrl)
    setDataState({
      ...dataState,
      loading: true
    }, () => {
      fetch(currentUrl
        , {
          method: 'POST',
          body: JSON.stringify(
            dataState.residentInfo
          ),
          headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json'
          },
        })
        .then((response) => response.json())
        .then((json) => {
          console.log(json)
          props.history.push({
            pathname: '/addHealthyInformation',
            state: {
              id: dataState.id,
              edit: dataState.edit,
              IDN: dataState.residentInfo.IDN,
              //        token: dataState.token
            }
          })
        });
    })
  }

  const validate = Yup.object({
    monthlyRent: Yup.number().positive().integer()
      //.max(15)
      .required(myConstClass.Required),
    houseArea: Yup.number().positive().integer()
      // .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),
    roomsNumber: Yup.number().positive().integer()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),
    residentsNumber: Yup.number().positive().integer()
      //  .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),
    houseType: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),
    residencePhone: Yup.number().positive().integer()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),
    residenceOwner: Yup.string()
      // .max(10, myConstClass.Date_Format)
      .required(myConstClass.Required),
    notes: Yup.string()
      // .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),
    rebuilding: Yup.array().of(
      Yup.object({
        title: Yup.string()
          //  .max(10, myConstClass.Less_10)
          .required(myConstClass.Required),
        description: Yup.string()
          //     .max(30, myConstClass.Less_30)
          .required(myConstClass.Required),
      })
    )
  })

  return (
    <div>
      <SideBar />
      {dataState.loading === true ?
        <Backdrop
          open>
          <CircularProgress color="inherit" />
        </Backdrop> :
        <Formik
          initialValues={
            {
              residentStatus: "ملك",
              monthlyRent: '',
              houseArea: '',
              roomsNumber: '',
              residentsNumber: '',
              houseType: '',
              humidity: '',
              upholsteryStatus: '',
              calddingStatus: '',
              residencePhone: '',
              residenceOwner: '',
              notes: '',
              IDN: props.location.state['IDN'],
              rebuilding: [{
                title: "",
                description: ""
              }]
            }
          }
          validationSchema={validate}
          onSubmit={values => {
            console.log('\\\\\\\\\\\\values//////////')
            console.log(values)
            const currentUrl = dataState.edit ? myConstClass.Edit_ResidenceInfo + dataState.id : myConstClass.ADD_ResidenceInfo;
            console.log('currentUrl')
            console.log(currentUrl)
            setDataState({
              ...dataState,
              loading: true,
              residentInfo: values
            });
            fetch(currentUrl
              , {
                method: 'POST',
                body: JSON.stringify(
                  values
                ),
                headers: {
                  'Accept': 'application/json',
                  'Content-type': 'application/json'
                },
              })
              .then((response) => response.json())
              .then((json) => {
                console.log(json)
                props.history.push({
                  pathname: '/addHealthyInformation',
                  state: {
                    id: dataState.id,
                    edit: dataState.edit,
                    IDN: dataState.residentInfo.IDN,
                    // token: dataState.token
                  }
                })

              });

          }}
        >
          {formik => (
            <div className="rec-contain" >
              <h4 className="h4-Reem"> إضافة معلومات السكن </h4>
              {console.log(formik.values)}
              <Form>
                <div className="column border-left">
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >الآجار الشهري :</label>
                        &nbsp;
                          <CustomTextField
                        // placeholder={values.monthlyRent}
                        //"مثال: 500.000" 
                        name="monthlyRent" />
                    </div>
                  </Box>
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" > منطقة المنزل :</label>&nbsp;
                        <CustomTextField
                        //  placeholder={values.houseArea}
                        //"مثال: 37" 
                        name="houseArea" />
                    </div></Box>
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >عدد الغرف :</label>&nbsp;
                        <CustomTextField
                        // placeholder={values.roomsNumber}
                        //"مثال: 3" 
                        name="roomsNumber" />
                    </div> </Box>

                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >نوع المنزل :</label>&nbsp;
                        <CustomTextField
                        //placeholder={values.houseType}
                        //"مثال: بيت عربي" 
                        name="houseType" />
                    </div></Box>
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >رقم المنزل :</label> &nbsp;
                        <CustomTextField
                        //  placeholder={values.residentsNumber}
                        //"مثال: 37" 
                        name="residentsNumber" />
                    </div> </Box>
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >هاتف المنزل :</label>&nbsp;
                        <CustomTextField
                        // placeholder={values.residencePhone}
                        //"مثال: 0112324562" 
                        name="residencePhone" />
                    </div>  </Box>

                </div>
                <div className="column border-left">
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >حالة الكسوة :</label>&nbsp;
                        <Field
                        name="calddingStatus"
                        type="select"
                        input={<BootstrapInput />}
                        labelId="demo-customized-select-label"
                        value={formik.values.calddingStatus}
                        as={Select}
                      >
                        <MenuItem value="ممتاز" selected>ممتاز</MenuItem>
                        <MenuItem value="جيد">جيد</MenuItem>
                        <MenuItem value="وسط">وسط</MenuItem>
                        <MenuItem value="سيء">سيء</MenuItem>
                      </Field>

                    </div></Box>
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >التنجيد :</label>&nbsp;
                        <Field
                        name="upholsteryStatus"
                        type="select"
                        input={<BootstrapInput />}
                        labelId="demo-customized-select-label"
                        value={formik.values.upholsteryStatus}
                        as={Select}
                      >
                        <MenuItem value="ممتاز" selected>ممتاز</MenuItem>
                        <MenuItem value="جيد">جيد</MenuItem>
                        <MenuItem value="وسط">وسط</MenuItem>
                        <MenuItem value="سيء">سيء</MenuItem>
                      </Field>

                    </div></Box>
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >الرطوبة :</label>&nbsp;
                        <Field
                        input={<BootstrapInput />}
                        labelId="demo-customized-select-label"
                        name="humidity"
                        type="select"
                        value={formik.values.humidity}
                        as={Select}
                      >
                        <MenuItem value="لايوجد" selected>لايوجد</MenuItem>
                        <MenuItem value="خفيفة">خفيفة</MenuItem>
                        <MenuItem value="متوسطة">متوسطة</MenuItem>
                        <MenuItem value="عالية">عالية</MenuItem>
                      </Field>
                    </div>
                  </Box>
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >حالة العقار :</label>&nbsp;
                        <Field
                        name="residentStatus"
                        type="select"
                        input={<BootstrapInput />}
                        labelId="demo-customized-select-label"
                        value={formik.values.residentStatus}
                        as={Select}
                      >
                        <MenuItem value="ملك" selected>ملك</MenuItem>
                        <MenuItem value="آجار">أجار</MenuItem>
                        <MenuItem value="استثمار">استثمار</MenuItem>
                      </Field>

                    </div></Box>
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >مالك المنزل :</label>&nbsp;
                        <CustomTextField
                        //    placeholder={values.residenceOwner}
                        //"مثال: محمد الأحمد" 
                        name="residenceOwner" />
                    </div>
                  </Box>
                  <Box marginRight={2} marginBottom={2}>
                    <div className="form-row r" >
                      <label className="label" >ملاحظات :</label>&nbsp;
                        <CustomTextField
                        //             placeholder={values.notes}
                        //"مثال: 0112324562"
                        name="notes"
                        type="input"
                        as={TextareaAutosize}
                      />
                    </div>
                  </Box>

                </div>

                <div className="rec-contain">
                  <h4 className="h4-Reemy">إضافة معلومات الترميم</h4>
                  <center >
                    <div className="form-row r">
                      <FieldArray name="rebuilding">
                        {
                          arrayHelpers => (
                            <div>
                              {formik.values.rebuilding.map((rebuildItem, index) => {
                                return (
                                  <div className="form-row r" key={index}>
                                    <CustomTextField
                                      placeholder="العنوان"
                                      name={`rebuilding.${index}.title`}
                                    /> &nbsp;
                                    <CustomTextField
                                      placeholder="ملاحظات"
                                      name={`rebuilding.${index}.description`}
                                      type="input"
                                      as={TextareaAutosize}
                                    /> &nbsp;&nbsp;&nbsp;&nbsp;
                                    <div  >
                                      {formik.values.rebuilding.length - 1 === index &&
                                        <IconButton aria-label="delete" >
                                          <AddCircleRounded fontSize="large"
                                            onClick={() =>
                                              arrayHelpers.push({
                                                title: "",
                                                description: "",
                                              })
                                            }
                                          />
                                        </IconButton>
                                        //             <button
                                        //               className="btn btn-primary btn-rounded mr"
                                        //               onClick={() =>
                                        //                 arrayHelpers.push({
                                        //                   title: "",
                                        //                   description: "",
                                        //                 })
                                        //               }
                                        //             >
                                        //               إضافة ترميم
                                        // </button>

                                      }
                              &nbsp;
                              {formik.values.rebuilding.length !== 1 &&
                                        <IconButton aria-label="delete" >
                                          <CancelOutlined fontSize="large"
                                            onClick={() => arrayHelpers.remove(index)}></CancelOutlined>
                                        </IconButton>
                                        // < button
                                        //   className="btn btn-primary btn-rounded mr"
                                        //   onClick={() => arrayHelpers.remove(index)}>
                                        //   حذف ترميم </button>
                                      }
                                    </div>
                                  </div>
                                );
                              })}
                            </div>
                          )
                        }
                      </FieldArray>
                    </div>
                    <button
                      className="ReemButton"
                      type="submit">
                      تأكيد
              </button>
                  </center>
                </div>

              </Form>
            </div>
          )}
        </Formik>
      }
    </div>
  )
}

export default ResidentInformation;
