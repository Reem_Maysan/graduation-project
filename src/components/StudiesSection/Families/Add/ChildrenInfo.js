import { Container } from 'react-bootstrap';
import './PersonalInformation.css';
import DatePicker from 'react-date-picker';
import HealthyInformation from './HealthyInformation';
import { useState, useEffect } from "react";
import '../../../Routes';

import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { createStyles, makeStyles, withStyles, Theme } from '@material-ui/core/styles';

import { AddBox, AddBoxRounded, AddCircleRounded, AddRounded, Cancel, CancelOutlined, CancelScheduleSend, CancelSharp, DeleteSharp, LocalSee, RemoveCircleOutline } from '@material-ui/icons';

import IconButton from '@material-ui/core/IconButton';

import SideBar from '../../../SideBar/SideBar';

import {
  Formik,
  Field,
  Form,
  useField,
  FieldAttributes,
  FieldArray
} from "formik";
import {
  TextField,
  Button,
  Checkbox,
  Radio,
  FormControlLabel,
  Select,
  Box, Divider,
  MenuItem, InputBase,
  InputLabel,
  TextareaAutosize, FormControl
} from "@material-ui/core";


import { CustomTextField } from '../../../TextFields/TextField';

import * as Yup from 'yup';
import { date } from 'yup/lib/locale';

import clsx from 'clsx';

import RadioGroup from '@material-ui/core/RadioGroup';

const useStyles = makeStyles({
  root: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  icon: {
    borderRadius: '50%',
    width: 16,
    height: 16,
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:hover ~ &': {
      backgroundColor: '#ebf1f5',
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#137cbd',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
      content: '""',
    },
    'input:hover ~ &': {
      backgroundColor: '#106ba3',
    },
  },
});

function StyledRadio(props) {
  const classes = useStyles();

  return (
    <Radio
      className={classes.root}
      disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}


function ChildrenInformation(props) {
  const [childState, setChildState] = useState(
    {
      loading: false,
      id: props.location.state['id'],
      edit: props.location.state['edit'],
      childIndex: props.location.state['childIndex'],
      //token: props.location.state['token'],
      child:
      {
        name: "",
        gender: "",
        birthPlace: "",
        birthDate: "",
        address: "",
        currentStudy: "",
        lastStudy: "",
        currentWork: "",
        workAddress: "",
        salary: 0,
        familyStatus: "",
        ethicalCommitment: "",
        jurisdiction: "",

        healthProblem: "",
        physician: "",
        therapy: "",
        monthlyCost: 0,
        medicineCost: 0,
        healthyNotes: "",

        category: "",
        source: "",
        level: "",
        diligence: "",
        annuallnstallment: 0,
        transportationFare: 0,
        educationNotes: ""
      },
      jsonBody: {
        IDN: props.location.state['IDN'],
        children: props.location.state['children'],
      }
    });
  const BootstrapInput = withStyles((theme) => ({
    root: {
      'label + &': {
        marginTop: theme.spacing(0),
      },
    },
    input: {
      borderRadius: 4,
      position: 'relative',
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #ced4da',
      fontSize: 16,
      textAlign: 'center',
      padding: '10px 26px 10px 12px',
      width: 120,
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderRadius: 4,
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
  }))(InputBase);


  const childrenInfoGet = () => {
    // to view child's info in edit mode
    setChildState({ ...childState, loading: true });
    for (var i = 0; i < childState.jsonBody.children.length; i++) {
      if (childState.childIndex === i) {
        console.log('WE FOUND IT')
        console.log(childState.childIndex)
        setChildState({ ...childState, child: childState.jsonBody.children[i], loading: false });
      }
    }
    console.log('childState')
    console.log(childState.jsonBody)
  }

  useEffect(() => {
    console.log('useEffect')
    if (childState.edit) {
      console.log('------------EDIT MODE------------')
      childrenInfoGet();
    }
    else {
      console.log('------------ADD MODE------------')
    }
  });


  const handleChange = (event) => {
    console.log('hiiiiii from handleChange');
    const newChildState = childState.child;
    newChildState[event.target.id] = event.target.value;
    setChildState({ ...childState, child: newChildState })
    console.log(childState)
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('\\\\\\\\\\\\handleSubmit//////////')
    console.log(childState)
    const newChild = childState.child;
    const children = childState.jsonBody.children;
    const list = [...children]
    list.push(newChild);
    console.log('000000000000000list00000000000000')
    console.log('newChild / children / list/ newJson')
    console.log(newChild)
    console.log(children)
    console.log(list)
    const newJson = {
      IDN: childState.jsonBody.IDN,
      children: list
    };
    console.log(newJson)
    // setChildState({ ...childState, jsonBody: newJson });
    // console.log('childState')
    // console.log(childState)
    // const currentUrl = childState.edit ? myConstClass.Edit_ChildInfo + childState.id : myConstClass.ADD_ChildInfo;
    // console.log('jsonBody')
    // console.log(childState.jsonBody)

    // console.log('currentUrl')
    // console.log(currentUrl)

    // setChildState({ ...childState, loading: true }, () => {
    //   fetch(currentUrl
    //     , {
    //       method: 'POST',
    //       body: JSON.stringify(
    //         childState.jsonBody
    //       ),
    //       headers: {
    //         'Accept': 'application/json',
    //         'Content-type': 'application/json'
    //       },
    //     })
    //     .then((response) => response.json())
    //     .then((json) => {
    //       setChildState({ ...childState, loading: false })
    //       console.log(json)
    //       props.history.push({
    //         pathname: '/addChildList',
    //         state: {
    //           id: childState.id,
    //           edit: childState.edit,
    //           IDN: childState.jsonBody.IDN,
    //           token: childState.token
    //         }
    //       })
    //     });
    // })
    props.history.push({
      pathname: '/addChildList',
      state: {
        id: childState.id,
        edit: childState.edit,
        IDN: childState.jsonBody.IDN,
        token: childState.token
      }
    })
  }

  const validate = Yup.object({

    name: Yup.string()
      // .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),

    gender: Yup.string()
      //  .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),

    birthPlace: Yup.string()
      // .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),

    birthDate: Yup.date()
      //  .max(10, myConstClass.Date_Format)
      .required(myConstClass.Required),


    address: Yup.string()
      //     .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

    currentStudy: Yup.string()
      // .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),

    lastStudy: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    currentWork: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    workAddress: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    familyStatus: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    ethicalCommitment: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),


    jurisdiction: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    monthlyCost: Yup.number().integer().positive()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    medicineCost: Yup.number().integer().positive()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),


    annuallnstallment: Yup.number().integer().positive()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),


    transportationFare: Yup.number().integer().positive()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),


    educationNotes: Yup.string()
      // .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),


    salary: Yup.number().integer().positive()
      //.min(10, myConstClass.Less_10)
      .required(myConstClass.Required),

    healthProblem: Yup.string()
      //     .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

    physician: Yup.string()
      // .max(10, myConstClass.Less_10)
      .required(myConstClass.Required),

    therapy: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    healthyNotes: Yup.string()
      // .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

    category: Yup.string()
      // .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),


    source: Yup.string()
      // .max(20, myConstClass.Less_20)
      .required(myConstClass.Required),

    level: Yup.string()
      // .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

    diligence: Yup.string()
      // .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),

  })

  return (
    <div>
      <SideBar />
      {childState.loading === true ?
        <Backdrop
          open>
          <CircularProgress color="inherit" />
        </Backdrop> :
        <Formik
          validateOnChange={true}
          initialValues={
            {
              name: "",
              gender: "",
              birthPlace: "",
              birthDate: "",
              address: "",
              currentStudy: "",
              lastStudy: "",
              currentWork: "",
              workAddress: "",
              salary: 0,
              familyStatus: "",
              ethicalCommitment: "",
              jurisdiction: "",
              healthProblem: "",
              physician: "",
              therapy: "",
              monthlyCost: 0,
              medicineCost: 0,
              healthyNotes: "",

              category: "",
              source: "",
              level: "",
              diligence: "",
              annuallnstallment: 0,
              transportationFare: 0,
              educationNotes: ""
            }
          }
          validationSchema={validate}
          onSubmit={values => {
            console.log('\\\\\\\\\\\\values//////////')
            console.log(values)
            const currentUrl = childState.edit ? myConstClass.Edit_ChildInfo + childState.id : myConstClass.ADD_ChildInfo;
            console.log('currentUrl')
            console.log(currentUrl)
            setChildState({
              ...childState,
              loading: true,
              child: values
            });
            fetch(currentUrl
              , {
                method: 'POST',
                body: JSON.stringify(
                  values
                ),
                headers: {
                  'Accept': 'application/json',
                  'Content-type': 'application/json'
                },
              })
              .then((response) => response.json())
              .then((json) => {
                console.log(json)
                props.history.push({
                  pathname: '/addChildList',
                  state: {
                    // id: childState.id,
                    // edit: childState.edit,
                    // IDN: childState.IDN,
                    //token: childState.token
                  }
                })
              });
          }}
        >
          {formik => (
            <div>
              {console.log(formik.values)}
              <Form>
                <div className="rec-contain">
                  <h4 className="h4-Reem"> المعلومات الشخصية للطفل </h4>
                  <div className="column border-left">
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الاسم :</label>
                &nbsp;
                <CustomTextField
                          name="name" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الجنس :</label>&nbsp;
                                                <FormControl component="fieldset">
                          <RadioGroup
                            aria-label="gender" name="gender"
                            onChange={(val) => {
                              formik.values.gender = val.target.value
                            }}>
                            <FormControlLabel
                              control={<StyledRadio />}
                              label="ذكر"
                              name="gender"
                              value="m"
                            />
                            <FormControlLabel
                              control={<StyledRadio />}
                              label="مؤنث"
                              name="gender"
                              value="f"
                            />
                          </RadioGroup>
                        </FormControl>

                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >مكان الولادة :</label>&nbsp;
                <CustomTextField
                          name="birthPlace" />
                      </div> </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >تاريخ الولادة :</label>&nbsp;
                <CustomTextField
                          name="birthDate" />
                      </div></Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >العنوان :</label>&nbsp;
                <CustomTextField
                          name="address" />
                      </div>  </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الدراسة الحالية :</label>&nbsp;
                <CustomTextField
                          name="currentStudy" />
                      </div>
                    </Box>

                  </div>
                  <div className="column border-left">
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >عنوان العمل :</label>&nbsp;
                <CustomTextField
                          name="workAddress" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الراتب :</label>&nbsp;
                <CustomTextField
                          name="salary" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الحالة الاجتماعية  :</label>&nbsp;
                        <Field
                          name="familyStatus"
                          type="select"
                          input={<BootstrapInput />}
                          labelId="demo-customized-select-label"
                          value={formik.values.familyStatus}
                          as={Select}
                        >
                          <MenuItem value="أعزب" selected>أعزب</MenuItem>
                          <MenuItem value="متزوج">متزوج</MenuItem>
                          <MenuItem value="خاطب">خاطب</MenuItem>
                          <MenuItem value="أرملة">أرملة</MenuItem>
                          <MenuItem value="مطلق">مطلق</MenuItem>
                        </Field>

                      </div></Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الالتزام الأخلاقي :</label>&nbsp;
                        <Field
                          name="ethicalCommitment"
                          type="select"
                          input={<BootstrapInput />}
                          labelId="demo-customized-select-label"
                          value={formik.values.ethicalCommitment}
                          as={Select}
                        >
                          <MenuItem value="ملتزم" selected>ملتزم</MenuItem>
                          <MenuItem value="غير ملتزم">غير ملتزم</MenuItem>
                          <MenuItem value="متوسط">متوسط</MenuItem>
                        </Field>

                      </div></Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الدراسة الاخيرة :</label>&nbsp;
                <CustomTextField
                          name="lastStudy" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >العمل الحالي :</label>&nbsp;
                <CustomTextField
                          name="currentWork" />
                      </div>
                    </Box>

                  </div>
                </div>
                <div className="rec-contain">
                  <h4 className="h4-Reem"> المعلومات الصحية للطفل
                  </h4>
                  <div className="column border-left">
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >المشكلة الصحية :</label>&nbsp;
                <CustomTextField
                          name="healthProblem" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الطبيب المعالج :</label>&nbsp;
                <CustomTextField
                          name="physician" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >العلاج :</label>&nbsp;
                <CustomTextField
                          name="therapy" />
                      </div>
                    </Box>
                  </div>
                  <div className="column border-left">
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >التكلفة الشهرية :</label>&nbsp;
                <CustomTextField
                          name="monthlyCost" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >كلفة الدواء :</label>&nbsp;
                <CustomTextField
                          name="medicineCost" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >ملاحظات :</label>&nbsp;
                <CustomTextField
                          name="healthyNotes" />
                      </div>
                    </Box>
                  </div>

                  <div className="column border-left">
                  </div>
                </div>
                <div className="rec-contain">
                  <h4 className="h4-Reem"> المعلومات الصحية للطفل
                  </h4>
                  <div className="column border-left">
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" > أجرة النقل  :</label>&nbsp;
                <CustomTextField
                          name="transportationFare" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" > ملاحظات  :</label>&nbsp;
                <CustomTextField
                          name="educationNotes" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >مابعرف :</label>&nbsp;
                <CustomTextField
                          name="jurisdiction" />
                      </div>
                    </Box>
                  </div>
                  <div className="column border-left">
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" >الفئة :</label>&nbsp;
                <CustomTextField
                          name="category" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" > مصدر الشهادة :</label>&nbsp;
                <CustomTextField
                          name="source" />
                      </div>
                    </Box>
                    <Box marginRight={2} marginBottom={2}>
                      <div className="form-row r" >
                        <label className="label" > مستوى التعليم  :</label>&nbsp;
                <CustomTextField
                          name="level" />
                      </div>
                    </Box>

                  </div>
                 <center>
                  <button className="ReemButton"
                    type="submit">
                    تأكيد
</button> </center>
                </div>
                

              </Form>
            </div>

          )}
        </Formik>
      }</div>
  )
}
export default ChildrenInformation;
