import React, { useState, useEffect } from "react";
import { Container } from 'react-bootstrap';
import DatePicker from 'react-date-picker';
import '../../../Routes';
import Axios from 'axios';
import * as myConstClass from '../../../../constants';
import axios from 'axios';
import { Link } from 'react-router-dom';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import * as Yup from 'yup';
import { Box, Divider } from '@material-ui/core';

import {
    Formik,
    Field,
    Form,
    useField,
    FieldAttributes,
    FieldArray
} from "formik";
import {
    TextField,
    Button,
    Checkbox,
    Radio,
    FormControlLabel,
    Select,
    MenuItem, InputBase,
    InputLabel,
    TextareaAutosize, FormControl
} from "@material-ui/core";
import { date } from 'yup/lib/locale';

import { createStyles, makeStyles, withStyles, Theme } from '@material-ui/core/styles';

import { MyTextField } from '../../../TextFields/formikTextField';

function Pers(props) {

    const BootstrapInput = withStyles((theme) => ({
        root: {
            'label + &': {
                marginTop: theme.spacing(0),
            },
        },
        input: {
            borderRadius: 4,
            position: 'relative',
            backgroundColor: theme.palette.background.paper,
            border: '1px solid #ced4da',
            fontSize: 16,
            textAlign: 'center',
            padding: '10px 26px 10px 12px',
            width: 120,
            transition: theme.transitions.create(['border-color', 'box-shadow']),
            // Use the system font instead of the default Roboto font.
            fontFamily: [
                '-apple-system',
                'BlinkMacSystemFont',
                '"Segoe UI"',
                'Roboto',
                '"Helvetica Neue"',
                'Arial',
                'sans-serif',
                '"Apple Color Emoji"',
                '"Segoe UI Emoji"',
                '"Segoe UI Symbol"',
            ].join(','),
            '&:focus': {
                borderRadius: 4,
                borderColor: '#80bdff',
                boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
            },
        },
    }))(InputBase);

    const [dataState, setDataState] = useState({
        loading: false,
        id: props.location.state['id'],
        edit: props.location.state['edit'],
        //   token: props.location.state['token'],
        personalInfo: {
            firstName: '',
            fatherName: '',
            lastName: '',
            motherFirstName: '',
            motherLastName: '',
            birthPlace: '',
            birthDate: '',
            nationality: '',
            IDN: '',
            maritalStatus: 'أعزب',
            education: '',
            ethicalCommitment: 'ملتزم',
            gender: 'f',
            address: '',
            familyHistory: '',
            currentWork: '',
            currentWorkAddress: '',
            currentWorkPhone: 1,
        }
    });

    const validate = Yup.object({
        firstName: Yup.string()
            //   .max(15, myConstClass.Less_15)
            .required(myConstClass.Required),
        fatherName: Yup.string()
            //   .max(15, myConstClass.Less_15)
            .required(myConstClass.Required),
        lastName: Yup.string()
            //   .max(20, myConstClass.Less_20)
            .required(myConstClass.Required),
        motherFirstName: Yup.string()
            //   .max(15, myConstClass.Less_15)
            .required(myConstClass.Required),
        motherLastName: Yup.string()
            //  .max(20, myConstClass.Less_20)
            .required(myConstClass.Required),
        birthPlace: Yup.string()
            //  .max(15, myConstClass.Less_15)
            .required(myConstClass.Required),
        birthDate: Yup.string()
            // .max(10, myConstClass.Date_Format)
            .required(myConstClass.Required),
        nationality: Yup.string()
            //   .max(15, myConstClass.Less_15)
            .required(myConstClass.Required),
        IDN: Yup.string()
            // .max(20, myConstClass.Less_20)
            .required(myConstClass.Required),
        education: Yup.string()
            //     .max(15, myConstClass.Less_15)
            .required(myConstClass.Required),
        address: Yup.string()
            //   .max(30, myConstClass.Less_30)
            .required(myConstClass.Required),
        familyHistory: Yup.string()
            //    .max(10, myConstClass.Less_10) 
            .required(myConstClass.Required),
        currentWork: Yup.string()
            ////  .max(20, myConstClass.Less_20)
            .required(myConstClass.Required),
        currentWorkAddress: Yup.string()
            //  .max(30, myConstClass.Less_30)
            .required(myConstClass.Required),
        currentWorkPhone: Yup.number().integer().positive()
            //  .min(10, myConstClass.Less_10)
            .required(myConstClass.Required),
    })

    useEffect(() => {
        console.log('useEffect')
        if (dataState.edit) {
            console.log('------------EDIT MODE------------')
            personalInfoGet();
        }
        else {
            console.log('------------ADD MODE------------')
        }
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('\\\\\\\\\\\\handleSubmit//////////')
        console.log(dataState)
        const currentUrl = dataState.edit ? myConstClass.Edit_PersonalInfo + dataState.id : myConstClass.ADD_PersonalInfo;
        console.log('currentUrl')
        console.log(currentUrl)
        setDataState({
            ...dataState,
            loading: true
        }, () => {
            fetch(currentUrl
                , {
                    method: 'POST',
                    body: JSON.stringify(
                        dataState.personalInfo
                    ),
                    headers: {
                        'Accept': 'application/json',
                        'Content-type': 'application/json'
                    },
                })
                .then((response) => response.json())
                .then((json) => {
                    console.log(json)
                    props.history.push({
                        pathname: '/addResidentInformation',
                        state: {
                            id: dataState.id,
                            edit: dataState.edit,
                            IDN: dataState.IDN,
                            //        token: dataState.token
                        }
                    })
                });
        })
    }

    const personalInfoGet = async () => {
        console.log('hi from personalInfoGet')
        setDataState({ ...dataState, loading: true });
        const res = await axios.get(myConstClass.GET_PersonalInfo + dataState.id)
        setDataState({ ...dataState, personalInfo: res.data['data'], loading: false });
        console.log('personalInfo')
        console.log(res.data['data'])
        console.log(dataState.personalInfo)
    }
    return (
        <Formik
            initialValues={{
                firstName: dataState.personalInfo.firstName,
                fatherName: dataState.personalInfo.fatherName,
                lastName: dataState.personalInfo.lastName,
                motherFirstName: dataState.personalInfo.motherFirstName,
                motherLastName: dataState.personalInfo.motherLastName,
                birthPlace: dataState.personalInfo.birthPlace,
                birthDate: dataState.personalInfo.birthDate,
                nationality: dataState.personalInfo.nationality,
                IDN: dataState.personalInfo.IDN,
                maritalStatus: dataState.personalInfo.maritalStatus,
                education: dataState.personalInfo.education,
                ethicalCommitment: dataState.personalInfo.ethicalCommitment,
                gender: dataState.personalInfo.gender,
                address: dataState.personalInfo.address,
                familyHistory: dataState.personalInfo.familyHistory,
                currentWork: dataState.personalInfo.currentWork,
                currentWorkAddress: dataState.personalInfo.currentWorkAddress,
                currentWorkPhone: dataState.personalInfo.currentWorkPhone,
            }}
            validationSchema={validate}
            onSubmit={values => {
                console.log('\\\\\\\\\\\\values//////////')
                console.log(values)
                const currentUrl = dataState.edit ? myConstClass.Edit_PersonalInfo + dataState.id : myConstClass.ADD_PersonalInfo;
                console.log('currentUrl')
                console.log(currentUrl)
                setDataState({
                    ...dataState,
                    loading: true
                });
                fetch(currentUrl
                    , {
                        method: 'POST',
                        body: JSON.stringify(
                            values
                        ),
                        headers: {
                            'Accept': 'application/json',
                            'Content-type': 'application/json'
                        },
                    })
                    .then((response) => response.json())
                    .then((json) => {
                        console.log(json)

                    });
            }}
        >
            {
                formik => (
                    <div className="rec-contain" >
                        <center>
                            <h6> إضافة مساعدة مالية </h6>
                            <Divider></Divider>
                            {console.log(formik.values)}
                            <Form >
                                <div className="column border-left">
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="الاسم الاول:"
                                                name="firstName" type="text"
                                                placeholder="الاسم الاول" />
                                        </div>
                                    </Box>

                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="اسم الأب:"
                                                name="fatherName" type="text"
                                                placeholder="اسم الأب" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="الكنية:"
                                                name="lastName" type="text"
                                                placeholder="الكنية" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="اسم الأم:"
                                                name="motherFirstName" type="text"
                                                placeholder="اسم الأم" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="كنية الأم:"
                                                name="motherLastName" type="text"
                                                placeholder="كنية الأم" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="مكان الولادة:"
                                                name="birthPlace" type="text"
                                                placeholder="مكان الولادة" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="العمل الحالي:"
                                                name="currentWork" type="text"
                                                placeholder="العمل الحالي" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="هاتف العمل الحالي:"
                                                name="currentWorkPhone" type="text"
                                                placeholder="هاتف العمل الحالي" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="عنوان العمل الحالي:"
                                                name="currentWorkAddress" type="text"
                                                placeholder="عنوان العمل الحالي" />
                                        </div>
                                    </Box>
                                </div>
                                <div className="column border-left">
                                    <Box marginRight={5} marginTop={2} >
                                        <div className="form-row r" >
                                            <label className="label" >الحالة الاجتماعية  :</label>&nbsp;
                        <Field
                                                name="maritalStatus"
                                                type="select"
                                                input={<BootstrapInput />}
                                                labelId="demo-customized-select-label"
                                                value={formik.values.maritalStatus}
                                                as={Select}
                                            >
                                                <MenuItem value="أعزب" selected>أعزب</MenuItem>
                                                <MenuItem value="متزوج">متزوج</MenuItem>
                                                <MenuItem value="خاطب">خاطب</MenuItem>
                                                <MenuItem value="أرملة">أرملة</MenuItem>
                                                <MenuItem value="مطلق">مطلق</MenuItem>
                                            </Field>

                                        </div></Box>
                                    <Box marginRight={5} marginTop={2}>
                                        <div className="form-row r" >
                                            <label className="label" >الالتزام الأخلاقي :</label>&nbsp;
                        <Field
                                                name="upholsteryStatus"
                                                type="select"
                                                input={<BootstrapInput />}
                                                labelId="demo-customized-select-label"
                                                value={formik.values.ethicalCommitment}
                                                as={Select}
                                            >
                                                <MenuItem value="ملتزم" selected>ملتزم</MenuItem>
                                                <MenuItem value="غير ملتزم">غير ملتزم</MenuItem>
                                                <MenuItem value="متوسط">متوسط</MenuItem>
                                            </Field>

                                        </div></Box>
                                    <Box marginRight={5} marginTop={2}>
                                        <div className="form-row r" >
                                            <label className="label" >الجنس :</label>&nbsp;
                                            <FormControlLabel
                                                control={<Radio />}
                                                label="ذكر"
                                                name="gender"
                                                value={formik.values.gender}
                                                checked={formik.values.gender === "m"}

                                            />


                                            <FormControlLabel
                                                control={<Radio />}
                                                label="مؤنث"
                                                name="gender"
                                                value={formik.values.gender}
                                                checked={formik.values.gender === "f"} />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="رقم الهوية:"
                                                name="IDN" type="text"
                                                placeholder="رقم الهوية" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="تاريخ الميلاد:"
                                                name="birthDate" type="text"
                                                placeholder="تاريخ الميلاد" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="العنوان:"
                                                name="address" type="text"
                                                placeholder="العنوان" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="التعليم:"
                                                name="education" type="text"
                                                placeholder="التعليم" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="تاريخ العائلة:"
                                                name="familyHistory" type="text"
                                                placeholder="تاريخ العائلة" />
                                        </div>
                                    </Box>
                                    <Box marginRight={15} >
                                        <div className="form-row r" >
                                            <MyTextField
                                                label="الجنسية:"
                                                name="nationality" type="text"
                                                placeholder="الجنسية" />
                                        </div>
                                    </Box>
                                </div>

                                <button className="btn btn-primary btn-rounded mr"
                                    type="submit"
                                >
                                    إضافة
              </button>
                            </Form>

                        </center>
                    </div>
                )
            }
        </Formik >
    );
}
export default Pers;

/*  onSubmit={values => {
                console.log('\\\\\\\\\\\\values//////////')
                console.log(values)
                //     const currentUrl = dataState.edit ? myConstClass.Edit_PersonalInfo + dataState.id : myConstClass.ADD_PersonalInfo;
                //     console.log('currentUrl')
                //     console.log(currentUrl)
                //     setDataState({
                //         ...dataState,
                //         loading: true
                //     });
                //     fetch(currentUrl
                //         , {
                //             method: 'POST',
                //             body: JSON.stringify(
                //                 values
                //             ),
                //             headers: {
                //                 'Accept': 'application/json',
                //                 'Content-type': 'application/json'
                //             },
                //         })
                //         .then((response) => response.json())
                //         .then((json) => {
                //             console.log(json)

                //         });
                //     const newJson = {
                //         firstName: '',
                //         fatherName: '',
                //         lastName: '',
                //         motherFirstName: '',
                //         motherLastName: '',
                //         birthPlace: '',
                //         birthDate: '',
                //         nationality: '',
                //         IDN: '',
                //         maritalStatus: 'أعزب',
                //         education: '',
                //         ethicalCommitment: 'ملتزم',
                //         gender: '',
                //         address: '',
                //         familyHistory: '',
                //         currentWork: '',
                //         currentWorkAddress: '',
                //         currentWorkPhone: 1,
                //     }
                //     setDataState({
                //         ...dataState,
                //         personalInfo: newJson
                //     });

            } */