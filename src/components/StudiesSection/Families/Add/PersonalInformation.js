import React, { useState, useEffect } from "react";
import { Container } from 'react-bootstrap';
import DatePicker from 'react-date-picker';
import '../../../Routes';
import Axios from 'axios';
import * as myConstClass from '../../../../constants';
import axios from 'axios';
import { Link } from 'react-router-dom';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

import * as Yup from 'yup';
import { Box, Divider } from '@material-ui/core';

import { CustomTextField } from '../../../TextFields/TextField';

import {
   Formik,
   Field,
   Form,
   useField,
   FieldAttributes,
   FieldArray
} from "formik";
import {
   TextField,
   Button,
   Checkbox,
   Radio,
   FormControlLabel,
   Select,
   MenuItem, InputBase,
   InputLabel,
   TextareaAutosize, FormControl
} from "@material-ui/core";
import { date } from 'yup/lib/locale';

import clsx from 'clsx';
import { createStyles, makeStyles, withStyles, Theme } from '@material-ui/core/styles';

import { MyTextField } from '../../../TextFields/formikTextField';

import RadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';


import SideBar from '../../../SideBar/SideBar';

const useStyles = makeStyles({
   root: {
      '&:hover': {
         backgroundColor: 'transparent',
      },
   },
   icon: {
      borderRadius: '50%',
      width: 16,
      height: 16,
      boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
      backgroundColor: '#f5f8fa',
      backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
      '$root.Mui-focusVisible &': {
         outline: '2px auto rgba(19,124,189,.6)',
         outlineOffset: 2,
      },
      'input:hover ~ &': {
         backgroundColor: '#ebf1f5',
      },
      'input:disabled ~ &': {
         boxShadow: 'none',
         background: 'rgba(206,217,224,.5)',
      },
   },
   checkedIcon: {
      backgroundColor: '#137cbd',
      backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
      '&:before': {
         display: 'block',
         width: 16,
         height: 16,
         backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
         content: '""',
      },
      'input:hover ~ &': {
         backgroundColor: '#106ba3',
      },
   },
});

function StyledRadio(props) {
   const classes = useStyles();

   return (
      <Radio
         className={classes.root}
         disableRipple
         color="default"
         checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
         icon={<span className={classes.icon} />}
         {...props}
      />
   );
}

function PersonalInformation(props) {

   const classes = useStyles();


   const BootstrapInput = withStyles((theme) => ({
      root: {
         'label + &': {
            marginTop: theme.spacing(0),
         },
      },
      input: {
         borderRadius: 4,
         position: 'relative',
         backgroundColor: theme.palette.background.paper,
         border: '1px solid #ced4da',
         fontSize: 16,
         textAlign: 'center',
         padding: '10px 26px 10px 12px',
         width: 120,
         transition: theme.transitions.create(['border-color', 'box-shadow']),
         // Use the system font instead of the default Roboto font.
         fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
         ].join(','),
         '&:focus': {
            borderRadius: 4,
            borderColor: '#80bdff',
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
         },
      },
   }))(InputBase);

   const [dataState, setDataState] = useState({
      loading: false,
      id: props.location.state['id'],
      edit: props.location.state['edit'],
      //   token: props.location.state['token'],
      personalInfo: {
         firstName: '',
         fatherName: '',
         lastName: '',
         motherFirstName: '',
         motherLastName: '',
         birthPlace: '',
         birthDate: '',
         nationality: '',
         IDN: '',
         maritalStatus: 'أعزب',
         education: '',
         ethicalCommitment: 'ملتزم',
         gender: '',
         address: '',
         familyHistory: '',
         currentWork: '',
         currentWorkAddress: '',
         currentWorkPhone: 1,
      }
   });

   const validate = Yup.object({
      firstName: Yup.string()
         //   .max(15, myConstClass.Less_15)
         .required(myConstClass.Required),
      fatherName: Yup.string()
         //   .max(15, myConstClass.Less_15)
         .required(myConstClass.Required),
      lastName: Yup.string()
         //   .max(20, myConstClass.Less_20)
         .required(myConstClass.Required),
      motherFirstName: Yup.string()
         //   .max(15, myConstClass.Less_15)
         .required(myConstClass.Required),
      motherLastName: Yup.string()
         //  .max(20, myConstClass.Less_20)
         .required(myConstClass.Required),
      birthPlace: Yup.string()
         //  .max(15, myConstClass.Less_15)
         .required(myConstClass.Required),
      birthDate: Yup.string()
         // .max(10, myConstClass.Date_Format)
         .required(myConstClass.Required),
      nationality: Yup.string()
         //   .max(15, myConstClass.Less_15)
         .required(myConstClass.Required),
      IDN: Yup.string()
         // .max(20, myConstClass.Less_20)
         .required(myConstClass.Required),
      education: Yup.string()
         //     .max(15, myConstClass.Less_15)
         .required(myConstClass.Required),
      address: Yup.string()
         //   .max(30, myConstClass.Less_30)
         .required(myConstClass.Required),
      familyHistory: Yup.string()
         //    .max(10, myConstClass.Less_10) 
         .required(myConstClass.Required),
      currentWork: Yup.string()
         ////  .max(20, myConstClass.Less_20)
         .required(myConstClass.Required),
      currentWorkAddress: Yup.string()
         //  .max(30, myConstClass.Less_30)
         .required(myConstClass.Required),
      currentWorkPhone: Yup.number().integer().positive()
         //  .min(10, myConstClass.Less_10)
         .required(myConstClass.Required),
   })

   useEffect(() => {
      console.log('useEffect')
      if (dataState.edit) {
         console.log('------------EDIT MODE------------')
         personalInfoGet();
      }
      else {
         console.log('------------ADD MODE------------')
      }
   }, []);

   const handleSubmit = (e) => {
      e.preventDefault();
      console.log('\\\\\\\\\\\\handleSubmit//////////')
      console.log(dataState)
      const currentUrl = dataState.edit ? myConstClass.Edit_PersonalInfo + dataState.id : myConstClass.ADD_PersonalInfo;
      console.log('currentUrl')
      console.log(currentUrl)
      setDataState({
         ...dataState,
         loading: true
      }, () => {
         fetch(currentUrl
            , {
               method: 'POST',
               body: JSON.stringify(
                  dataState.personalInfo
               ),
               headers: {
                  'Accept': 'application/json',
                  'Content-type': 'application/json'
               },
            })
            .then((response) => response.json())
            .then((json) => {
               console.log(json)
               props.history.push({
                  pathname: '/addResidentInformation',
                  state: {
                     id: dataState.id,
                     edit: dataState.edit,
                     IDN: dataState.IDN,
                     //        token: dataState.token
                  }
               })
            });
      })
   }

   const personalInfoGet = async () => {
      console.log('hi from personalInfoGet')
      setDataState({ ...dataState, loading: true });
      const res = await axios.get(myConstClass.GET_PersonalInfo + dataState.id)
      setDataState({ ...dataState, personalInfo: res.data['data'], loading: false });
      console.log('personalInfo')
      console.log(res.data['data'])
      console.log(dataState.personalInfo)
   }

   return (
      <div>
         <SideBar />
         {
            dataState.loading === true ?
               <Backdrop open>
                  <CircularProgress color="inherit" />
               </Backdrop> :
               <Formik
                  initialValues={{
                     firstName: '',
                     fatherName: '',
                     lastName: '',
                     motherFirstName: '',
                     motherLastName: '',
                     birthPlace: '',
                     birthDate: '',
                     nationality: '',
                     IDN: '',
                     maritalStatus: 'أعزب',
                     education: '',
                     ethicalCommitment: 'ملتزم',
                     gender: '',
                     address: '',
                     familyHistory: '',
                     currentWork: '',
                     currentWorkAddress: '',
                     currentWorkPhone: 1,
                  }}
                  validationSchema={validate}
                  onSubmit={values => {
                     console.log('\\\\\\\\\\\\values//////////')
                     console.log(values)
                     const currentUrl = dataState.edit ? myConstClass.Edit_PersonalInfo + dataState.id : myConstClass.ADD_PersonalInfo;
                     console.log('currentUrl')
                     console.log(currentUrl)
                     setDataState({
                        ...dataState,
                        loading: true,
                        personalInfo: values
                     });
                     fetch(currentUrl
                        , {
                           method: 'POST',
                           body: JSON.stringify(
                              values
                           ),
                           headers: {
                              'Accept': 'application/json',
                              'Content-type': 'application/json'
                           },
                        })
                        .then((response) => {
                           if (response.status == 200)
                              response.json()
                        })
                        .then((json) => {
                           console.log(json)
                           props.history.push({
                              pathname: '/addResidentInformation',
                              state: {
                                 id: dataState.id,
                                 edit: dataState.edit,
                                 IDN: values.IDN,
                                 // token: dataState.token
                              }
                           })

                        });
                  }}
               >
                  {
                     formik => (
                        <div className="rec-contain"  >
                           <h4 className="h4-Reem"> إضافة المعلومات الشخصية </h4>
                           {console.log(formik.values)}
                           <Form>
                              <div className="column border-left">
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label">الاسم الأول :</label>&nbsp;
                                          <CustomTextField
                                          name="firstName" />
                                    </div>
                                 </Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >اسم الأب :</label>&nbsp;
                        <CustomTextField
                                          placeholder={formik.values.fatherName}
                                          name="fatherName" />
                                    </div></Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >الكنية :</label>&nbsp;
                        <CustomTextField
                                          name="lastName" />
                                    </div> </Box>

                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >اسم الأم :</label>&nbsp;
                        <CustomTextField
                                          name="motherFirstName" />
                                    </div></Box>

                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >كنية الأم :</label> &nbsp;
                        <CustomTextField
                                          name="motherLastName" />
                                    </div> </Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >مكان الولادة :</label>&nbsp;
                        <CustomTextField
                                          name="birthPlace" />
                                    </div>  </Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >العمل الحالي :</label>&nbsp;
                        <CustomTextField
                                          name="currentWork" />
                                    </div>
                                 </Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >هاتف العمل الحالي :</label>&nbsp;
                        <CustomTextField
                                          name="currentWorkPhone" />
                                    </div>
                                 </Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >عنوان العمل الحالي :</label>&nbsp;
                        <CustomTextField
                                          name="currentWorkAddress" />
                                    </div>
                                 </Box>

                              </div>
                              <div className="column border-left">
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >الحالة الاجتماعية  :</label>&nbsp;
                        <Field
                                          name="maritalStatus"
                                          type="select"
                                          input={<BootstrapInput />}
                                          labelId="demo-customized-select-label"
                                          value={formik.values.maritalStatus}
                                          as={Select}
                                       >
                                          <MenuItem value="أعزب" selected>أعزب</MenuItem>
                                          <MenuItem value="متزوج">متزوج</MenuItem>
                                          <MenuItem value="خاطب">خاطب</MenuItem>
                                          <MenuItem value="أرملة">أرملة</MenuItem>
                                          <MenuItem value="مطلق">مطلق</MenuItem>
                                       </Field>

                                    </div></Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >الالتزام الأخلاقي :</label>&nbsp;
                        <Field
                                          name="upholsteryStatus"
                                          type="select"
                                          input={<BootstrapInput />}
                                          labelId="demo-customized-select-label"
                                          value={formik.values.ethicalCommitment}
                                          as={Select}
                                       >
                                          <MenuItem value="ملتزم" selected>ملتزم</MenuItem>
                                          <MenuItem value="غير ملتزم">غير ملتزم</MenuItem>
                                          <MenuItem value="متوسط">متوسط</MenuItem>
                                       </Field>

                                    </div></Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >الجنس :</label>&nbsp;
                                                <FormControl component="fieldset">
                                          <RadioGroup
                                             aria-label="gender" name="gender"
                                             onChange={(val) => {
                                                formik.values.gender = val.target.value
                                             }}>
                                             <FormControlLabel
                                                control={<StyledRadio />}
                                                label="ذكر"
                                                name="gender"
                                                value="m"
                                             />
                                             <FormControlLabel
                                                control={<StyledRadio />}
                                                label="مؤنث"
                                                name="gender"
                                                value="f"
                                             />
                                          </RadioGroup>
                                       </FormControl>

                                    </div>
                                 </Box>

                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >رقم الهوية :</label>&nbsp;
                        <CustomTextField
                                          name="IDN" />
                                    </div>
                                 </Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >تاريخ الميلاد :</label>&nbsp;
                        <CustomTextField
                                          name="birthDate" />
                                    </div>
                                 </Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >العنوان :</label>&nbsp;
                        <CustomTextField
                                          name="address" />
                                    </div>
                                 </Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >التعليم :</label>&nbsp;
                        <CustomTextField
                                          name="education" />
                                    </div>
                                 </Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >تاريخ العائلة :</label>&nbsp;
                        <CustomTextField
                                          name="familyHistory" />
                                    </div>
                                 </Box>
                                 <Box marginRight={2} marginBottom={2}>
                                    <div className="form-row r" >
                                       <label className="label" >الجنسية :</label>&nbsp;
                        <CustomTextField
                                          name="nationality" />
                                    </div>
                                    <button className="ReemButton"

                                       type="submit">
                                       تأكيد
              </button>
                                 </Box>
                              </div>

                           </Form>
                        </div>
                     )
                  }
               </Formik >
         }</div>
   );
}
export default PersonalInformation;
