import react, { Component } from 'react';
import axios from 'axios';
import { Table, Button } from 'react-bootstrap';
import { LoopCircleLoading } from 'react-loadingg';
import SideBar from '../../../SideBar/SideBar';
import { WindMillLoading } from 'react-loadingg';
import * as myConstClass from '../../../../constants';
import { Link } from 'react-router-dom';
class UnAccFam extends Component {

  constructor(props) {
    super(props);
    this.state = {
      families: [],
      acceptFamilyId: "",
      rejectFamilyId: ""
    }
  }
  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.GET_PendingForms)
    console.log(res.data)
    console.log(res.firstName)
    this.setState({
      families: res.data["data"],
    })
  }

async acceptFamily(id){
  
    this.setState({
      acceptFamilyId: id
    });
    let data = new FormData();
    data.append('id', this.state.rejectFamilyId);
    for (var value of data.values()) {
      console.log("data is " + value);
   }
  
    console.log(data);
     axios({
      method: 'post',
      headers: {   Authorization :  'Bearer ' + localStorage.getItem('token') },
      url:myConstClass.Accept_Form,
      data: data
    }).then(res =>{
      if(res.status == 200){
      console.log("okkkk");
      alert("تم قبول العائلة");
      window.location.href = "/content";
     
      }
  }
  ).catch(
  )
    
  }

  async rejectFamily(id) {
    this.setState({
      rejectFamilyId: id
    });
    let data = new FormData();
    data.append('id', this.state.rejectFamilyId);
    for (var value of data.values()) {
      console.log("data is " + value);
   }
  
    console.log(data);
     axios({
      method: 'post',
      headers: {   Authorization :  'Bearer ' + localStorage.getItem('token') },
      url: myConstClass.Reject_Form,
      data: data
    }).then(res =>{
      if(res.status == 200){
      console.log("okkkk");
      alert("تم رفض العائلة");
      window.location.href = "/content";
      
      }
  }
  ).catch(
  )
  
  }

  render() {
    const { families } = this.state;
    return (
      <div>
        <SideBar />
        <div>
          <center>
            <table className="show-table" >
              <thead  >
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th>الاسم الثلاثي </th>
                  <th>العملية</th>
                </tr>
              </thead>
              <tbody>
                {(families.length > 0) ? families.map((person, index) => {
                  return (
                    <tr>
                      <td> {person.id}</td>
                      <td>
                        <Link to={{
                          pathname: '/displayPerson/',
                          state: {
                            id: person.id
                          }
                        }}>{person.firstName + ' '}{person.fatherName + ' '}{person.lastName + ' '}</Link>
                      </td>
                      {/* <td>

                        <a href={"/nameUnAcc/" + person.id}>{person.firstName + ' '}{person.fatherName + ' '}{person.lastName + ' '} </a></td>
                      */}
                      <td>
                      <Link type="button"  onClick= {() => {this.rejectFamily(person.id)}} > رفض </Link>
                      <Link type="button"  onClick= {() => { this.acceptFamily(person.id) }} > قبول </Link>
                      </td>
                    </tr>
                  )
                }) : <tr><td colSpan="5">جاري التحميل...</td></tr>}
              </tbody>
            </table>
          </center>
        </div>
      </div>
    );
  }
}

export default UnAccFam;