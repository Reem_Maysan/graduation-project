import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Table, Button } from 'react-bootstrap';


import './DisplayPage.css';
import { LoopCircleLoading } from 'react-loadingg';
import SideBar from '../../../SideBar/SideBar';
import { WindMillLoading } from 'react-loadingg';
import * as myConstClass from '../../../../constants';

class DisplayAssociation extends Component {

    constructor(props) {
        super(props);
        this.state = {
            families: [],
        }
    }
    componentDidMount() {
        this.refreshList();
    }

    async refreshList() {
        console.log('refreshList')
        const res = await axios.get(myConstClass.GET_AssociationForms)
        console.log(res.data)
        this.setState({
            families: res.data["data"],
        })
        console.log('families')
        console.log(this.state.families)
    }

    render() {
        return (
            <div>
                <SideBar />
                <div>
                <button
                        className="ReemButton"
                        onClick={() => {
                            this.props.history.push({
                                pathname: '/addPersonalInformation',
                                state: {
                                    id: -1,
                                    edit: false,
                                    // token: dataState.token
                                }
                            })
                        }} >
                        إضافة عائلة</button>
                    <center>
                        <table className="show-table" >
                            <thead  >
                                <tr>
                                    <th>الرقم التسلسلي </th>
                                    <th>الاسم الثلاثي </th>
                                    <th>العملية</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    Array.isArray(this.state.families) ?
                                        (this.state.families.length > 0) ? this.state.families.map((person, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td> {index + 1}</td> 
                                                    <td>
                                                        <Link to={{
                                                            pathname: '/displayPerson/',
                                                            state: {
                                                                id: person.id,
                                                                num:index
                                                            }
                                                        }}>{person.firstName + ' '}{person.fatherName + ' '}{person.lastName + ' '}</Link>
                                                    </td>
                                                    <td>  <Link to={{
                                                        pathname: '/addResidentInformation',
                                                        state: {
                                                            id: person.id,
                                                            edit: true,
                                                        }
                                                    }}> تعديل </Link>

                                                    </td>
                                                </tr>
                                            )
                                        }) : <tr><td colSpan="5">جاري التحميل...</td></tr>
                                        : <tr><td colSpan="5">جاري التحميل...</td></tr>}
                            </tbody>
                        </table>

                    </center>
                
                    <br />
                    <br />
                </div>
            </div>
        );
    }
}

export default DisplayAssociation;