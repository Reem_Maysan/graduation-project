import React, { Component } from 'react';
import SideBar from '../../SideBar/SideBar';
import '../../Content/Content.css';

import { Link } from 'react-router-dom';
class AllAids extends Component {
  render() {
    return (
      <div>
        <SideBar />
        <div className="rows ">
          <div className="col-sm-4">
            <div className="card card-raduis">
              <div className="card-body">
                <h5 className="card-title">   المساعدات المالية</h5>
                <Link
                  class="btn btn-primary"
                  role="button"
                  to={{
                    pathname: '/addFinicialAid',
                    state: {
                      aid:'Financial',
                  //    id: -1,
                    //  edit: false,
                    }
                  }}> إضافة </Link>
                <Link
                  class="btn btn-primary"
                  role="button"
                  to={{
                    pathname: '/getFinicialAid',
                    state: {
                      aid:'Financial',
                      //id: -1,
                     // edit: false,
                    }
                  }}> عرض </Link>
              </div>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title"> المساعدات العينية </h5>
                <Link
                  class="btn btn-primary"
                  role="button"
                  to={{
                    pathname: '/addConcreteAid',
                    state: {
                      aid:'Material',
                      id: -1,
                      edit: false,
                    }
                  }}> إضافة </Link>
                <Link
                  class="btn btn-primary"
                  role="button"
                  to={{
                    pathname: '/getConcreteAid',
                    state: {
                      aid:'Material',
                      id: -1,
                      edit: false,
                    }
                  }}> عرض </Link>
              </div>
            </div>
          </div>
        </div>

        <div className="rows ">
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title"> المساعدات الصحية المالية</h5>
                <Link
                  class="btn btn-primary"
                  role="button"
                  to={{
                    pathname: '/addFinancialHealthAid',
                    state: {
                      aid:'HealthyFinancial',
                      id: -1,
                      edit: false,
                    }
                  }}> إضافة </Link>
                <Link
                  class="btn btn-primary"
                  role="button"
                  to={{
                    pathname: '/getFinancialHealthAid',
                    state: {
                      aid:'HealthyFinancial',
                      id: -1,
                      edit: false,
                    }
                  }}> عرض </Link>
              </div>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title"> المساعدات الصحية العينية</h5>
                <Link
                  class="btn btn-primary"
                  role="button"
                  to={{
                    pathname: '/addMaterialHealthAid',
                    state: {
                      aid:'HealthyMaterial',
                      id: -1,
                      edit: false,
                    }
                  }}> إضافة </Link>
                <Link
                  class="btn btn-primary"
                  role="button"
                  to={{
                    pathname: '/getMaterialHealthAid',
                    state: {
                      aid:'HealthyMaterial',
                      id: -1,
                      edit: false,
                    }
                  }}> عرض </Link>
              </div>
            </div>
          </div>
        </div>



      </div>


    )
  }



}
export default AllAids;