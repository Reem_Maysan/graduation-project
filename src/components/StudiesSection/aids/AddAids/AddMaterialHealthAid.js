import { Container } from 'react-bootstrap';
import '../../../Routes';
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';
import React, { useState, useEffect } from "react";

import * as Yup from 'yup';
import { Box } from '@material-ui/core';

import {
    Formik,
    Form,
    useFormik,
} from "formik";
import { MyTextField } from '../../../TextFields/formikTextField';

import SideBar from '../../../SideBar/SideBar';
function AddMaterialHealthyAid(props) {

    const [materialAidInfo, setMaterialAidInfo] = useState({
        loading: false,
        jsonBody: {
            IDN: "",
            aidDescription: "",
            materialRequired: 0,
            notes: ""
        }
    });

    const validate = Yup.object({
        IDN: Yup.string()
            .max(7, '7 محارف او اقل')
            .required(myConstClass.Required),
        aidDescription: Yup.string()
            //   .max(15, myConstClass.Less_15)
            .required(myConstClass.Required),
        notes: Yup.string()
            //  .max(30, myConstClass.Less_30)
            .required(myConstClass.Required),
        materialRequired: Yup.number().integer().positive()
            //  .min(10, myConstClass.Less_10)
            .required(myConstClass.Required),
    })

    return (
        <Formik
            initialValues={{
                IDN: materialAidInfo.jsonBody.IDN,
                aidDescription: materialAidInfo.jsonBody.aidDescription,
                materialRequired: materialAidInfo.jsonBody.materialRequired,
                notes: materialAidInfo.jsonBody.notes
            }}
            validationSchema={validate}
            onSubmit={values => {
                console.log('\\\\\\\\\\\\values//////////')
                console.log(values)
                console.log('currentUrl')
                console.log(myConstClass.ADD_AddMaterialHealthyAid)
                setMaterialAidInfo({
                    ...materialAidInfo,
                    loading: true
                });
                fetch(myConstClass.ADD_AddMaterialHealthyAid
                    , {
                        method: 'POST',
                        body: JSON.stringify(
                            values
                        ),
                        headers: {
                            'Accept': 'application/json',
                            'Content-type': 'application/json'
                        },
                    })
                    .then((response) => response.json())
                    .then((json) => {
                        console.log(json)

                    });
                const newJson = {
                    IDN: "",
                    aidDescription: "",
                    materialRequired: 0,
                    notes: ""
                }
                setMaterialAidInfo({
                    ...materialAidInfo,
                    jsonBody: newJson
                });
            }}
        >
            {
                formik => (
                    <div>
                        <SideBar />
                        <div className="rec-contain">
                            <center>
                                <h6> إضافة مساعدة عينية صحية </h6>
                                <Form >
                                    <div className="column border-left">
                                        <Box marginBottom={2}>
                                            <div className="form-row r" >
                                                <MyTextField label="توصيف المساعدة"
                                                    name="aidDescription" type="text"
                                                    placeholder="توصيف المساعدة" />
                                            </div>
                                            <div className="form-row r" >
                                                <MyTextField label="رقم الهوية"
                                                    name="IDN" type="text"
                                                    placeholder="رقم الهوية" />
                                            </div>
                                        </Box>
                                    </div>
                                    <div className="column border-left">
                                        <Box marginBottom={2}>
                                            <div className="form-row r" >
                                                <MyTextField label="القرض المطلوب"
                                                    name="materialRequired" type="text"
                                                    placeholder="القرض المطلوب" />
                                            </div>
                                        </Box>
                                        <Box marginBottom={2}>
                                            <div className="form-row r" >
                                                <MyTextField label="ملاحظات"
                                                    name="notes" type="text"
                                                    placeholder="ملاحظات" />
                                            </div>
                                        </Box>
                                    </div>
                                    <button className="btn btn-primary btn-rounded mr"
                                        type="submit"
                                    >
                                        إضافة
            </button>
                                    <button className="btn btn-primary btn-rounded mr"
                                        type="reset"
                                    >
                                        طلب جديد
            </button>
                                </Form>
                            </center>
                        </div>
                    </div>
                )
            }

        </Formik >
    )
    // return (
    //     <div className="contain">
    //         <h6 className="h6-h2"> إضافة مساعدة صحية عينية  </h6>
    //         <form>
    //             <div className="column border-left">
    //                 <div >
    //                     <div className="form-row r">
    //                         <label className="label" > الرقم الوطني   :</label>
    //                         <input className="input"
    //                             id="IDN"
    //                             value={materialAidInfo.jsonBody.IDN}
    //                             onChange={e => handleChange(e)}
    //                         />
    //                     </div>
    //                     <div className="form-row r">
    //                         <label className="label" >  توصيف المساعدة:</label>
    //                         <textarea
    //                             rows="2"
    //                             id="aidDescription"
    //                             value={materialAidInfo.jsonBody.aidDescription}
    //                             onChange={e => handleChange(e)}
    //                         />
    //                     </div>
    //                 </div>
    //             </div>
    //             <div className="column border-left">
    //                 <div >
    //                     <div className="form-row r">
    //                         <label className="label" > الغرض  المطلوب:</label>
    //                         <input className="input"
    //                             id="IDN"
    //                             value={materialAidInfo.jsonBody.materialRequired}
    //                             onChange={e => handleChange(e)}
    //                         />
    //                     </div>
    //                     <div className="form-row r">
    //                         <label className="label" >  ملاحظات    :</label>
    //                         <textarea
    //                             name="notes"
    //                             id="notes"
    //                             value={materialAidInfo.jsonBody.notes}
    //                             onChange={e => handleChange(e)}
    //                         />
    //                     </div>
    //                 </div>
    //             </div>
    //         </form>
    //         <center>
    //             <button
    //                 onClick={(event) => handleSubmit(event)}
    //                 class="btn btn-primary"
    //                 type="submit"
    //             > حفظ </button>
    //         </center>
    //     </div>
    // )
}
export default AddMaterialHealthyAid;