import React, { useState, useEffect } from "react";
import { Container } from 'react-bootstrap';
import DatePicker from 'react-date-picker';
import '../../../Routes';
import Axios from 'axios';
import * as myConstClass from '../../../../constants';
import axios from 'axios';
import { Link } from 'react-router-dom';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
 
 import * as Yup from 'yup';
import { Box } from '@material-ui/core';

import {
  Formik,
  Field,
  Form,
  useField,
  FieldAttributes,
  FieldArray
} from "formik";
import {
  TextField,
  Button,
  Checkbox,
  Radio,
  FormControlLabel,
  Select,
  MenuItem, InputBase,
  InputLabel,
  TextareaAutosize, FormControl
} from "@material-ui/core";
import { date } from 'yup/lib/locale';

import { createStyles, makeStyles, withStyles, Theme } from '@material-ui/core/styles';

import { MyTextField } from '../../../TextFields/formikTextField';

function AddFund() {

  const [financialAidInfo, setFinancialAidInfo] = useState({
    loading: false,
    jsonBody: {
      IDN: "",
      aidDescription: "",
      financialRequired: 0,
      notes: ""
    }
  });

  const validate = Yup.object({
    IDN: Yup.string()
      .max(7, '7 محارف او اقل')
      .required(myConstClass.Required),
    aidDescription: Yup.string()
      //   .max(15, myConstClass.Less_15)
      .required(myConstClass.Required),
    notes: Yup.string()
      //  .max(30, myConstClass.Less_30)
      .required(myConstClass.Required),
    financialRequired: Yup.number().integer().positive()
      //  .min(10, myConstClass.Less_10)
      .required(myConstClass.Required),
  })

  return (
    <Formik
      initialValues={{
        IDN: financialAidInfo.jsonBody.IDN,
        aidDescription: financialAidInfo.jsonBody.aidDescription,
        financialRequired: financialAidInfo.jsonBody.financialRequired,
        notes: financialAidInfo.jsonBody.notes
      }} 
      validationSchema={validate}
      onSubmit={values => {
        console.log('\\\\\\\\\\\\values//////////')
        console.log(values)
        console.log('currentUrl')
        console.log(myConstClass.ADD_AddFinancialAid)
        setFinancialAidInfo({
          ...financialAidInfo,
          loading: true
        });
        fetch(myConstClass.ADD_AddFinancialAid
          , {
            method: 'POST',
            body: JSON.stringify(
              values
            ),
            headers: {
              'Accept': 'application/json',
              'Content-type': 'application/json'
            },
          })
          .then((response) => response.json())
          .then((json) => {
            console.log(json)

          });
        const newJson = {
          IDN: "",
          aidDescription: "",
          financialRequired: 0,
          notes: ""
        }
        setFinancialAidInfo({
          ...financialAidInfo,
          jsonBody: newJson
        });
      }}
    >
      {
        formik => (
          <div className="rec-contain"> 
            <center>
              <h6> إضافة مساعدة مالية </h6>
              <Form >
                <div className="column border-left">
                  <Box marginBottom={2}>
                    <div className="form-row r" >
                      <MyTextField label="توصيف المساعدة"
                        name="aidDescription" type="text"
                        placeholder="توصيف المساعدة" />
                      <MyTextField label="رقم الهوية"
                        name="IDN" type="text"
                        placeholder="رقم الهوية" />
                    </div>
                  </Box>
                </div>
                <div className="column border-left">
                  <Box marginBottom={2}>
                    <div className="form-row r" >
                      <MyTextField label="القرض المطلوب"
                        name="financialRequired" type="text"
                        placeholder="القرض المطلوب" />
                      <MyTextField label="ملاحظات"
                        name="notes" type="text"
                        placeholder="ملاحظات" />
                    </div>
                  </Box>
                </div>
                <button className="btn btn-primary btn-rounded mr"
                  type="submit"
                >
                  إضافة
              </button>
                <button className="btn btn-green btn-rounded mr"
                  type="reset"
                >
                  طلب جديد
              </button>
              </Form>
            </center>
          </div>
        )
      } 
    </Formik > 
  );
}
export default AddFund;