import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Checkbox from "@material-ui/core/Checkbox";
import './displayAids.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';
import '../../../Routes';
import SideBar from '../../../SideBar/SideBar';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Button } from "@material-ui/core";
import { TrainRounded } from "@material-ui/icons";

import EditIcon from '@material-ui/icons/Edit';


import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';
function DisplayFinancialHealthAid() {

    const [displayFinancialHealthAid, setDisplayFinancialHealthAid] = useState({
        loading: false,
        currentPage: "Archive",
    });

    const [displayArchiveAid, setdisplayArchiveAid] = useState({
        loading: false,
        archiveJsonBody: [{
            id: 0,
            aidDescription: "",
            form_id: 0,
            createdAt: "",
            form: {
                id: 0,
                employee_id: 0,
                firstName: "",
                fatherName: "",
                lastName: "",
                motherFirstName: "",
                motherLastName: "",
                birthPlace: "",
                birthDate: "",
                nationality: "",
                IDN: 0,
                maritalStatus: "",
                education: "",
                ethicalCommitment: "",
                gender: "",
                address: "",
                formType: "",
                formStatus: "",
                familyHistory: "",
                currentWork: "",
                currentWorkAddress: "",
                currentWorkPhone: 0,
                created_at: null,
                updated_at: null
            }
        }],
    });

    const [displayPendingAid, setdisplayPendingAid] = useState({
        loading: false,
        pendingJsonBody: [{
            id: 0,
            aidDescription: "",
            form_id: 0,
            createdAt: "",
            form: {
                id: 0,
                employee_id: 0,
                firstName: "",
                fatherName: "",
                lastName: "",
                motherFirstName: "",
                motherLastName: "",
                birthPlace: "",
                birthDate: "",
                nationality: "",
                IDN: 0,
                maritalStatus: "",
                education: "",
                ethicalCommitment: "",
                gender: "",
                address: "",
                formType: "",
                formStatus: "",
                familyHistory: "",
                currentWork: "",
                currentWorkAddress: "",
                currentWorkPhone: 0,
                created_at: null,
                updated_at: null
            }
        }]
    });

    const financialHealthArchiveInfoGet = async () => {
        console.log('financialHealthArchiveInfoGet financialHealthArchiveInfoGet financialHealthArchiveInfoGet')
        //   setdisplayArchiveAid({ ...displayArchiveAid, loading: true });
        const res = await axios.get(myConstClass.GET_ArchiveFinancialHealthyAidFamilies)
        console.log('json data')
        console.log(res.data['data'])
        setdisplayArchiveAid({ ...displayArchiveAid, jsonBody: res.data['data'] });
    }
    const financialHealthPendingInfoGet = async () => {
        console.log('financialHealthPendingInfoGet financialHealthPendingInfoGet financialHealthPendingInfoGet')
        //  setdisplayPendingAid({ ...displayPendingAid, loading: true });
        const res = await axios.get(myConstClass.GET_PendingFinancialHealthyAidFamilies)
        console.log('json data')
        console.log(res.data['data'])
        setdisplayPendingAid({ ...displayPendingAid, jsonBody: res.data['data'] });
    }

    useEffect(() => {
        console.log('useEffect')
        setDisplayFinancialHealthAid({ ...displayFinancialHealthAid, loading: true });
        financialHealthArchiveInfoGet();
        financialHealthPendingInfoGet();
        setDisplayFinancialHealthAid({ ...displayFinancialHealthAid, loading: false });
    }, []);

    const handleChange = (event) => {
        console.log('hiiii from handleChange')
        setDisplayFinancialHealthAid({ ...displayFinancialHealthAid, currentPage: event.target.value })
        console.log(displayFinancialHealthAid)
    }

    //////////Tab/////////////
    function TabPanel(props) {
        const { children, value, index, ...other } = props;

        return (
            <div
                item="tabpanel"
                hidden={value !== index}
                id={`nav-tabpanel-${index}`}
                aria-labelledby={`nav-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Box p={3}>
                        <Typography>{children}</Typography>
                    </Box>
                )}
            </div>
        );
    }

    TabPanel.propTypes = {
        children: PropTypes.node,
        index: PropTypes.any.isRequired,
        value: PropTypes.any.isRequired,
    };

    function a11yProps(index) {
        return {
            id: `nav-tab-${index}`,
            'aria-controls': `nav-tabpanel-${index}`,
        };
    }

    function LinkTab(props) {
        return (
            <Tab
                component="a"
                onClick={(event) => {
                    event.preventDefault();
                }}
                {...props}
            />
        );
    }

    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
            backgroundColor: theme.palette.background.paper,
        },
    }));

    const handleChangeTab = (event, newValue) => {
        setValue(newValue);
        console.log(value)
        // if (value == 0) {
        //     console.log('0000000000000000000')
        //     materialArchiveInfoGet();

        // } else {
        //     console.log('1111111111111111111')
        //     materialPendingInfoGet();
        // }
    };

    const classes = useStyles();
    const [value, setValue] = useState(0);
    ////////////Tab/////////////

    return (
        <div>
            <SideBar />
         
                {
                    displayFinancialHealthAid.loading === true ?
                        <Backdrop open>
                            <CircularProgress color="inherit" />
                        </Backdrop> :
                        <div className={classes.root}>
                            <AppBar position="static">
                                <Tabs
                                    variant="fullWidth"
                                    value={value}
                                    onChange={handleChangeTab}
                                    aria-label="nav tabs example"
                                >
                                    <LinkTab label="المعلومات المؤرشفة" href="/drafts" {...a11yProps(0)} />
                                    <LinkTab label="المعلومات المعلقة" href="/trash" {...a11yProps(1)} />
                                </Tabs>
                            </AppBar>
                            <TabPanel value={value} index={0}>
                             
                                    <Box marginLeft={10} marginRight={10} marginTop={5}>
                                        <TableContainer component={Paper} >
                                            <Table aria-label="collapsible table root_container root_container-s" >
                                                <TableHead >
                                                    <TableRow >
                                                        <TableCell align="center">رقم المساعدة</TableCell>
                                                        <TableCell align="center">وصف المساعدة</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {
                                                        Array.isArray(displayArchiveAid.archiveJsonBody) ?
                                                            displayArchiveAid.archiveJsonBody.map((aid, index) => (
                                                                <TableRow key={index}>
                                                                    <TableCell align="center">{index + 1}</TableCell>
                                                                    <TableCell align="center">{aid.aidDescription}</TableCell>
                                                                </TableRow>
                                                            )) :
                                                            <TableRow >
                                                                <TableCell align="center">1</TableCell>
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                            </TableRow>
                                                    }
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </Box>
                              
                            </TabPanel>

                            <TabPanel value={value} index={1}>
                               
                                    <Box marginLeft={10} marginRight={10} marginTop={5}>
                                        <TableContainer component={Paper} >
                                            <Table aria-label="collapsible table" >
                                                <TableHead >
                                                    <TableRow >
                                                        <TableCell align="center">رقم المساعدة</TableCell>
                                                        <TableCell align="center">وصف المساعدة</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {
                                                        Array.isArray(displayPendingAid.pendingJsonBody) ?
                                                            displayPendingAid.pendingJsonBody.map((aid, index) => (
                                                                <TableRow key={index}>
                                                                    <TableCell align="center">{index + 1}</TableCell>
                                                                    <TableCell align="center">{aid.aidDescription}</TableCell>
                                                                </TableRow>
                                                            )) :
                                                            <TableRow >
                                                                <TableCell align="center">1</TableCell>
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                            </TableRow>
                                                    }
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </Box>
                         
                            </TabPanel>
                        </div>

                }</div>
      
    );
}
export default DisplayFinancialHealthAid;

/*      <center><br /> <br />
            <div>
                <input type="radio" name="Archive"
                    value="Archive"
                    checked={displayFinancialHealthAid.currentPage === "Archive"}
                    onChange={(e) => handleChange(e)} id="currentPage"
                />
                <label className="form-check-label" > المؤرشفة </label>
                        &nbsp; &nbsp; &nbsp;
                <input type="radio" name="Pending"
                    value="Pending"
                    checked={displayFinancialHealthAid.currentPage === "Pending"}
                    onChange={(e) => handleChange(e)} id="currentPage"
                />
                <label className="form-check-label"  > المعلقة </label>
                <br />
                <br />
            </div>
            {
                displayFinancialHealthAid.currentPage === "Archive" ?
                    displayArchiveAid.loading === true ?
                        <Backdrop open>
                            <CircularProgress color="inherit" />
                        </Backdrop> :
                        <center>
                            <Box marginLeft={10} marginRight={10} marginTop={5}>
                                <TableContainer component={Paper} >
                                    <Table aria-label="collapsible table" >
                                        <TableHead >
                                            <TableRow >
                                                <TableCell align="center">رقم المساعدة</TableCell>
                                                <TableCell align="center">وصف المساعدة</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                Array.isArray(displayArchiveAid.archiveJsonBody['data']) ?
                                                    displayArchiveAid.archiveJsonBody['data'].map((fainancialAid, index) => (
                                                        <TableRow key={index}>
                                                            <TableCell align="center">{index + 1}</TableCell>
                                                            <TableCell align="center">{fainancialAid.aidDescription}</TableCell>
                                                        </TableRow>
                                                    )) :
                                                    <TableRow >
                                                        <TableCell align="center">1</TableCell>
                                                        <TableCell align="center">لا يوجد</TableCell>
                                                    </TableRow>
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Box>
                        </center> :
                    displayPendingAid.loading === true ?
                        <Backdrop open>
                            <CircularProgress color="inherit" />
                        </Backdrop> :
                        <center>
                            <Box marginLeft={10} marginRight={10} marginTop={5}>
                                <TableContainer component={Paper} >
                                    <Table aria-label="collapsible table" >
                                        <TableHead >
                                            <TableRow >
                                                <TableCell align="center">رقم المساعدة</TableCell>
                                                <TableCell align="center">وصف المساعدة</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                Array.isArray(displayPendingAid.pendingJsonBody['data']) ?
                                                    displayPendingAid.pendingJsonBody['data'].map((fainancialAid, index) => (
                                                        <TableRow key={index}>
                                                            <TableCell align="center">{index + 1}</TableCell>
                                                            <TableCell align="center">{fainancialAid.aidDescription}</TableCell>
                                                        </TableRow>
                                                    )) :
                                                    <TableRow >
                                                        <TableCell align="center">1</TableCell>
                                                        <TableCell align="center">لا يوجد</TableCell>
                                                    </TableRow>
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Box>
                        </center>
            }
        </center>
    */