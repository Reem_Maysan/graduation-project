import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Checkbox from "@material-ui/core/Checkbox";

import SideBar from '../../../SideBar/SideBar';
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';
import '../../../Routes';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Button } from "@material-ui/core";
import { TrainRounded } from "@material-ui/icons";

import EditIcon from '@material-ui/icons/Edit';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';

function DisplayMaterialAid(props) {

    const [displayMaterialAid, setDisplayMaterialAid] = useState({
        loading: false,
        currentPage: "Archive",
        jsonBody: [{
            id: 0,
            aidDescription: "",
            form_id: 0,
            createdAt: "",
        }]
    });

    const [displayArchiveMaterialAid, setDisplayArchiveMaterialAid] = useState({
        loading: false,
        archiveJsonBody: [{
            id: 0,
            aidDescription: "",
            form_id: 0,
            createdAt: "",
            form: {
                id: 0,
                employee_id: 0,
                firstName: "",
                fatherName: "",
                lastName: "",
                motherFirstName: "",
                motherLastName: "",
                birthPlace: "",
                birthDate: "",
                nationality: "",
                IDN: 0,
                maritalStatus: "",
                education: "",
                ethicalCommitment: "",
                gender: "",
                address: "",
                formType: "",
                formStatus: "",
                familyHistory: "",
                currentWork: "",
                currentWorkAddress: "",
                currentWorkPhone: 0,
                created_at: null,
                updated_at: null
            }
        }],
    });

    const [displayPendingMaterialAid, setDisplayPendingMaterialAid] = useState({
        loading: false,
        form_pending: {},
        form_id: "",
        materialAidID: "",
        pendingJsonBody: [{
            id: 0,
            aidDescription: "",
            form_id: 0,
            createdAt: "",
            form: {
                id: 0,
                employee_id: 0,
                firstName: "",
                fatherName: "",
                lastName: "",
                motherFirstName: "",
                motherLastName: "",
                birthPlace: "",
                birthDate: "",
                nationality: "",
                IDN: 0,
                maritalStatus: "",
                education: "",
                ethicalCommitment: "",
                gender: "",
                address: "",
                formType: "",
                formStatus: "",
                familyHistory: "",
                currentWork: "",
                currentWorkAddress: "",
                currentWorkPhone: 0,
                created_at: null,
                updated_at: null
            }


        }]
    });

    const materialArchiveInfoGet = async () => {
        console.log('materialArchiveInfoGet materialArchiveInfoGet materialArchiveInfoGet')
        //setDisplayMaterialAid({ ...displayMaterialAid, loading: true });
        const res = await axios.get(myConstClass.GET_ArchiveMaterialAid)
        console.log('json data')
        console.log(res.data['data'])
        // setDisplayMaterialAid({ ...displayMaterialAid, loading: false });
        setDisplayArchiveMaterialAid({ ...displayArchiveMaterialAid, archiveJsonBody: res.data['data'] });
    }
    const materialPendingInfoGet = async () => {
        console.log('materialPendingInfoGet materialPendingInfoGet materialPendingInfoGet')
        //setDisplayMaterialAid({ ...displayMaterialAid, loading: true });
        const res = await axios.get(myConstClass.GET_PendingMaterialAidFamilies)
        console.log('json data')
        console.log(res.data['data'])
        //setDisplayMaterialAid({ ...displayMaterialAid, loading: false });
        setDisplayPendingMaterialAid({ ...displayPendingMaterialAid, pendingJsonBody: res.data['data'] });
    }

    useEffect(() => {
        console.log('useEffect')
        console.log(props)
        setDisplayMaterialAid({ ...displayMaterialAid, loading: true });
        materialArchiveInfoGet();
        materialPendingInfoGet();
        setDisplayMaterialAid({ ...displayMaterialAid, loading: false });
    }, []);

    //////////Tab/////////////
    function TabPanel(props) {
        const { children, value, index, ...other } = props;

        return (
            <div
                item="tabpanel"
                hidden={value !== index}
                id={`nav-tabpanel-${index}`}
                aria-labelledby={`nav-tab-${index}`}
                {...other}
            >
                {value === index && (
                    <Box p={3}>
                        <Typography>{children}</Typography>
                    </Box>
                )}
            </div>
        );
    }

    TabPanel.propTypes = {
        children: PropTypes.node,
        index: PropTypes.any.isRequired,
        value: PropTypes.any.isRequired,
    };

    function a11yProps(index) {
        return {
            id: `nav-tab-${index}`,
            'aria-controls': `nav-tabpanel-${index}`,
        };
    }

    function LinkTab(props) {
        return (
            <Tab
                component="a"
                onClick={(event) => {
                    event.preventDefault();
                }}
                {...props}
            />
        );
    }

    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
            backgroundColor: theme.palette.background.paper,
        },
    }));

    const handleChangeTab = (event, newValue) => {
        setValue(newValue);
        console.log(value)
        // if (value == 0) {
        //     console.log('0000000000000000000')
        //     materialArchiveInfoGet();

        // } else {
        //     console.log('1111111111111111111')
        //     materialPendingInfoGet();
        // }
    };

    const classes = useStyles();
    const [value, setValue] = useState(0);
    ////////////Tab/////////////
    const accept = (material_id, form_id) => {
        setDisplayPendingMaterialAid({
            ...displayPendingMaterialAid,
            materialAidID: material_id,
            form_id: form_id
        });
        console.log(material_id);
        console.log(form_id);
    }

    return (
        <div>
            <SideBar />
            <center>
                {
                    displayMaterialAid.loading === true ?
                        <Backdrop open>
                            <CircularProgress color="inherit" />
                        </Backdrop> :
                        <div className={classes.root}>
                            <AppBar position="static">
                                <Tabs
                                    variant="fullWidth"
                                    value={value}
                                    onChange={handleChangeTab}
                                    aria-label="nav tabs example"
                                >
                                    <LinkTab label="المساعدات المؤرشفة" href="/drafts" {...a11yProps(0)} />
                                    <LinkTab label="المساعدات المعلقة" href="/trash" {...a11yProps(1)} />
                                </Tabs>
                            </AppBar>
                            <TabPanel value={value} index={0}>
                                <div className="container_rectangle">
                                    <Box marginLeft={10} marginRight={10} marginTop={5}>
                                        <TableContainer component={Paper} >
                                            <Table aria-label="collapsible table" >
                                                <TableHead >
                                                    <TableRow >
                                                        <TableCell align="center">رقم المساعدة</TableCell>
                                                        <TableCell align="center">وصف المساعدة</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {
                                                        Array.isArray(displayArchiveMaterialAid.archiveJsonBody) ?
                                                            displayArchiveMaterialAid.archiveJsonBody.map((aid, index) => (
                                                                <TableRow key={index}>
                                                                    <TableCell align="center">{index + 1}</TableCell>
                                                                    <TableCell align="center">{aid.aidDescription}</TableCell>
                                                                </TableRow>
                                                            )) :
                                                            <TableRow >
                                                                <TableCell align="center">1</TableCell>
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                            </TableRow>
                                                    }
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </Box>
                                </div>
                            </TabPanel>
                            <TabPanel value={value} index={1}>
                                <div className="container_rectangle">
                                    <Box marginLeft={10} marginRight={10} marginTop={5}>
                                        <TableContainer component={Paper} >
                                            <Table aria-label="collapsible table" >
                                                <TableHead >
                                                    <TableRow >
                                                        <TableCell align="center">رقم المساعدة</TableCell>
                                                        <TableCell align="center">وصف المساعدة</TableCell>
                                                        <TableCell align="center">العملية</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {
                                                        Array.isArray(displayPendingMaterialAid.pendingJsonBody) ?
                                                            displayPendingMaterialAid.pendingJsonBody.map((aid, index) => (
                                                                <TableRow key={index}>
                                                                    <TableCell align="center">{index + 1}</TableCell>
                                                                    <TableCell align="center">{aid.aidDescription}</TableCell>
                                                                    <TableCell align="center"> <Link path="/acceptMAterialAid/"
                                                                        onClick={() => { accept(aid.id, aid.form.id) }}
                                                                    >قبول</Link>
                                                                    </TableCell>

                                                                </TableRow>
                                                            )) :
                                                            <TableRow >
                                                                <TableCell align="center">1</TableCell>
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                            </TableRow>
                                                    }
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </Box>
                                </div>
                            </TabPanel>
                        </div>
                }</center>
        </div>
    );
}
export default DisplayMaterialAid;

