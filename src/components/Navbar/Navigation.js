import React, { Component } from 'react' ;
//import { FaBeer } from "@react-icons/all-files/fa/FaBeer";
import IconButton from '@material-ui/core/IconButton';
import './Navigation.css';
import PersonIcon from '@material-ui/icons/Person';
import SearchIcon from '@material-ui/icons/Search';
import { Nav , Link ,Form, Button, FormGroup, FormControl, ControlLabel , InputGroup , NavLink , NavItem} from "react-bootstrap";
class Navigation extends Component{
render(){
    const { FaIcon, FaStack } = require('react-fa-icon');
  
  
    return(
        
  <nav className="navbar navbar-dark  navbar-color nav_v">
       
   
  <div className="container-fluid">
  <Form inline>
           
            <div className="title"><PersonIcon className="icon_person"/> {"  " + localStorage.getItem('name')}    </div>
         
   {/* <div className="search">
    <SearchIcon />
    <input className="search-input" type="text" placeholder="ابحث" aria-label="Search"/>
    </div>*/}
    </Form>
    <Nav >

		<Nav.Link className="navItem" href="/" > تسجيل الخروج </Nav.Link>	
  
		</Nav>
  </div>
</nav>


    );}}
    export default Navigation;