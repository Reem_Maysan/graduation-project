import React, { Component } from 'react';
import SideBar from '../SideBar/SideBar';
import '../Content/Content'
class EdicationSystemList extends Component {
  render() {
    return (
      <div >
        <SideBar />
        <div className="rows ">
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">  الدورات والمواد  </h5>
                <center>
                <a class="btn btn-primary" href="/managecoursesAndSubjects" role="button"> عرض </a>
                </center>
              </div>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">     المناسبات </h5>
                <center>
                <a class="btn btn-primary" href="/eventsAndVications" role="button"> دخول </a>
                </center>
              </div>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">    الاستفسارات  </h5>
                <center>
                <a class="btn btn-primary" href="/inquiries" role="button"> دخول </a>
                </center>
              </div>
            </div>
          </div>
     
        </div>
        <div className="rows ">
        <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">    إدارة الطلاب </h5>
                <center>
                <a class="btn btn-primary" href="/managestudents" role="button"> دخول </a>
                </center>
              </div>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">   إدارة أولياء الأمور </h5>
                <center>
                <a class="btn btn-primary" href="/managepearents" role="button"> عرض </a>
                </center>
              </div>
            </div>
          </div>
          <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">    إدارة المعلمين</h5>
                <center>
                <a class="btn btn-primary" href="/manageteacher" role="button"> دخول </a>
                </center>
              </div>
            </div>
          </div>
     
        </div>
        <div className="rows ">
        <div className="col-sm-4">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">   الشكاوى   </h5>
                <center>
                <a class="btn btn-primary" href="/getManagerComplaints" role="button"> دخول </a>
                </center>
              </div>
            </div>
          </div>
    

     
        </div>
 
      </div>


    )
  }



}
export default EdicationSystemList;