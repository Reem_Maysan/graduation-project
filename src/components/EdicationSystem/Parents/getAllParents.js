import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import SideBar from '../../SideBar/SideBar';
import { Button} from 'react-bootstrap';
import * as myConstClass from '../../../constants';
class getAllParents extends Component {

  constructor(props) {
    super(props);
    this.state = {
        parents: [],
    }
  }
  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.getParentsOfEducation);
    console.log("res is 1" + res.data["data"])
    this.setState({
        parents: res.data["data"],
    })
  }
  render() {
    return (
        
      <div>
        <SideBar />
        <div>
          <center>
            <table className="show-table" size="sm" >
              <thead>
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th> الاسم الثلاثي   </th>
                  <th>اسم المستخدم </th>
                  <th>العنوان  </th>
                  <th>موبايل  </th>
                  <th>رقم الهاتف (أرضي)  </th>
                  <th> الرقم الوطني </th>
                  <th>  تم الإنشاء </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {(this.state.parents.length > 0) ? this.state.parents.map((parent, index) => {
                  return (
                    <tr key={index}>
                      <td> {index + 1}</td>
                      <td> {parent.firstName}  {parent.fatherName}   {parent.lastName} </td>
                      <td>{parent.userName} </td>
                      <td>{parent.address} </td>
                      <td>{parent.phoneNumber} </td>
                      <td>{parent.homePhone} </td>
                      <td>{parent.IDN} </td>
                      <td>{parent.created_at} </td>
                    
                    </tr>
                  )
                }) : <tr><td colSpan="5">جاري التحميل...</td></tr>}
              </tbody>
            </table>
          </center>
        </div>
      </div>
    );
  }
}

export default getAllParents;