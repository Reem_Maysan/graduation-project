import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import SideBar from '../../SideBar/SideBar';
import { Button} from 'react-bootstrap';
import * as myConstClass from '../../../constants';
class getAllComplaints extends Component {

  constructor(props) {
    super(props);
    this.state = {
        complaints: [],
    }
  }
  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.getComplaintsOfEducation);
    console.log("res is 1" + res.data["data"])
    this.setState({
        complaints: res.data["data"],
    })
  }
  render() {
    return (
        
      <div>
        <SideBar />
        <div>
          <center>
            <table className="show-table" size="sm" >
              <thead  >
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th>محتوى الشكوى   </th>
                  <th>تاريخ الإنشاء</th>
                </tr>
              </thead>
              <tbody>
                {(this.state.complaints.length > 0) ? this.state.complaints.map((complaint, index) => {
                  return (
                    <tr key={index}>
                      <td> {index + 1}</td>

                      <td>{complaint.complaintContent}</td>
                      <td>{complaint.created_at}</td>
                    </tr>
                  )
                }) : <tr><td colSpan="5">جاري التحميل...</td></tr>}
              </tbody>
            </table>
          </center>
        </div>
      </div>
    );
  }
}

export default getAllComplaints;