import react, { Component } from 'react';
import SideBar from '../../SideBar/SideBar';
import "../EdicationSystem.css";
import * as myConstClass from '../../../constants';
import axios from 'axios';
import { render } from '@testing-library/react';
import { Button} from 'react-bootstrap';

class AddEvent extends Component{

    constructor(props) {
        super(props);
        this.state = {
            name:"",
            description:"",
            eventDate :"",
            emp_id:"",
            employees :[]
            
        }
      
        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
      }
       
      changeHandler = e =>{
        this.setState({
            [e.target.name ] : e.target.value
        })
      }
        
    submitHandler = e =>{
      e.preventDefault();
      let data = new FormData();
      data.append('name', this.state.name);
      data.append('description', this.state.description);
      data.append('eventDate', this.state.eventDate);
      for (var value of data.values()) {
        console.log("data is " + value);
     }
    
      console.log(data);
       axios({
        method: 'post',
        headers: {   Authorization :  'Bearer ' + localStorage.getItem('token') },
        url: myConstClass.addEventfEducation,
        data: data
      }).then(res =>{
        console.log(res.data);
        if(res = 200){
        console.log("okkkk");
        alert("تمت عملية الإضافة بنجاح!")
        window.location.href = "/content";
        }
    }
    ).catch(
    )
      
    
       }

        render(){
        return(
           
            <div >
            <SideBar />
            <div className="rec-contain-s">
            <h6>إضافة حدث </h6>
            <div className="form-row r">
            <label className="label" > اسم الحدث  :</label>
             <input className="input" name="name" type="text" onChange={this.changeHandler}/>
              </div>
              <div className="form-row r">
             <label className="label" >  الوصف  :</label>
             <textarea className="input" name="description" type="text" onChange={this.changeHandler}/>
              </div>
              <div className="form-row r">
             <label className="label" >  تاريخ الحدث   :</label>
             <input className="input" name="eventDate" type="date" onChange={this.changeHandler}/>
              </div> 
              <center><button className="t-button" onClick ={this.submitHandler}> حفظ </button></center> 
              </div>                
            </div>
        )
    }
    
}

export default AddEvent;