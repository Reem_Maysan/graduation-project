import react, { Component } from 'react';
import SideBar from '../../SideBar/SideBar';
import "../EdicationSystem.css";
import * as myConstClass from '../../../constants';
import axios from 'axios';
import { render } from '@testing-library/react';
import { Button} from 'react-bootstrap';

class getInfoOfEvent extends Component{

    constructor(props) {
        super(props);
        this.state = {
            id : this.props.match.params.id ,
            event : {}
            
        }
      
       
      }
       
      componentDidMount() {
        this.refreshList();
      }
    
      async refreshList() {
        const config = {
          headers : {
            Authorization :  'Bearer ' + localStorage.getItem('token')
          }
        };
        const res = await axios.get(myConstClass.getEventInfoOfEducation + this.state.id , config)
        console.log(res.body);
        console.log(res.data);
        this.setState({
          event: res.data["data"],
        })
       
      }

        render(){
            const {event} =this.state;
        return(
           
            <div >
            <SideBar />
            <div className="rec-contain-s">
            <h6>معلومات المناسبة  </h6>
            <div className="form-row r">
            <label className="label" > اسم المناسبة  :</label>
             <input className="input" name="name" type="text" value={event.name} readOnly/>
              </div>
              <div className="form-row r">
             <label className="label" >  الوصف  :</label>
             <textarea className="input" name="description" type="text" value={event.description} readOnly/>
              </div>
              <div className="form-row r">
             <label className="label" >  تاريخ المناسبة   :</label>
             <input className="input" name="eventDate" type="date"  value={event.eventDate} readOnly/>
              </div> 
              
              </div>                
            </div>
        )
    }
    
}

export default getInfoOfEvent;