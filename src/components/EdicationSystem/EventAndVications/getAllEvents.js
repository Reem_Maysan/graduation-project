import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import SideBar from '../../SideBar/SideBar';
import { Button} from 'react-bootstrap';
import * as myConstClass from '../../../constants';
class getAllEvents extends Component {

  constructor(props) {
    super(props);
    this.state = {
      events: [],
    }
  }
  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const config = {
      headers : {
        Authorization :  'Bearer ' + localStorage.getItem('token')
      }
    };
    const res = await axios.get(myConstClass.getEventsOfEducation , config)
    console.log(res.data)
    this.setState({
      events: res.data["data"],
    })
   
  }

  render() {
   
    return (
        
      <div>
        <SideBar />
        <div>
          <center>
           <br/>   
          <Button className ="add_button_ed" onClick={()=> window.location.href ="/addEvent"} > إضافة مناسبة </Button>
            <table className="show-table" size="sm" >
              <thead  >
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th>اسم الحدث  </th>
                  <th>  وقت الإنشاء </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {(this.state.events.length > 0) ? this.state.events.map((event, index) => {
                  return (
                    <tr key={index}>
                      <td> {index + 1}</td>
                      <td>{event.name} </td>
                      <td> {event.created_at}</td>
                      <td><Link  to = {'/getFullInfoOfEvent/' + event.id} > عرض </Link>
                      </td>
                    </tr>
                  )
                }) : <tr><td colSpan="5">جاري التحميل...</td></tr>}
              </tbody>
            </table>
          </center>
        </div>
      </div>
    );
  }
}

export default getAllEvents;