import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import SideBar from '../../SideBar/SideBar';
import { Button} from 'react-bootstrap';
import * as myConstClass from '../../../constants';
class getAllTeachers extends Component {

  constructor(props) {
    super(props);
    this.state = {
        teachers: [],
    }
  }
  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.getTeachersOfEducation);
    console.log("res is 1" + res.data["data"])
    this.setState({
        teachers: res.data["data"],
    })
  }
  render() {
    return (
        
      <div>
        <SideBar />
        <div>
          <center>
           <br />   
          
            <table className="show-table" size="sm" >
              <thead  >
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th> الاسم الثلاثي  </th>
                  <th> اسم المستخدم </th>
                  <th> رقم الهاتف   </th>
                  <th>الرقم الوطني </th>
                  <th>تمت إضافته  </th>
                  
                </tr>
              </thead>
              <tbody>
                {(this.state.teachers.length > -1) ? this.state.teachers.map((teacher, index) => {
                  return (
                      <tr key={index}>
                      <td> {index + 1}</td>
                      <td> {teacher.firstName} {teacher.fatherName} {teacher.lastName}  </td>
                      <td> {teacher.userName } </td>
                      <td> {teacher.phoneNumber } </td>
                      <td> {teacher.IDN } </td>
                      <td> {teacher.created_at } </td>
                      
                    </tr>
                  )
                }) : <tr><td colSpan="5">جاري التحميل...</td></tr>}
              </tbody>
            </table>
          </center>
        </div>
      </div>
    );
  }
}

export default getAllTeachers;