import react, { Component } from 'react';
import SideBar from '../../SideBar/SideBar';
import "../EdicationSystem.css";
import * as myConstClass from '../../../constants';
import axios from 'axios';
import { render } from '@testing-library/react';
import { Button} from 'react-bootstrap';

class getInfoOfTeacher extends Component{

    constructor(props) {
        super(props);
        this.state = {
            id : this.props.match.params.id ,
            teacherInfo : [],
            teacherCourses : [] ,
            students : []
        
            
        }
      
       
      }
       
      componentDidMount() {
        this.refreshList();
      }
    
      async refreshList() {
        const config = {
          headers : {
            Authorization :  'Bearer ' + localStorage.getItem('token')
          }
        };
        const res = await axios.get(myConstClass.getInfoTeacher + this.state.id , config);
        if(res.status == 200){
          console.log(res.data["data"]);
            console.log("goodssssssssssssssssss");
            console.log("/////////////////////////////////////");
            console.log(res.data["data"].teacher);
            console.log("/////////////////////////////////////");
            console.log(res.data["data"].courses );
            console.log("/////////////////////////////////////");
            console.log( res.data["data"].students );
            console.log("/////////////////////////////////////");
          this.setState({
           teacherInfo : res.data["data"],
     

          })
        }else{
          console.log("no");
        }
        
       
      }

        render(){
          const {teacherInfo} = this.state;
        return(
           
            <div >
            <SideBar />
            <div className="rec-contain-s">
            <h6> معلومات المعلم   </h6>
            <h6> المعلومات الشخصية    </h6>
           { this.state.teacherInfo.map((teacherInfo) =>(
             <div>
            <div className="form-row r">
             <label className="label" > الاسم الأول      :</label>
             <input  name="name" type="text"  value={teacherInfo.firstName} readOnly/>
             <label className="label" > اسم الأب    :</label>
             <input  name="name" type="text" value={teacherInfo.lastName} readOnly/>
             <label className="label" >  الكنية    :</label>
             <input  name="name" type="text" value={teacherInfo.fatherName} readOnly/>
              </div>
              <h6>  معلومات المواد الحالية    </h6>
               <table className="show-table" size="sm" >
               <thead  >
                 <tr>
                   <th>الرقم التسلسلي </th>
                   <th> اسم المادة   </th>
                   <th> أسماء الطلاب    </th>
                 </tr>
               </thead>
               <tbody>
                 {teacherInfo.teacherCourses.data.map((course, index) =>(
                     <tr>
                       <td> {index+1} </td>
                       <td> {course.fullName} </td>
                     </tr>
                   ))}
                
               </tbody>
             </table>
             <br/>



             <h6>  معلومات الطلاب     </h6>
               <table className="show-table" size="sm" >
               <thead  >
                 <tr>
                   <th>الرقم التسلسلي </th>
                   <th> اسم الطالب   </th>
                   
                 </tr>
               </thead>
               <tbody>
                 {teacherInfo.students.data !="No Students" ? teacherInfo.students.data.map((student, index) =>(
                   student.data.map((stu , index) =>(
                    <tr>
                   <td> {index+1} </td>
                   <td> {stu.fullName} </td>
                   </tr>
                    ))
                   )) :"No Students"
                   }
               </tbody>
             </table>
                 <br/>
             
              </div>
           ))}
             
              
                  
               

      
             
            
              
              
              </div>                
            </div>
        )
    }
    
}

export default getInfoOfTeacher;