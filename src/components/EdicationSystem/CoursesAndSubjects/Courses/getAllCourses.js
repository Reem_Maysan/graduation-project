import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import SideBar from '../../../SideBar/SideBar';
import { Button} from 'react-bootstrap';
import * as myConstClass from '../../../../constants';
import "../../EdicationSystem.css";
class getAllCourses extends Component {

  constructor(props) {
    super(props);
    this.state = {
      courses: [],
    }
  }
  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.getCategoriesOfEducation);
    console.log(res.data)
    this.setState({
      courses: res.data["data"],
    })
   
  }


  async archive(id) {
    axios.get("https://thawing-everglades-11191.herokuapp.com/api/v1/educationDep/categories/archiveCategory/" + id).then(
      res =>{
          console.log("")
          console.log(res);
          if(res = 200){
           console.log("تمت عملية الأرشفة بنجاح");
           alert(" تمت عملية الأرشفة بنجاح !");
           this.refreshList();
          }
      }
  ).catch(
      error =>{
          console.log(error)
          console.log("  خطأ في عملية الارشفة ");
      })
    }
    
  render() {
   
    return (
        
      <div>
        <SideBar />
        <div>
          <center>
          <br/>
          <Button className ="add_button_ed" onClick={()=> window.location.href ="/addCourse"} > إضافة دورة </Button>
            <table className="show-table" size="sm" >
              <thead  >
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th>اسم الدورة  </th>
                  <th></th>
             
                </tr>
              </thead>
              <tbody>
                {(this.state.courses.length > 0) ? this.state.courses.map((course, index) => {
                  return (
                    <tr key={index}>
                      <td> {index + 1}</td>
                     <Link to = {'/getFullInfo/' + course.id}> <td>  {course.name} </td></Link>
                      <td><Link to = {'/editCourse/' + course.id}> تعديل </Link>
                          <Link  onClick  = {()=>{this.archive(course.id)}}> أرشفة </Link>
                      </td>
                    </tr>
                  )
                }) : <tr><td colSpan="5">جاري التحميل...</td></tr>}
              </tbody>
            </table>
          </center>
        </div>
      </div>
    );
  }
}

export default getAllCourses;