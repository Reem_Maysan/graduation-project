import react, { Component } from 'react';
import SideBar from '../../../SideBar/SideBar';
import EditIcon from '@material-ui/icons/Edit';
import "../../EdicationSystem.css";
import React, { useState, useEffect } from "react";
import * as myConstClass from '../../../../constants';
import axios from 'axios';
import { render } from '@testing-library/react';


class getFullInfoOfCourse extends Component{

    constructor(props) {
        super(props);
        this.state = {
            id : this.props.match.params.id ,
            info :{} ,
            name:"",
            description:"",
            startingDate:"",
            finishingDate:""
            
        }
      
        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
      }
       
      changeHandler = e =>{
        this.setState({
            [e.target.name ] : e.target.value
        })
      }
      
      componentDidMount() {
        this.refreshList();
      }
    
      async refreshList() {
        const res = await axios.get(myConstClass.getCategorieyFullInfoOfEducation + id );
        console.log(res.data)
        this.setState({
          info: res.data["data"],
        })
       
      }
  /*  submitHandler = e =>{
      e.preventDefault();
      let data = new FormData();
      data.append('name', this.state.name);
      data.append('description', this.state.description);
      data.append('startingDate', this.state.startingDate);
      data.append('finishingDate', this.state.finishingDate);
      for (var value of data.values()) {
        console.log("the data is " + value);
     }
    
      console.log(data);
       axios({
        method: 'post',
      //  headers: {   Authorization :  'Bearer ' + localStorage.getItem('token') },
        url:  myConstClass.addCategoryOfEducation,
        data: data
      }).then(res =>{
        console.log(res.data);
        if(res = 200){
        console.log("the request is ok");
        window.location.href = "/courses";
        }else{
          console.log("erroer");
        }
    }
    ).catch(
    )
      
    
       }*/
        render(){
            const {name , description , startingDate , finishingDate} = this.state;
        return(
           
            <div >
                 <SideBar />
             <div className="rec-contain-s">
            <h6>إضافة  دورة  </h6>
            <div className="form-row r">
             <label className="label" > اسم الدورة  :</label>
             <input className="input" name="name" type="text" value={this.state.info.name}/>
              </div>
              <div className="form-row r">
             <label className="label" >  الوصف  :</label>
             <textarea className="input" name="description" type="text" />
              </div>
              <div className="form-row r">
             <label className="label" >  تاريخ البدء  :</label>
             <input className="input" name="startingDate" type="date"/>
              </div>
              <div className="form-row r">
             <label className="label" >  تاريخ الانتهاء   :</label>
             <input className="input" name="finishingDate" type="date" />
              </div>
                   
         
              </div>
        
            
        
           
            
           
                
            </div>
        )
    }
    }


export default getFullInfoOfCourse;