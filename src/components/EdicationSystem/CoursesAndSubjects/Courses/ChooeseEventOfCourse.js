import React, { Component } from 'react';
import SideBar from '../../../SideBar/SideBar';
import '../../../Content/Content'
class EventOfSubject extends Component {
  render() {
    return (
      <div >
        <SideBar />
        <div className="rows ">
          <div className="col-sm-6">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">   إضافة مادة جديدة </h5>
                <center>
                <a class="btn btn-primary" href="/addCourse" role="button"> عرض </a>
                </center>
              </div>
            </div>
          </div>
          <div className="col-sm-6">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">  عرض المواد  </h5>
                <center>
                <a class="btn btn-primary" href="/viewCourse" role="button"> دخول </a>
                </center>
              </div>
            </div>
          </div>
     
        </div>
        <div className="rows ">
          <div className="col-sm-6">
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">   المواد المؤرشفة </h5>
                <center>
                <a class="btn btn-primary" href="/archiveCourse" role="button"> عرض </a>
                </center>
              </div>
            </div>
          </div>
         
     
        </div>
  
 
      </div>


    )
  }



}
export default EventOfSubject;