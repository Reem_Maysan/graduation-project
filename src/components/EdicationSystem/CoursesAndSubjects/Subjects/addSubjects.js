import react, { Component } from 'react';
import SideBar from '../../../SideBar/SideBar';
import EditIcon from '@material-ui/icons/Edit';
import "../../EdicationSystem.css";
import React, { useState, useEffect } from "react";
import * as myConstClass from '../../../../constants';
import axios from 'axios';
import { render } from '@testing-library/react';


class AddSubject extends Component{

    constructor(props) {
        super(props);
        this.state = {
            name:"",
            description:"",
            cost :"",
            weaklyHours:"",
            categoryID:"",
            categories :[]
            
        }
      
        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
      }
       
      changeHandler = e =>{
        this.setState({
            [e.target.name ] : e.target.value
        })
      }
      
    
     
      componentDidMount() {
        this.refreshList();
      }
    
      async refreshList() {
        const res = await axios.get(myConstClass.getCategoriesOfEducation)
        console.log(res.data)
        this.setState({
          categories: res.data["data"],
    
        })
      }
        
    submitHandler = e =>{
      e.preventDefault();
      let data = new FormData();
      data.append('name', this.state.name);
      data.append('description', this.state.description);
      data.append('cost', this.state.cost);
      data.append('weaklyHours', this.state.weaklyHours);
      data.append('categoryID', this.state.categoryID);
      for (var value of data.values()) {
        console.log("data is " + value);
     }
    
      console.log(data);
       axios({
        method: 'post',
    //    headers: {   Authorization :  'Bearer ' + localStorage.getItem('token') },
        url: myConstClass.addCourseOfEducation,
        data: data
      }).then(res =>{
        console.log(res.data);
        if(res = 200){
        console.log("okkkk");
        window.location.href = "/subjects";
        }
    }
    ).catch(
    )
      
    
       }
        render(){
            const {name,description,cost ,weaklyHours,categoryID,categories} = this.state;
        return(
           
            <div >
                 <SideBar />
             <div className="rec-contain-s">
            <h6>إضافة مادة جديدة</h6>
            <div className="form-row r">
             <label className="label" > اسم المادة  :</label>
             <input className="input" name="name" type="text" onChange={this.changeHandler}/>
              </div>
              <div className="form-row r">
             <label className="label" >  الوصف  :</label>
             <textarea className="input" name="description" type="text" onChange={this.changeHandler}/>
              </div>
              <div className="form-row r">
             <label className="label" >  السعر  :</label>
             <input className="input" name="cost" type="number" onChange={this.changeHandler}/>
              </div>
              <div className="form-row r">
             <label className="label" >  عدد الساعات الأسبوعية  :</label>
             <input className="input" name="weaklyHours" type="number" onChange={this.changeHandler}/>
              </div>
              <div className="form-row r">
              <label style={{margin:15}}><b>اختر الدورة  </b></label>
              <select style ={{height:40 , width:300 , borderRadius:5}}  value={this.state.categoryID}
               onChange={(e) => this.setState({ categoryID : e.target.value })}>
                {this.state.categories.map(category => (
                     <option key={category.id} value={category.id}>{category.name}</option>
                       ))}
               </select>
              </div>    
                   
              <center><button className="t-button" onClick ={this.submitHandler}> حفظ </button></center> 
          
              </div>
        
            
        
           
            
           
                
            </div>
        )
    }
    }


export default AddSubject;