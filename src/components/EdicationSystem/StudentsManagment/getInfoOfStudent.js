import react, { Component } from 'react';
import SideBar from '../../SideBar/SideBar';
import "../EdicationSystem.css";
import * as myConstClass from '../../../constants';
import axios from 'axios';
import { render } from '@testing-library/react';
import { Button} from 'react-bootstrap';

class getInfoOfStudents extends Component{

    constructor(props) {
        super(props);
        this.state = {
            id : this.props.match.params.id ,
            personalInfo : {} ,
            activeCoursesInfo :[] 
        } 
      }
       
      componentDidMount() {
        this.refreshList();
      }
    
      async refreshList() {
        const config = {
          headers : {
            Authorization :  'Bearer ' + localStorage.getItem('token')
          }
        };
        const res = await axios.get(myConstClass.getInfoStudent + this.state.id , config);
        if(res.status = 200){
            console.log("goodssssssssssssssssss");
            console.log("/////////////////////////////////////");
            console.log(res.data["data"].personalInfo);
            console.log("/////////////////////////////////////");
            console.log(res.data["data"].activeCoursesInfo.data );
            console.log("/////////////////////////////////////");
            console.log( res.data["data"].archivedCoursesInfo );
            console.log("/////////////////////////////////////");
            console.log( res.data["data"].marks);
            console.log( res.data["data"].rates);
            console.log( res.data["data"].generalRate);
          this.setState({
            personalInfo : res.data["data"].personalInfo ,
            activeCoursesInfo : res.data["data"].activeCoursesInfo.data
          })
        }else{
          console.log("no");
        }
        
       
      }

        render(){
          const {personalInfo} = this.state;
        return(
           
            <div >
            <SideBar />
            <div className="rec-contain-s">
            <h6> معلومات الطالب   </h6>
            <h6> المعلومات الشخصية    </h6>
            <div className="form-row r">
             <label className="label" > الاسم الثلاثي    :</label>
             <input  name="name" type="text" 
             value={personalInfo.firstName  + " " + personalInfo.fatherName  + " " + personalInfo.lastName } readOnly/>
             <label className="label" > اسم الأم    :</label>
             <input  name="name" type="text" value={personalInfo.motherName} readOnly/>
             <label className="label" >  تاريخ الولادة   :</label>
             <input  name="name" type="text" value={personalInfo.birthDate} readOnly/>
              </div>

              <h6>  معلومات المواد الحالية    </h6>
              <center>
                  
                <table className="show-table" size="sm" >
                  <thead  >
                    <tr>
                      <th>الرقم التسلسلي </th>
                      <th> اسم المادة   </th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    {(this.state.activeCoursesInfo.length > -1) ? this.state.activeCoursesInfo.map((activecourse, index) => {
                      return (
                        <tr key={index}>
                          <td> {index + 1}</td>
    
                          <td> {activecourse.data.name} </td>
                          
                        
                        </tr>
                      )
                    }) : <tr><td colSpan="5">Loading...</td></tr>}
                  </tbody>
                </table>
              </center>
            
              
              </div>                
            </div>
        )
    }
    
}

export default getInfoOfStudents;