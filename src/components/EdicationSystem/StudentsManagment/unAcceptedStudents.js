import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import SideBar from '../../SideBar/SideBar';
import { Button} from 'react-bootstrap';
import { DialogComponent } from '@syncfusion/ej2-react-popups';
import * as myConstClass from '../../../constants';
import Dialog from "@material-ui/core/Dialog";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
class UnAcceptedStudent extends Component {

  constructor(props) {
    super(props);
    this.state = {
        un_acc_students: [],
        open : false,
        pValue : "",
        course_id :"" ,
        student_id :""
    }
   
    this.changeHandler = this.changeHandler.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
  }
  
handleClickOpen() {
    this.setState({ open: true });
}
handleClickClose() {
    this.setState({
         open: false , 
         
        });
}

changeHandler = e =>{
    this.setState({
        [e.target.name ] : e.target.value
    })
}

  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.getPendingStudentsOfEducation);
    if(res.status = 200){
    console.log(res.body);
    console.log(res.data["data"]);
    console.log("good");
    this.setState({
        un_acc_students: res.data["data"],
    })
  }else{
    console.log("error");
  }
}
  async accept(stu_id , course_id) {
    this.setState({
       open: true ,
       student_id : stu_id ,
       course_id : course_id
    });
    console.log(stu_id);
    console.log(course_id);
    }


    submitHandler = e =>{
      e.preventDefault();
      let data = new FormData();
      data.append('course_id', this.state.course_id);
      data.append('student_id', this.state.student_id);
      data.append('pValue', this.state.pValue);
      for (var value of data.values()) {
        console.log("the data is " + value);
     }
      console.log(data);
       axios({
        method: 'post',
        url:  myConstClass.addPaymentOfStudent,
        data: data
      }).then(res =>{
        console.log(res.data);
        if(res = 200){
        console.log("the request is ok");
        window.location.href = "/unAcceptedStudents";
        }else{
          console.log("erroer");
        }
    }
    ).catch(
    )
      
    
       }
  render() {
    return (
        
      <div>
       
        <div>
          <center>
            <table className="show-table" size="sm" >
              <thead  >
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th> الاسم الثلاثي   </th>
                  <th> اسم المادة</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {(this.state.un_acc_students.length > 0) ? 
                this.state.un_acc_students.map((un_stu, index) => {
                  return (
                    <tr key={index}>
                      <td> {index + 1}</td>
                      <td>{un_stu.student.firstName} {un_stu.student.fatherName} {un_stu.student.lastName} </td>
                      <td>{un_stu.course.name}</td>
                      <td><Link to={{
                            pathname: "/unAcceptedStudents",
                        }}
                      type="button"
                      
                      onClick = {()=>{this.accept(un_stu.student.id , un_stu.course.id)} }> قبول </Link></td>
                      <Dialog open={this.state.open} onClose={this.handleClickClose}>
                        <DialogTitle>{"الرجاء تحديد سعر المادة "}</DialogTitle>
                        <DialogContent>
                        <input autoFocus  margin="dense" id="pValue" name ="pValue" type="number"
                         onChange ={this.changeHandler}
                        fullWidth />
                        </DialogContent>
                        <DialogActions>
                        <Button onClick={this.submitHandler} color="primary" autoFocus>تأكيد </Button>
                        <br/>
                        <Button onClick={this.handleClickClose} color="primary" autoFocus> إغلاق  </Button>
                        </DialogActions>
                       </Dialog>
                    </tr>
                  )
                }) : <tr><td colSpan="5">جاري التحميل...</td></tr>}
              </tbody>
            </table>
          
          </center>

        </div>
      </div>
    );
  }
}

export default UnAcceptedStudent;