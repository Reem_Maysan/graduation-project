import React, { useState, useEffect } from "react";
import SideBar from '../../SideBar/SideBar';
import '../../Content/Content';
import '../EdicationSystem.css';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Checkbox from "@material-ui/core/Checkbox";
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../constants';
import '../../Routes';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Divider } from "@material-ui/core";

import { SnackbarProvider, useSnackbar } from 'notistack';
import Button from "@material-ui/core/Button";

import { TrainRounded } from "@material-ui/icons";
import EditIcon from '@material-ui/icons/Edit';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';

import TextField from '@material-ui/core/TextField';
import { Card, Icon, Image } from 'semantic-ui-react';
import GetAllStudents from './getAllStudents';
import UnAcceptedStudent from "./unAcceptedStudents";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      item="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,
  };
}

function LinkTab(props) {
  return (
    <Tab
      component="a"
      onClick={(event) => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

function CheooseEventOfStudent(props) {

  const [pageInfo, setPageInfo] = useState({
    loading: false,
    jsonBodyAdd: {
    },
    jsonBodyGet: {
    },
  });

  useEffect(() => {
    console.log('useEffect')
  }, []);


  const classes = useStyles();
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      {
        pageInfo.loading === true ?
          <Backdrop
            open>
            <CircularProgress color="inherit" />
          </Backdrop> :

          <div className={classes.root}>
            <SideBar />
            <AppBar position="static">
              <Tabs
                variant="fullWidth"
                value={value}
                onChange={handleChange}
                aria-label="nav tabs example"
              >
                <LinkTab className="colors"  label="استعراض الطلاب المعلقين" href="/drafts" {...a11yProps(0)} />
                <LinkTab className="colors" label="استعراض جميع الطلاب" href="/trash" {...a11yProps(1)} />
              </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
              <UnAcceptedStudent />
            </TabPanel>

            <TabPanel value={value} index={1}>
              <div>
                {pageInfo.loading === true ?
                  <Backdrop
                    open>
                    <CircularProgress color="inherit" />
                  </Backdrop> :
                  <GetAllStudents />
                }</div>
            </TabPanel>
          </div>
      }
    </div>
  );
}
// class CheooseEventOfStudent extends Component {


//   render() {
//     return (
//       <div >
//         <SideBar />
//         <div className="rows ">
//           <div className="col-sm-6">
//             <div className="card">
//               <div className="card-body">
//                 <h5 className="card-title">   الطلاب المعلقين    </h5>
//                 <center>
//                   <a class="btn btn-primary" href="/unAcceptedStudents" role="button"> عرض </a>
//                 </center>
//               </div>
//             </div>
//           </div>
//           <div className="col-sm-6">
//             <div className="card">
//               <div className="card-body">
//                 <h5 className="card-title">   جميع الطلاب  </h5>
//                 <center>
//                   <a class="btn btn-primary" href="/allStudents" role="button"> دخول </a>
//                 </center>
//               </div>
//             </div>
//           </div>
//         </div>
//       </div>
//     )
//   }
// }
export default CheooseEventOfStudent;