import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import SideBar from '../../SideBar/SideBar';
import * as myConstClass from '../../../constants';
class getAllStudents extends Component {

    constructor(props) {
        super(props);
        this.state = {
            students: {},
        }
      }
      componentDidMount() {
        this.refreshList();
      }
    
      async refreshList() {
        const config = {
          headers : {
            Authorization :  'Bearer ' + localStorage.getItem('token')
          }
        };
        const res = await axios.get(myConstClass.getAllStudentOfEdication , config);
        console.log(res.data["data"]);
        if(res.status = 200){
          console.log("good");
          this.setState({
            students: res.data["data"],
          })
        }else{
          console.log("no");
        }
        }
     
      render() {
        return (
            
          <div>
           
            <div>
              <center>
                  
                <table className="show-table" size="sm" >
                  <thead  >
                    <tr>
                      <th>الرقم التسلسلي </th>
                      <th> الاسم الثلاثي   </th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    { this.state.students  != "لا يوجد طلاب" ?
                    (this.state.students.length > -1) ? this.state.students.map((student, index) => {
                      return (
                        <tr key={index}>
                          <td> {index + 1}</td>
    
                          <td> {student.firstName} {student.fatherName} {student.lastName}</td>
                          <td><Link  to = {'/getInfoOfStudent/' + student.id} > عرض </Link>
                          </td>
                        </tr>
                      )
                    }) : <tr><td colSpan="5">جاري التحميل...</td></tr> : <center> لا يوجد طلاب</center>}
                  </tbody>
                </table>
              </center>
            </div>
          </div>
        );
      }
}

export default getAllStudents;