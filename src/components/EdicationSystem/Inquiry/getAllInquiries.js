import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import SideBar from '../../SideBar/SideBar';
import { Button } from 'react-bootstrap';
import * as myConstClass from '../../../constants';
class getAllInquiries extends Component {

  constructor(props) {
    super(props);
    this.state = {
      inquiries: {},
    }
  }
  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const config = {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token')
      }
    };
    const res = await axios.get(myConstClass.getInquiriesOfEducation, config);
    console.log("res is 1" + res.data["data"]);
    console.log("res is 2" + res.body);
    if (res.status = 200) {
      console.log("good");
      this.setState({
        inquiries: res.data["data"],
      })
      console.log(res.data["data"]);
    } else {
      console.log("no");
    }
  }

  render() {
    return (
      <div>
        <SideBar />
        <div>
          <center>
            <br/>
            <Button  className ="add_button_ed" onClick={() => window.location.href = "/addInquary"} > إضافة استفسار </Button>
            <table className="show-table" size="sm" >
              <thead  >
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th>محتوى الاستفسار   </th>
                  <th>تاريخ الإنشاء</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {
                  Array.isArray(this.state.inquiries) ?
                    (this.state.inquiries.length > 0) ? this.state.inquiries.map((inquery, index) => {
                      return (
                        <tr key={index}>
                          <td> {index + 1}</td>

                          <td> {inquery.content}</td>
                          <td>{inquery.created_at}</td>
                          <td><Link to={'/getFullInfoOfinquary/' + inquery.id} > عرض </Link>
                          </td>
                        </tr>
                      )
                    }) : <tr><td colSpan="5">جاري التحميل...</td></tr>
                    : <tr><td colSpan="5">جاري التحميل...</td></tr>
                }
              </tbody>
            </table>
          </center>
        </div>
      </div>
    );
  }
}

export default getAllInquiries;