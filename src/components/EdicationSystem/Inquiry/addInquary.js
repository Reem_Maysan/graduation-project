import React, { Component } from 'react';
import {Formik , Field, ErrorMessage, FieldArray} from "formik";
import SideBar from '../../SideBar/SideBar';
import * as myConstClass from '../../../constants';
import axios from 'axios';
class addInquary extends Component {
  
    constructor(props) {
        super(props);
        this.state = {
            Content:"",
            choices:[],
            
        }
    }
  onSubmit = (values) => {
    console.log(values);
    this.setState({
        Content : values.Content ,
        choices : values.choices
    })
    let data = new FormData();
    data.append('Content', this.state.Content);
    data.append('choices', this.state.choices);
    for (var value of data.values()) {
      console.log("data is " + value);
   }
    axios({
        method: 'post',
        headers: {   Authorization :  'Bearer ' + localStorage.getItem('token') },
        url: myConstClass.postAddInquary ,
        data: values
      }).then(
         res =>{
             console.log(res);
             if(res.status == 200){
              alert("تمت عملية الإضافة بنجاح!");
              window.location.href = '/content';
              this.refreshList();
             console.log("good");
             }
         }
     ).catch(
         error =>{
             console.log(error)
         }) 
     


  }

  form = (props) => {
    return <form onSubmit={props.handleSubmit}>
        <div className="rec-contain-s">
        <div className="form-row r">
      <label  className="label" >  المحتوى : </label>
      <Field className="input" name="Content" />
      </div>
      <br />
      <div className="form-row r">
      <label  className="label" >  الخيارات : </label>
      <FieldArray 
        name="choices" className="input"
        render={ arrayHelper => (
          <div className="form-inline">
            {props.values.choices.map((item, index)=>(
              <div key={index}>
                <Field className="input" name={`choices.${index}`} />
                <button className="add_inquary_button" onClick={()=>arrayHelper.remove(index)}> - </button>
                  
              </div>
            ))}
            <button  className="add_inquary_button" onClick={()=>arrayHelper.push('')}> + </button>
          </div>
        )}
      />
      </div>
      <center><button className="t-button" onClick = {this.submitHandler}  type="submit" > حفظ </button></center> 
     
      </div>
    </form>
  }

  render() {
    return (
        <div>
        <SideBar />
        <div>
        <center>
        <Formik 
          initialValues={{
            Content: "",
            choices: []
          }}
          onSubmit={this.onSubmit}
          render={this.form}
          />
          </center>
      </div>
      </div>
    );
  }
}

export default addInquary;
