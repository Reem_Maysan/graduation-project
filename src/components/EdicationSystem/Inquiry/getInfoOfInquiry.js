import react, { Component } from 'react';
import SideBar from '../../SideBar/SideBar';
import "../EdicationSystem.css";
import * as myConstClass from '../../../constants';
import axios from 'axios';
import { render } from '@testing-library/react';
import { Button} from 'react-bootstrap';

class getInfoOfInquary extends Component{

    constructor(props) {
        super(props);
        this.state = {
            id : this.props.match.params.id ,
            inquary : {} ,
            enquiry:{} ,
            answerStatistic:[],
            choice:[]
            
        }
      
       
      }
       
      componentDidMount() {
        this.refreshList();
      }
    
      async refreshList() {
        const config = {
          headers : {
            Authorization :  'Bearer ' + localStorage.getItem('token')
          }
        };
        const res = await axios.get(myConstClass.getInquaryInfoOfEducation + this.state.id , config)
        console.log("res is 2" + res.body);
        if(res.status = 200){
          console.log("good");
          console.log(res.data["data"]);
          console.log(res.data["data"].enquiry );
          console.log( res.data["data"].answerStatistic );
          console.log( res.data["data"].enquiry.choice);
          this.setState({
            inquary: res.data["data"],
            enquiry : res.data["data"].enquiry,
            answerStatistic :  res.data["data"].answerStatistic ,
            choice : res.data["data"].enquiry.choice
          })
        }else{
          console.log("no");
        }
        
       
      }

        render(){
        return(
           
            <div >
            <SideBar />
            <div className="rec-contain-s">
            <h6>معلومات المناسبة  </h6>
            <div className="form-row r">
            <label className="label" >محتوى الاستفسار </label>
             <input className="input" name="name" type="text" value={this.state.enquiry.content} readOnly/>
              </div>
              <label className="label" >خيارات الاستفسار </label>
              <table className="show-table" size="sm" >
              <thead  >
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th>خيارات الاستفسار   </th>
                </tr>
              </thead>
              <tbody>
                {(this.state.choice.length > -1) ? this.state.choice.map((choice, index) => {
                  return (
                    <tr key={index}>
                      <td> {index + 1}</td>
                      <td> {choice.choice}</td>
                    </tr>
                  )
                }) : <tr><td colSpan="5">Loading...</td></tr>}
              </tbody>
            </table>
             
            
              
              </div>                
            </div>
        )
    }
    
}

export default getInfoOfInquary;