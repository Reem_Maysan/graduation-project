import React, { Component } from 'react';
import axios from 'axios';
import { IconButton } from '@material-ui/core';
import { Table, Button } from 'react-bootstrap';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import './PersonalInformation.css';
class PartnerListNe extends Component {
  constructor(props) {
    super(props);
    this.state = {
    patrners: [] ,
    id : '' ,
    name : '' ,
   
    }
  }
  
  
changeHandler = e =>{
  this.setState({
      [e.target.name ] : e.target.value
  })
}

 
  
 e

  componentDidMount() {
    this.refreshList();
  }
  async refreshList(){
    const res = await axios.get('')
    console.log(res.data)
    this.setState({
        loading:false, 
        patrners: res.data ,
        name : ''
    })
  }

    render() {
       /*{users.id}*/
           const {patrners , id , name} = this.state;
                return (
                  <div>
                    <center>
             
                       <table className="table-t table-bordered">
                      
                  <thead>
                <tr>
                <th scope="col">الرقم</th>
                <th scope="col"> اسم الشريك</th>
            
                </tr>
               
                </thead>
                <tbody>
                      { (patrners.length > 0) ? patrners.map( (partner, index) => {
                         return (
                            
                          <tr>
                            <td> {partner.id} </td>
                            <td> {partner.name}</td> 
                          </tr>
                          
                        )
                       }) : <tr><td colSpan="5">الرجاء الانتظار...</td></tr> }
                    </tbody>
                  </table>
                  <center><a href="/addPartnerNe" role="button" className="btn btn-n"> إضافة شريك </a></center>
              
                  </center>
                  <a href="/childrenNe" className="next-c" type="submit">التالي  &raquo; </a>
                  </div>
                  
             

                );
               

        
    }
}
export default PartnerListNe;
