import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import './PersonalInformation.css';
import DatePicker from 'react-date-picker';
import HealthyInformation from './HealthyInformation';
import  { useState } from "react";

function PartnerInformationNe() {
    const [healthyInfo, setHealthyInfo] = useState([{ healthProblem : "" , physician : "" ,
    therapy : "" , monthlyCost : "" , medicineCost:"" , notes:""} ]);
   const [IDN, setIDN] = useState('');
    
   // handle input change
   const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...healthyInfo];
    list[index][name] = value;
    setHealthyInfo(list);
    console.log(healthyInfo);
  };
 
  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...healthyInfo];
    list.splice(index, 1);
    setHealthyInfo(list);
  };
 
  // handle click event of the Add button
  const handleAddClick = () => {
    setHealthyInfo([...healthyInfo , { healthProblem : "" , physician : "" ,
    therapy : "" , monthlyCost : "" , medicineCost:"" , notes:""}]);
  };
 
   
       return(
        
           <div className="contain">
            
              <h6 className="h6-h2"> إضافة المعلومات الشخصية للشريك </h6>
              
             <form>
                <div className = "column border-left">
                <div >
             <div className="form-row r">
             <label className="label" > الاسم الأول  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > اسم الأب  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >  الكنية    :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >  اسم الأم  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >مكان الولادة  :</label>
             <input className="input" />
              </div>
           

              <div className="form-row r">
             <label className="label" > الجنسية  :</label>
             <input className="input" />
              </div>
             
              <div className="form-row r">
             <label className="label" > رقم الهوية  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > مكان الهوية  :</label>
             <input className="input" />
              </div>
              
              <div className="form-row r">
             <label className="label" > تاريخ الميلاد   :</label>
             <input className="input"  id="date" name="date" placeholder="MM/DD/YYY" type="text" />
             </div>
             <div className="form-row r">
             <label className="label" > العنوان   :</label>
             <input className="input" />
              </div>
              </div> 
              </div>
              
              {/*********col ****************************************/}
              <div className = "column border-left">
                <div >
           
             
              <div className="form-row r">
             <label className="label" >  التعليم  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label"  >  الشهادة  :</label>
            
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >    مصدر الشهادة :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >  العمل الحالي  :</label>
             <input className="input" />

              </div>
              <div className="form-row r">
             <label className="label" >  عنوان العمل الحالي  :</label>
             <input className="input" />
              </div>
              { healthyInfo.map((info, index) => {
   return (
    
     <center>
        <h5 > إضافة المعلومات الصحية </h5>
       <input   className="input-f"
         name="healthProblem"
         placeholder= " المشكلة الصحية "
         value={info.healthProblem}
         onChange={e => handleInputChange(e, index)}
       />
       <input
         className="input-f"
         name="physician"
         placeholder= "الطبيب المعالج"
         value={info.physician}
         onChange={e => handleInputChange(e, index)}
       />
           <input
         className="input-f"
         name="therapy"
         placeholder= "العلاج"
         value={info.therapy}
         onChange={e => handleInputChange(e, index)}
       />
        <input   className="input-f"
         name="monthlyCost"
         placeholder= "التكلفة الشهرية"
         value={info.monthlyCost}
         onChange={e => handleInputChange(e, index)}
       />
        <input   className="input-f"
         name="medicineCost"
         placeholder= "تكلفة الدواء"
         value={info.medicineCost}
         onChange={e => handleInputChange(e, index)}
       />
        <textarea
                name="notes"
                placeholder= "ملاحظات"
                value={info.notes}
                onChange={e => handleInputChange(e, index)}
             />
   
  
     </center>
   );
 })}
               <a href="" className="next" type="submit">التالي  &raquo; </a>
    
              </div>
              </div>
       </form>
       </div>
       )
 
  }
  export default PartnerInformationNe;