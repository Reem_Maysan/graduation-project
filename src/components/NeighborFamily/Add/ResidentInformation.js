import { Container } from 'react-bootstrap';
import './PersonalInformation.css';
import DatePicker from 'react-date-picker';
import React, { useState } from "react";


function ResidentInformationNE(){
   const [rebuilding, setRebuilding] = useState([{ title : "" , description : "" }]);
   
  // handle input change
  const handleInputChange = (e, index) => {
   const { name, value } = e.target;
   const list = [...rebuilding];
   list[index][name] = value;
   setRebuilding(list);
   console.log(rebuilding);
 };

 // handle click event of the Remove button
 const handleRemoveClick = index => {
   const list = [...rebuilding];
   list.splice(index, 1);
   setRebuilding(list);
 };

 // handle click event of the Add button
 const handleAddClick = () => {
   setRebuilding([...rebuilding , { title: "", description: "" }]);
 };

  
       return(
        
           <div className="contain">
            
              <h6 className="h6-h2"> إضافة معلومات السكن </h6>
              
             <form>
                <div className = "column border-left">
                <div >
                <div className="form-row r">
             <label className="label" > حالة العقار   :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "0" > ملك</option> 
              <option value= "1" > آجار</option>
              <option value= "2" > استثمار</option>
              </select>               
              </div>

             <div className="form-row r">
             <label className="label" > الآجار الشهري  :</label>
             <input className="input" type="number"/>
              </div>
              <div className="form-row r">
             <label className="label" > مكان المنزل :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >  عدد الغرف    :</label>
             <input className="input" type="number" />
              </div>
              <div className="form-row r">
             <label className="label" >  نوع المنزل  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >  الرطوبة   :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "0" > لايوجد </option> 
              <option value= "1" > خفيفة </option>
              <option value= "2" > متوسطة </option>
              <option value= "2" > عالية </option>
              </select>               
              </div>
              <div className="form-row r">
             <label className="label" >  التنجيد   :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "0" > ممتاز </option> 
              <option value= "1" > جيد </option>
              <option value= "2" > وسط </option>
              <option value= "2" > سيء </option>
              </select>               
              </div>


              <div className="form-row r">
             <label className="label" >  حالة الكسوة   :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "0" > ممتاز </option> 
              <option value= "1" > جيد </option>
              <option value= "2" > وسط </option>
              <option value= "2" > سيء </option>
              </select>               
              </div>

              
            
              </div> 
              </div>
              
              {/*********col ****************************************/}
              <div className = "column border-left">
                <div >
                <div className="form-row r">
             <label className="label" > رقم المنزل  :</label>
             <input className="input" type="number" />
              </div>
             
              <div className="form-row r">
             <label className="label" > مالك المنزل  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
              <label className="label" > ملاحظات     :</label>
              <textarea  className="input"  rows="7"></textarea>
              </div>

             
              <label className="label"  >  الترميم (إن وجد)  :</label>
              {rebuilding.map((rebuild, index) => {
                 
        return (
          <div className="box">
              <div className="form-row r">
            <input   className="input-f"
              name="title"
			    placeholder= "العنوان"
              value={rebuild.title}
              onChange={e => handleInputChange(e, index)}
            />
             <textarea
                name="description"
                placeholder= "الوصف"
                value={rebuild.description}
                onChange={e => handleInputChange(e, index)}
             />
        
              {rebuilding.length !== 1 && 
              <button className="btn btn-primary btn-rounded mr" 
              onClick={() => handleRemoveClick(index)}>حذف</button>}
              {rebuilding.length - 1 === index && 
              <button className="btn btn-primary btn-rounded mr"
               onClick={handleAddClick}>إضافة</button>}
         
          </div>
          </div>
        );
      })}
            </div>
              <a href="/healthNe" className="next" type="submit">التالي  &raquo; </a>
                     
              
              </div>
              
       </form>
       </div>
       )
 
  }
  export default ResidentInformationNE;