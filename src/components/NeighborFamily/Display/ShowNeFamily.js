import react, { Component } from 'react';
import axios from 'axios';
import { Table, Button } from 'react-bootstrap';
import '../../AssFamily/ShowAss.css';
import { LoopCircleLoading } from 'react-loadingg';
import SideBar from '../../SideBar/SideBar';
import { WindMillLoading } from 'react-loadingg';
class ShowNeFamily extends Component{

    constructor(props) {
        super(props);
        this.state = {
        families: [] 
        
       
        }
      }
      componentDidMount() {
        this.refreshList();
      }
 
      async refreshList(){
        const res = await axios.get('')
        console.log(res.data)
        console.log( res.firstName)
        this.setState({
            families: res.data["data"] ,
         
        })
      }
    
        render() {
       
           /*{users.id}*/
               const {families } = this.state;
                    return (
                      <div>
                           <SideBar />
                     <div>
                     <center>
                        
                      <table className="show-table" size ="sm" >
                        <thead  >
                          <tr>
                            <th>الرقم التسلسلي :</th>
                            <th>الاسم الثلاثي :</th>
                          </tr>
                        </thead>
                        <tbody>
                        { (families.length > 0) ? families.map( (person, index) => {
                             return (
                              <tr>
                                <td> {index+1}</td>
                              <td><a href={"/nameNe/" + person.id }>{person.firstName + ' '}{person.fatherName  + ' ' }{person.lastName  + ' '} </a></td>
                              {/*  <td> {person.name}{person.username}{person.email} </td>*/}
                              </tr>
    
                                  )
                              }) : <tr><td colSpan="5">Loading...</td></tr> }
                              </tbody>
                      </table>
                      </center>
                      </div>
                      </div>
                    );
                }
}

export default ShowNeFamily;