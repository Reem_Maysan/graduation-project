import react, { Component } from 'react';
import axios from 'axios';
import { Table, Button } from 'react-bootstrap';
import ShowPerInfo from '../../test/ShowPerInfo';
import SideBar from '../../SideBar/SideBar';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';


class DisplayAllInfoNe extends Component{
   
    render(){
        return(
           
            <div>
                 <SideBar />
                 <div className="rec-contain">
                    <center> <h6>المعلومات الشخصية</h6></center>
            
                <div className = "column border-left">
                <div >
                <div className="form-row r">
                 <label className="label-dis" > الاسم الأول  :</label>
                  <input className="show-input" placeholder="محمد خير" disabled />
                 </div>


                 <div className="form-row r">
                 <label  className="label-dis" >  اسم الأب  :</label>
                  <input className="show-input"   disabled />
                 </div>

                 <div className="form-row r">
                 <label className="label-dis" > الكنية:</label>
                  <input  className="show-input" disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" > اسم الأم :</label>
                  <input  className="show-input"  disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" > كنية الأم  :</label>
                  <input  className="show-input"  disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" > مكان الولادة :</label>
                  <input className="show-input"   disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" >  الجنس :</label>
                  <input  className="show-input"  disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" >   رقم الهوية  :</label>
                  <input  className="show-input"  disabled />
                 </div>

                 <div className="form-row r">
                 <label className="label-dis" > الحالة الاجتماعية:</label>
                  <input className="show-input"   disabled />
                 </div>


                 </div>
                 </div>
                 <div className = "column border-left">
                <div >
                <div className="form-row r">
                 <label className="label-dis" > الجنسية  :</label>
                  <input className="show-input"   disabled />
                 </div>
                
                 <div className="form-row r">
                 <label className="label-dis" > تاريخ الميلاد :</label>
                  <input  className="show-input"  disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" > الالتزام الأخلاقي  :</label>
                  <input  className="show-input"  disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" > العنوان  :</label>
                  <input className="show-input"   disabled />
                 </div>
                 <div className="form-row r">
                 <label  className="label-dis" >  تاريخ العائلة :</label>
                  <input  className="show-input"  disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" > العمل الحالي  :</label>
                  <input className="show-input"   disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" > عنوان العمل الحالي  :</label>
                  <input className="show-input"   disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" >  تاريخ العمل الحالي :</label>
                  <input className="show-input"   disabled />
                 </div>
                 </div>
                 </div>
                </div>
              <br />
              <br />
              <div className="rec-contain">
                    <center> <h6>معلومات السكن</h6></center>
            
                <div className = "column border-left">
                <div >
                <div className="form-row r">
                 <label className="label-dis" >  حالة العقار    :</label>
                  <input className="show-input"  disabled />
                 </div>


                 <div className="form-row r">
                 <label  className="label-dis" >  الآجار الشهري  :</label>
                  <input className="show-input"   disabled />
                 </div>

                 <div className="form-row r">
                 <label className="label-dis" >  مكان المنزل  :</label>
                  <input  className="show-input" disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" >  عدد الغرف :</label>
                  <input  className="show-input"  disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" >  نوع المنزل  :</label>
                  <input  className="show-input"  disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" >الرطوبة  :</label>
                  <input className="show-input"   disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" >  التنجيد :</label>
                  <input  className="show-input"  disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" >     حالة الكسوة     :</label>
                  <input  className="show-input"  disabled />
                 </div>

                


                 </div>
                 </div>
                 <div className = "column border-left">
                <div >
                <div className="form-row r">
                 <label className="label-dis" >  رقم المنزل  :</label>
                  <input className="show-input"   disabled />
                 </div>
                
                 <div className="form-row r">
                 <label className="label-dis" >  مالك المنزل    :</label>
                  <input  className="show-input"  disabled />
                 </div>
                 <div className="form-row r">
                 <label className="label-dis" > ملاحظات   :</label>
                  <textarea  className="show-input"  disabled={true} />
                 </div>

                 
                 </div>
                 </div>
                </div>

                <br />
                <br />
                <div className="rec-contain"> 
                               
                           <center> <h6>معلومات الصحية</h6>
                      <table  size ="sm" >
                        <thead  >
                          <tr>
                          <th>   # </th>
                            <th>  المشكلة الصحية </th>
                            <th> الطبيب المعالج </th>
                            <th> العلاج </th>
                            <th>  التكلفة الشهرية</th>
                            <th>تكلفة الدواء   </th>
                            <th> ملاحظات </th>
                          </tr>
                        </thead>
                        <tbody>
             
                              </tbody>
                      </table>
                      </center>
                      </div>
                      <br />
                      <br />
                      <div className="rec-contain">
                                
                           <center> <h6>معلومات الراتب الخارجي </h6>
                      <table  size ="sm" >
                        <thead  >
                          <tr>
                            <th>   # </th>
                            <th>  القيمة </th>
                            <th> المصدر </th>
                      
                          </tr>
                        </thead>
                        <tbody>
             
                              </tbody>
                      </table>
                      </center>
                      </div>
                      <br/>
                      <br/>
                      <div className="rec-contain">
                                
                           <center> <h6>معلومات  الدين  </h6>
                      <table  size ="sm" >
                        <thead  >
                          <tr>
                            <th>   # </th>
                            <th>  القيمة </th>
                            <th> المصدر </th>
                      
                          </tr>
                        </thead>
                        <tbody>
             
                              </tbody>
                      </table>
                      </center>
                      </div>

                      <br />
                      <br/>
                      <div  className="rec-contain">
                      <center>
                           <h6>معلومات  الشريك  </h6>
                    
             
                       <table className="table-t table-bordered">
                      
                  <thead>
                <tr>
                <th scope="col">الرقم</th>
                <th scope="col"> اسم الشريك</th>
            
                </tr>
               
                </thead>
                <tbody>
                    
                    </tbody>
                  </table>
                
              
                  </center>
               
                  </div>

                  <br/>
                  <br/>
                  <div  className="rec-contain">
                          
                    <center>
                    <h6>معلومات  الأطفال   </h6>
                       <table className="table-t table-bordered">
                      
                  <thead>
                <tr>
                <th scope="col">الرقم</th>
                <th scope="col"> اسم الطفل</th>
            
                </tr>
               
                </thead>
                <tbody>
                      
                    </tbody>
                  </table>
               
                  </center>
                  
                  </div>
                  <br/>
                  <br/>
                
                
                 
              
    
                  
                  
            </div>
        )
    }
}

export default DisplayAllInfoNe;