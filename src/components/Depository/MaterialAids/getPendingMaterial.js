import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Table, Button } from 'react-bootstrap';
import { LoopCircleLoading } from 'react-loadingg';
import SideBar from '../../SideBar/SideBar';
import { WindMillLoading } from 'react-loadingg';
import * as myConstClass from '../../../constants';
import '../Depository.css'

class GetPendingMaterila extends Component {

  constructor(props) {
    super(props);
    this.state = {
      material_aids: [] ,
      form_pending : {},
      form_id: "",
      materialAidID:""
    }
  }
  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.GET_PendingMaterialAid)
    console.log(res.data)
    this.setState({
        material_aids: res.data["data"],
        form_pending: res.data["data"].form,

    })
  }
  async accept(material_id , form_id) {
    this.setState({ 
        materialAidID : material_id ,
        form_id : form_id
    });
    console.log(material_id);
    console.log(form_id);
    }


    submitHandler = e =>{
        e.preventDefault();
        let data = new FormData();
        data.append('form_id', this.form_id);
        data.append('materialAidID', this.state.materialAidID);
        for (var value of data.values()) {
          console.log("the data is " + value);
       }
        console.log(data);
         axios({
          method: 'post',
          url:  myConstClass.POST_AcceptMaterialAid,
          data: data
        }).then(res =>{
          console.log(res.data);
          if(res = 200){
          console.log("the request is ok");
          alert("تم قبول المساعدة بنجاح")
          }else{
            console.log("erroer");
          }
      }
      ).catch(
      )
        
      
         }
         
  render() {
    const { material_aids, form_pending } = this.state;
    return (
      <div>
        <SideBar />
        <div className="background-logo">
          <center>
         { material_aids != "لا يوجد مساعدات عينية معلقة" ?
            <table className="show-table" size="sm" >
              <thead  >
                <tr>
                  <th>الرقم  </th>
                  <th>وصف المساعدة   </th>
                  <th>اسم المستفيد  </th>
                  <th>الاسم الثلاثي للأم   </th>
                  <th> مكان الميلاد   </th>
                  <th> تاريخ الميلاد   </th>
                  <th> الجنسية  </th>
                  <th>الحالة الاجتماعية     </th>
                  <th>  المستوى التعليمي   </th>
                  <th> الالتزام الأخلاقي  </th>
                  <th>  الجنس  </th>
                  <th>العنوان      </th>
                  <th> تاريخ العائلة  </th>
                  <th>العمل الحالي      </th>
                  <th> عنوان العمل الحالي   </th>
                  <th>رقم هاتف العمل الحالي      </th>
                  <th>         </th>
                </tr>
              </thead>
              <tbody>
                {(material_aids.length > -1 ) ? material_aids.map((material_aid, index) => {
                  return (
                    <tr key={index}>
                         <td> {index + 1}</td>
                      <td> {material_aid.aidDescription}</td>
                      <td> {material_aid.form.firstName} {material_aid.form.fatherName} {material_aid.form.lastName}</td>
                      <td> {material_aid.form.motherFirstName} {material_aid.form.motherLastName} </td>
                      <td> {material_aid.form.birthPlace} </td>
                      <td> {material_aid.form.birthDate} </td>
                      <td> {material_aid.form.nationality} </td>
                      <td> {material_aid.form.maritalStatus} </td>
                      <td> {material_aid.form.education} </td>
                      <td> {material_aid.form.ethicalCommitment} </td>
                      <td> {material_aid.form.gender} </td>
                      <td> {material_aid.form.address} </td>
                      <td> {material_aid.form.familyHistory} </td>
                      <td> {material_aid.form.currentWork} </td>
                      <td> {material_aid.form.currentWorkAddress} </td>
                      <td> {material_aid.form.currentWorkPhone} </td>
                      <td>
                      
                      <Link path ="/acceptMAterialAid/" 
                        onClick = {()=>{this.accept(material_aid.id , material_aid.form.id)}}
                      >قبول</Link>
                      
                      </td>
                  
              
                    </tr>
                  )
                }) : <tr><td colSpan="5">Loading...</td></tr>}
              </tbody>
            </table>  : alert("لا يوجد مساعدات عينية معلقة") } 
          </center>
          
          
            
        </div>
      </div>
    );
  }
}

export default GetPendingMaterila;