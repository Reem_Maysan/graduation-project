import react, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Table, Button } from 'react-bootstrap';
import { LoopCircleLoading } from 'react-loadingg';
import SideBar from '../../SideBar/SideBar';
import { WindMillLoading } from 'react-loadingg';
import * as myConstClass from '../../../constants';
import '../Depository.css'

class AllStores extends Component {

  constructor(props) {
    super(props);
    this.state = {
      stores: []
    }
  }
  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.GET_ALL_Warehouses)
    console.log(res.data)
    this.setState({
      stores: res.data["data"],

    })
  }
  render() {

    /*{users.id}*/
    const { stores } = this.state;
    return (
      <div>
        <SideBar />
        <div className="background-logo">
        <a class="btn " href="/addStore" role="button" style={{backgroundColor:'gray' , color : 'white'}}>إضافة مستودع</a>
          <center>
         
            <table className="show-table" size="sm" >
              <thead  >
                <tr>
                  <th>الرقم  </th>
                  <th>اسم المستودع  </th>
                </tr>
              </thead>
              <tbody>
                {(stores.length > 0) ? stores.map((store, index) => {
                  return (
                    <tr key={index}>
                      <td> {store.id}</td>
                      <td>
                        <Link to={{
                          pathname: '/displayStoreDetails/' + store.id ,
                          state: {
                            id: store.id
                          }
                        }}>{store.name}</Link>
                      </td>
              
                    </tr>
                  )
                }) : <tr><td colSpan="5">Loading...</td></tr>}
              </tbody>
            </table>
          </center>
          
          
            
        </div>
      </div>
    );
  }
}

export default AllStores;