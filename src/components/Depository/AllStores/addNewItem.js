import react, { Component } from 'react';
import SideBar from '../../SideBar/SideBar';
import EditIcon from '@material-ui/icons/Edit';
import "../Depository.css";
import * as myConstClass from '../../../constants';
import axios from "axios";


class addNewItem extends Component{
    constructor(props) {
        super(props);
        this.state = {
          stores: [],
          id: this.props.match.params.id ,
          name : ""  ,
          quantity: "",
          expireDate : "",
          notes: "",
          categoryName:"",
          warehouseID: ""   ,        
        }
        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
      }
       

    componentDidMount(){
                
        this.refreshList();
    }
    async refreshList(){
        const res = await axios.get(myConstClass.GET_ALL_Warehouses)
        console.log(res.data)
        this.setState({
          stores: res.data["data"],
    
        })
      
}
    changeHandler = e =>{
    this.setState({
        [e.target.name ] : e.target.value
    })
  }

  submitHandler = e =>{
    e.preventDefault();
    let data = new FormData();
    data.append('name', this.state.name);
    data.append('quantity', this.state.quantity);
    data.append('expireDate', this.state.expireDate);
    data.append('notes', this.state.notes);
    data.append('categoryName', this.state.categoryName);
    data.append('warehouseID', this.state.warehouseID);
    for (var value of data.values()) {
      console.log( " data is" + value);
   }
    console.log(data);
     axios({
      method: 'post',
      url: myConstClass.POST_AddItemCategories,
      data: data
    }).then(res =>{
      if(res.status == 200){
        console.log("goooooooood");  
        window.location.href="/Store"
      }
  }
  ).catch(
  error =>{
      console.log(error)
  } 
  
    
      
  )
    
  
     }

    render(){
        return(
           
                <div >
                    <SideBar />
                    <div className="rec-contain-s">
                <h6>  إضافة مادة جديدة   </h6>
                <div className="form-row r">
                <label className="label"  > اسم المادة  :</label>
                <input className="input" type="text" name = "name" onChange = {this.changeHandler}/>
                </div>
                <div className="form-row r">
                <label className="label" >  الكمية  :</label>
                <input className="input" type="number" name="quantity" onChange = {this.changeHandler} />
                </div>
                <div className="form-row r">
                <label className="label" > تاريخ الإنتهاء   :</label>
                <input className="input" type="date" name="expireDate" onChange = {this.changeHandler} />
                </div>
                <div className="form-row r">
                <label className="label"  >  ملاحظات  :</label>
                <textarea className="input" type="text" name="notes" onChange = {this.changeHandler} />
                </div>
                <div className="form-row r">
                <label className="label" >  اسم الصنف  :</label>
                <textarea className="input" type="text" name="categoryName" onChange = {this.changeHandler} />
                </div>
                <div className="form-row r">
                <label className="label" >  المستودع  :</label>
                 <select  onChange = {(e) =>
                this.setState({ warehouseID : e.target.value }) } >
                <option hidden>اختار مستودع  </option>
                {this.state.stores.map(store => (
                   <option key={store.id}  value={store.id}>
                        {store.name}
                           </option>
                         
                       ))}
                         </select>
                </div>
                <center>
                
                <button className="t-button" onClick = {this.submitHandler}> حفظ </button>
                </center>
                </div>
    
                
            
                    
                </div>
        )
    }
}

export default addNewItem;