import axios from 'axios';
import react, { Component } from 'react';
import * as myConstClass from '../../../constants';
import SideBar from '../../SideBar/SideBar';

class DepositoryDetailes extends Component{
    
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id ,
      detailes : {},
      emp : {},
      items : []
     
      
  
      
    }
  }

  componentDidMount() {
    this.refreshList();
  }
  async refreshList() {
    const config = {
      headers : {
        Authorization :  'Bearer ' + localStorage.getItem('token')
      }
    };
    const res = await axios.get(myConstClass.GET_WareHousesDeatailes + 4 , config);
    if(res.status == 200){
    console.log(res)
    console.log("emp" + res.data)
    this.setState({
      detailes: res.data,
      emp: res.data.emp

    })
  }else{
    alert("المستودع غير موجود");
    window.location.href ="/Store";
  }
    const res2 = await axios.get("https://thawing-everglades-11191.herokuapp.com/api/v1/warehouseDep/warehouses/getWareHouseItems/" + this.state.id , config);
    if(res2.status == 200){
    console.log(res2.data)
    console.log("good response 2")
    this.setState({
      items: res2.data["data"],
    })
     }
    
  }
  render(){
    const {detailes , emp , items} = this.state;
  const handleClickAdd = () => window.location.href = '/addNewItem' ;
  const handleClickTrans = () => window.location.href = '/trasItem/' + localStorage.getItem('departmentID');
  const handleClickView = () => window.location.href ='/viewItem/'+ localStorage.getItem('departmentID');
  return (
    <div >
         <SideBar />
         <div className="container_rectangle">
      <h6 className="h6-h2"> عرض تفاصيل المستودع    </h6>
      <button className="but-depository" onClick={handleClickView}> عرض الأصناف </button>
      <form>
     
        <div className="column ">
          <div >
            <div className="form-row r">
              <label className="label" > اسم المستودع  :</label>
              <input className="input" value={detailes.name} readOnly/>
            </div>
            <div className="form-row r">
              <label className="label" >     المسؤول  :</label>
              <input className="input" value={emp.firstName}/>
            </div>
          
            
            <div className="column ">
          <div >
         
        </div>
</div>
          </div>
        </div>
        <div className="column ">
          <div >
          <div className="form-row r">
              <label className="label" > تاريخ الإنشاء  :</label>
              <input className="input"  value={detailes.created_at} readOnly/>
            </div>
            <div className="form-row r">
              <label className="label" >   الاسم الثلاثي  :</label>
              <input className="input" value={emp.firstName + " " + emp.fatherName + " " + emp.lastName } readOnly/>
            </div>
          </div>
        </div>
        
      </form>
    
      <div className="row">
      
        <button className="trans-item" onClick={handleClickTrans}> نقل مادة بين مستودعين  </button>
        <button className="add-item" onClick= {handleClickAdd} > إضافة مادة جديدة  </button>
      
      </div>
      <h6>  المواد الموجودة في المستودع     </h6>
      <center>
     
      <table className="show-tables" size="sm" >
              <thead  >
                <tr>
                  <th>رقم المادة</th>
                  <th>اسم االمادة  </th>
                  <th>الكمية   </th>
                  <th>تاريخ الإنشاء  </th>
                  <th>تاريخ الإنتهاء   </th>
                  <th>ملاحظات   </th>
                  <th></th>
          
                </tr>
              </thead>
              <tbody>
                
              {this.state.items != "لا يوجد مواد في هذا المستودع" ? this.state.items.map((item, index) => {
                  return (
                    <tr key={index}>
                     <td> {index  +1}</td> 
                     <td> {item.name}</td> 
                     <td> {item.quantity}</td> 
                     <td> {item.created_at}</td> 
                     <td> {item.expire_date}</td> 
                     <td> {item.notes}</td> 
                      <td>
                     
                      </td>
              
                    </tr>
                  )
                })  : "لا يوجد مواد في هذا المستودع"}
               
              </tbody>
            </table>
            </center>
    </div>
    </div>
  )
  }
}
export default DepositoryDetailes;