import axios from 'axios';
import { Component } from 'react';
import { Link } from 'react-router-dom';
import * as myConstClass from '../../../constants';
import SideBar from '../../SideBar/SideBar';
class viewItem extends Component{
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      items: []                 
    }  
    
  }
 
  1

componentDidMount(){
    
    this.refreshList();
}
async refreshList(){
   
    const res1 = await axios.get(myConstClass.GET_WareHouseCategories + 4);
    if(res1.status == 200){
    this.setState({
      items: res1.data["data"],
      
          })   
    }else{
  console.log("error")
}
}
  render(){
    const {items} = this.state;
  return (
    <div >
      <SideBar />
      <div>
      <h6 className="h6-h2"> عرض  الأصناف    </h6>
      <br/>
      <center>
      <table className="show-table" size="sm" >
              <thead>
                <tr>
                  <th> اسم الصنف  </th>
                </tr>
              </thead>
              <tbody>
                
              {(items.length > -1 ) ? items.map((item, index) => {
                  return (
                    
                    <tr >
                   <td>
                        <Link to={{
                          pathname: '/displayItemOfCategory/' + 4 ,
                        }}>{item.name}</Link>
                      </td>
                    </tr>
                      )
}) : <tr><td colSpan="5">جاري التحميل...</td></tr>}
              </tbody>
            </table>
            </center>
            </div>
    </div>
  )

}
}
export default viewItem;