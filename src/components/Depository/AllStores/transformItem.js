import react, { Component } from 'react';
import SideBar from '../../SideBar/SideBar';
import EditIcon from '@material-ui/icons/Edit';
import "../Depository.css";
import axios from "axios";
import * as myConstClass from '../../../constants';


class TransformItem extends Component {
        constructor(props) {
                super(props);
                this.state = {
                  stores: [],
                  id: this.props.match.params.id ,
                  oldWarehouse : "" ,
                  newWarehouse: "" ,
                  itmeID : "" ,
                  quantity : "",
                  items:[]                  
                }  
                this.changeHandler = this.changeHandler.bind(this);
                this.submitHandler = this.submitHandler.bind(this);
              }
               
            
      changeHandler = e =>{
        this.setState({
            [e.target.name ] : e.target.value
        })
      }
        
            componentDidMount(){
                
                this.refreshList();
            }
            async refreshList(){
                const res = await axios.get(myConstClass.GET_ALL_Warehouses)
                console.log(res.data)
                this.setState({
                  stores: res.data["data"],
            
                })
                const res1 = await axios.get(myConstClass.GET_WareHousesDeatailesItem + localStorage.getItem('departmentID') );
                console.log(res1.data)
                if(res1.status == 200){
                console.log(res1.body)
                this.setState({
                        items: res1.data["data"],
                  
                      })
                    console.log("my" + res1.data["data"] )
                    }else{
                        console.log("error")
                      }
        }

        async item(){
          const res1 = await axios.get(myConstClass.GET_WareHousesDeatailesItem + this.state.oldWarehouse );
          console.log(res1.data)
          if(res1.status == 200){
          console.log(res1.body)
          this.setState({
                  items: res1.data["data"],
            
                })
              console.log("my" + res1.data["data"] )
              }else{
                  console.log("error")
                }
        }
        
            
        submitHandler = e =>{
            e.preventDefault();
            let data = new FormData();
            data.append('oldWarehouse', this.state.oldWarehouse);
            data.append('newWarehouse', this.state.newWarehouse);
            data.append('itmeID', this.state.itmeID);
            data.append('quantity', this.state.quantity);
            for (var value of data.values()) {
              console.log( " data is" + value);
           }
            console.log(data);
             axios({
              method: 'post',
              url: "https://thawing-everglades-11191.herokuapp.com/api/v1/warehouseDep/warehouses/moveItemsBetweenWarehouses",
              data: data
            }).then(res =>{
              if(res.status == 200){
            alert("تم النقل بنجاح")
            window.location.href="/Store"
              }else{
                alert(res.body.data)
              }
          }
          ).catch(
          error =>{
              console.log(error)
           
          } 
          
            
              
          )
            
          
             }
   
 render(){
        return(
           
                <div className="role-cont" >
                    <SideBar />
                    <div className="rec-contain-s">
                <h6>  نقل مادة بين مستودعين</h6>
                <div className="form-row r">
                <label className="label" > من المستودع  :</label>
              <select  onChange = {(e) =>
              this.setState({ oldWarehouse : e.target.value }) } >
               <option hidden>اختار مستودع  </option>
               {this.state.stores.map(store => (
                   <option key={store.id}  value={store.id}>
                        {store.name}
                           </option>
                         
                       ))}
                         </select>
                </div>
                <div className="form-row r">
                <label className="label" >  المادة  :</label>
                <select  onChange={(e) => this.setState({ itmeID : e.target.value })} >
               <option hidden>اختار مادة  </option>
                   {this.state.items.map(item => (
                   <option key={item.id}  value={item.id}>
                        {item.name}
                           </option>
                       ))}
                    
                         </select>
                </div>
                <div className="form-row r">
                <label className="label" > إلى المستودع  :</label>
                <select  onChange={(e) => this.setState({ newWarehouse : e.target.value })} >
               <option hidden>اختار مستودع  </option>
               {this.state.stores.map(store => (
                   <option key={store.id}  value={store.id}>
                        {store.name}
                           </option>
                         
                       ))}
                         </select>
                </div>
                <div className="form-row r">
                <label className="label"  >  الكمية  :</label>
                <input className="input" name ="quantity" type="text" onChange ={this.changeHandler} />
                </div>
                
                <center>
                
                <button className="t-button" onClick = {this.submitHandler}> نقل </button>
                </center>
                
                </div>
            
                    
                </div>
        )
    
}
}
export default TransformItem;