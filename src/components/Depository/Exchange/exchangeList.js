import axios from 'axios';
import react, { Component } from 'react';
import { Link } from 'react-router-dom';
import SideBar from '../../SideBar/SideBar';
import { Button } from 'react-bootstrap';
import * as myConstClass from '../../../constants';
import Dialog from "@material-ui/core/Dialog";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { isEmptyArray } from 'formik';

class exchangeList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: 2,
      //this.props.match.params.id,
      exList: [],
      listID: ""
    }

    this.changeHandler = this.changeHandler.bind(this);
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
  }

  handleClickOpen() {
    this.setState({ open: true });
  }
  handleClickClose() {
    this.setState({
      open: false,

    });
  }

  changeHandler = e => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.GET_PendingExchangeLists  + localStorage.getItem('departmentID'));
    if (res.status == 200) {
      console.log(res.body);
      console.log(res.data["data"]);
      console.log("good");
      this.setState({
        exList: res.data["data"],
      })
    } else {
      console.log("error");
    }
  }

  async export(depo_id) {
    console.log("the id is " + depo_id);
    this.setState({
      listID: this.depo_id
    })
    let data = new FormData();
    data.append('listID', depo_id);
    for (var value of data.values()) {
      console.log("ListId is of post " + value);
    }
    axios({
      method: 'post',
      headers: { Authorization: 'Bearer ' + localStorage.getItem('token') },
      url: myConstClass.POST_WareHouseCategories,
      data: data
    }).then(res => {
      if (res.status == 200) {
        console.log("good");
        alert("تم التصريف بنجاح");
        this.refreshList();
      }
    }
    ).catch(
      console.log("error")
    )


  }


  render() {
    return (
      <div>
       
        <div>
          {/* <a class="btn " href={"/getArchivedList/" + this.state.id} role="button" >أرشيف قوائم الصرف </a> */}
          <h6 className="h6-h2"> عرض  قوائم الصرف المعلقة    </h6>
          <br />
          <center>
            <table className="show-table" size="sm" >
              <thead>
                <tr>
                  <th>الرقم</th>
                  <th>  العنوان  </th>
                  <th>  التوصيف  </th>
                  <th>  الأولوية  </th>
                  <th>  ملاحظات  </th>
                  <th> تاريخ الإنشاء  </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {
                  Array.isArray(this.state.exList) ?

                    (this.state.exList.length > -1) ? this.state.exList.map((exList, index) => {
                      return (
                        <tr key={index}>
                          <td> {index + 1}</td>
                          <td>{exList.title}</td>
                          <td>{exList.description}</td>
                          <td>{exList.priority}</td>
                          <td>{exList.notes}</td>
                          <td>{exList.created_at}</td>
                          <td><Link type="button"
                            onClick={() => { this.export(exList.id) }} > تصريف </Link></td>
                        </tr>
                      )
                    }) : <tr><td colSpan="5">جاري التحميل...</td></tr>
                    :
                    <tr><td colSpan="5">جاري التحميل...</td></tr>
                }
              </tbody>
            </table>
          </center>
        </div>
      </div>
    );
  }
}

export default exchangeList;