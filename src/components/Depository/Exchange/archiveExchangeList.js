import axios from 'axios';
import react, { Component } from 'react';
import { Link } from 'react-router-dom';
import SideBar from '../../SideBar/SideBar';
import { Button} from 'react-bootstrap';
import * as myConstClass from '../../../constants';
import Dialog from "@material-ui/core/Dialog";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";

class ArchiveExchangeList extends Component {

  constructor(props) {
    super(props);
    this.state = {
    //  id: this.props.match.params.id ,
      arExList: [],
      open : false,
        
    }
   
    this.changeHandler = this.changeHandler.bind(this);
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
  }
  
handleClickOpen() {
    this.setState({ open: true });
}
handleClickClose() {
    this.setState({
         open: false , 
         
        });
}

changeHandler = e =>{
    this.setState({
        [e.target.name ] : e.target.value
    })
}

  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.GET_ArchiveExchangeLists + localStorage.getItem('departmentID'));
    if(res.status = 200){
    console.log(res.body);
    console.log(res.data["data"]);
    console.log("good");
    this.setState({
        arExList: res.data["data"],
    })
  }else{
    console.log("error");
  }
}
/*
  async accept(stu_id , course_id) {
    this.setState({
       open: true ,
     
    });
    console.log(stu_id);
    console.log(course_id);
    }


    submitHandler = e =>{
      e.preventDefault();
      let data = new FormData();
      data.append('course_id', this.state.course_id);
      data.append('student_id', this.state.student_id);
      data.append('pValue', this.state.pValue);
      for (var value of data.values()) {
        console.log("the data is " + value);
     }
      console.log(data);
       axios({
        method: 'post',
        url:  myConstClass.addPaymentOfStudent,
        data: data
      }).then(res =>{
        console.log(res.data);
        if(res = 200){
        console.log("the request is ok");
        window.location.href = "/unAcceptedStudents";
        }else{
          console.log("erroer");
        }
    }
    ).catch(
    )
      
    
       }*/
  render() {
    return (
        
      <div>
        
        <div>
        
        <h6 className="h6-h2">   قوائم الصرف المؤرشفة    </h6>
      <br/>
      <center>
      <table className="show-table" size="sm" >
              <thead>
                <tr>
                  <th>  العنوان  </th>
                  <th>  التوصيف  </th>
                  <th>  الأولوية  </th>
                  <th>  ملاحظات  </th>
                  <th> تاريخ الإنشاء  </th>
                  <th> تاريخ الصرف  </th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {this.state.arExList !=  "لا يوجد قوائم صرف مؤرشفة في هذا المستودع" ?
                (this.state.arExList.length > -1) ? this.state.arExList.map((exList, index) => {
                  return (
                    <tr key={index}>
                      <td> {index + 1}</td>
                      <td>{exList.title}</td>
                      <td>{exList.description}</td>
                      <td>{exList.priority}</td>
                      <td>{exList.notes}</td>
                      <td>{exList.created_at}</td>
                      <td>{exList.exchange_date}</td>
                 
                    </tr>
                  )
                
                }) : <tr><td colSpan="6"> جاري التحميل...</td></tr>  : <center>لا يوجد قوائم صرف مؤرشفة في هذا المستودع </center>}
              </tbody>
            </table>
          
          </center>
          </div>
        </div>
    );
  }
}

export default ArchiveExchangeList;