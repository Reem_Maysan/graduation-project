import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Checkbox from "@material-ui/core/Checkbox";
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../constants';
import '../../Routes';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';

import SideBar from '../../SideBar/SideBar';

import ExchangeList from './exchangeList';
import archiveExchangeList from "./archiveExchangeList";
import ArchiveExchangeList from "./archiveExchangeList";

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            item="tabpanel"
            hidden={value !== index}
            id={`nav-tabpanel-${index}`}
            aria-labelledby={`nav-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    };
}

function LinkTab(props) {
    return (
        <Tab
            component="a"
            onClick={(event) => {
                event.preventDefault();
            }}
            {...props}
        />
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

function ExchangeTabs() {

    const [pageInfo, setPageInfo] = useState({
        loading: false,
        jsonBodyAdd: {
        },
        jsonBodyGet: {
        },
    });

    useEffect(() => {
        console.log('useEffect')
    }, []);


    const classes = useStyles();
    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div>
            <SideBar />
            {
                pageInfo.loading === true ?
                    <Backdrop
                        open>
                        <CircularProgress color="inherit" />
                    </Backdrop> :

                    <div className={classes.root}>

                        <AppBar position="static">
                            <Tabs
                                variant="fullWidth"
                                value={value}
                                onChange={handleChange}
                                aria-label="nav tabs example"
                            >
                                <LinkTab label="قوائم الصرف المؤرشفة" href="/drafts" {...a11yProps(0)} />
                                <LinkTab label="قوائم الصرف المعلقة" href="/trash" {...a11yProps(1)} />
                            </Tabs>
                        </AppBar>
                        <TabPanel value={value} index={0}>
                            <div>
                                <ArchiveExchangeList />
                            </div>
                        </TabPanel>

                        <TabPanel value={value} index={1}>
                            <div>
                                {pageInfo.loading === true ?
                                    <Backdrop
                                        open>
                                        <CircularProgress color="inherit" />
                                    </Backdrop> : 
                                      <ExchangeList /> 
                                }</div>
                        </TabPanel>
                    </div>
            }
        </div>
    );
}

export default ExchangeTabs;