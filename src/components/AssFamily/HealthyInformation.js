import React, { useState } from "react";
import { Container } from 'react-bootstrap';
import './PersonalInformation.css';
import DatePicker from 'react-date-picker';
import '../Routes';

function HealthyInformation() {

  const [healthyInfo, setHealthyInfo] = useState([{
    healthProblem: "",
    physician: "",
    therapy: "",
    monthlyCost: "",
    medicineCost: "",
    notes: "",
    IDN: "27793310"
  }]);
  /* 
    const [IDN, setIDN] = useState('');
   */

  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...healthyInfo];
    list[index][name] = value;
    setHealthyInfo(list);
    console.log(healthyInfo);
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...healthyInfo];
    list.splice(index, 1);
    setHealthyInfo(list);
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    setHealthyInfo([...healthyInfo, {
      healthProblem: "", physician: "",
      therapy: "", monthlyCost: "", medicineCost: "", notes: ""
    }]);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('\\\\\\\\\\\\handleSubmit//////////')
    console.log(healthyInfo)
    fetch('https://thawing-everglades-11191.herokuapp.com/api/v1/addForm/healthyInfo'
      , {
        method: 'POST',
        body: JSON.stringify(
          /*  {
        healthProblem:"ضيق تنقس",
        physician:"سمير سنديان",
        therapy:"وقف التدخين",
        monthlyCost:10000,
        medicineCost:3400,
        notes:"لا يوجد",
        IDN:"000027793310"
           } */
          healthyInfo
        ),
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json'
        },
      })
      .then((response) => response.json())
      .then((json) => console.log(json));

  }


  return (
    <div className="contain">
      <center>
        <h6 className="h6-h2"> إضافة المعلومات الصحية </h6>
        <form onSubmit={e => { handleSubmit(e) }}>
          <div className="p-t" >
            {healthyInfo.map((info, index) => {
              return (
                <div >
                  <input className="input-f"
                    name="healthProblem"
                    placeholder=" المشكلة الصحية "
                    value={info.healthProblem}
                    onChange={e => handleInputChange(e, index)}
                  />
                  <input
                    className="input-f"
                    name="physician"
                    placeholder="الطبيب المعالج"
                    value={info.physician}
                    onChange={e => handleInputChange(e, index)}
                  />
                  <input
                    className="input-f"
                    name="therapy"
                    placeholder="العلاج"
                    value={info.therapy}
                    onChange={e => handleInputChange(e, index)}
                  />
                  <input className="input-f"
                    name="monthlyCost"
                    placeholder="التكلفة الشهرية"
                    value={info.monthlyCost}
                    onChange={e => handleInputChange(e, index)}
                  />
                  <input className="input-f"
                    name="medicineCost"
                    placeholder="تكلفة الدواء"
                    value={info.medicineCost}
                    onChange={e => handleInputChange(e, index)}
                  />

                  <textarea
                    name="notes"
                    placeholder="ملاحظات"
                    value={info.notes}
                    onChange={e => handleInputChange(e, index)}
                  />

                  {healthyInfo.length !== 1 &&
                    <button className="btn btn-primary btn-rounded mr"
                      onClick={() => handleRemoveClick(index)}>حذف</button>}
                  {healthyInfo.length - 1 === index &&
                    <button className="btn btn-primary btn-rounded mr"
                      onClick={handleAddClick}>إضافة</button>}


                </div>

              );
            })}
            <br />
            {/* <button>submit</button> */}
          </div>
        </form>

      </center>
      <br />
      <br />
      <a href="/sal" className="next" type="submit">التالي  &raquo; </a>
      <br />
      <br />
      <a href="/sal" className="next" type="submit">تخطي  &raquo; </a>


    </div>
  );


}
export default HealthyInformation;