import React, { useState } from "react";
import './PersonalInformation.css';
import '../Routes';

function OutSalaryInforamation() {
  const [outSalary, setOutSalary] = useState([{
    source: "",
    deptSalary: "",
    IDN: "27793310"
  }]);
  /* 
   const [IDN, setIDN] = useState(''); */


  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...outSalary];
    list[index][name] = value;
    setOutSalary(list);
    console.log(outSalary);
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...outSalary];
    list.splice(index, 1);
    setOutSalary(list);
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    setOutSalary([...outSalary, { source: "", deptSalary: "" }]);
  };


  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('\\\\\\\\\\\\handleSubmit//////////')
    console.log(outSalary)
    fetch('https://thawing-everglades-11191.herokuapp.com/api/v1/addForm/monthlySalary'
      , {
        method: 'POST',
        body: JSON.stringify(
          /*  {
         source :["س1","س2","س3"],
         salaryValue :[ 180000,1800,4000 ],
         IDN :"000027793310"
           } */
          outSalary
        ),
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json'
        },
      })
      .then((response) => response.json())
      .then((json) => console.log(json));

  }


  return (
    <div className="contain">
      <center>
        <form onSubmit={e => { handleSubmit(e) }}>
          <h6 className="h6-h2"> إضافة راتب خارجي  </h6>
          <div className="p-t" >
            {outSalary.map((salary, index) => {
              return (
                <div className="box">
                  <input className="input-f"
                    name="source"
                    placeholder="المصدر"
                    value={salary.source}
                    onChange={e => handleInputChange(e, index)}
                  />
                  <input
                    className="input-f"
                    name="deptSalary"
                    placeholder="القيمة"
                    value={salary.deptSalary}
                    onChange={e => handleInputChange(e, index)}
                  />

                  {outSalary.length !== 1 &&
                    <button className="btn btn-primary btn-rounded mr"
                      onClick={() => handleRemoveClick(index)}>حذف</button>}
                  {outSalary.length - 1 === index &&
                    <button className="btn btn-primary btn-rounded mr"
                      onClick={handleAddClick}>إضافة</button>}

                </div>
              );
            })}
          </div>
          {/*  <button>submit</button> */}
        </form>
      </center>
      <br />
      <br />
      <a href="/dept" className="next" type="submit">التالي  &raquo; </a>
      <br />
      <br />
      <a href="/dept" className="next" type="submit">تخطي  &raquo; </a>

    </div>









  );
}

export default OutSalaryInforamation;