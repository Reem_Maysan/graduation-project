import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import './PersonalInformation.css';
import DatePicker from 'react-date-picker';
import HealthyInformation from './HealthyInformation';
import  { useState } from "react";

function ChildrenInformation() {
    const [healthyInfo, setHealthyInfo] = useState([{ healthProblem : "" , physician : "" ,
    therapy : "" , monthlyCost : "" , medicineCost:"" , notes:""} ]);
   const [IDN, setIDN] = useState('');
    
   // handle input change
   const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...healthyInfo];
    list[index][name] = value;
    setHealthyInfo(list);
    console.log(healthyInfo);
  };
 
  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...healthyInfo];
    list.splice(index, 1);
    setHealthyInfo(list);
  };
 
  // handle click event of the Add button
  const handleAddClick = () => {
    setHealthyInfo([...healthyInfo , { healthProblem : "" , physician : "" ,
    therapy : "" , monthlyCost : "" , medicineCost:"" , notes:""}]);
  };
 
   
       return(
        
           <div className="contain">
            
              <h6 className="h6-h2"> إضافة المعلومات الشخصية للطفل </h6>
              
             <form>
                <div className = "column border-left">
                <div >
             <div className="form-row r">
             <label className="label" > الاسم   :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
              <label className="col-form-label col-sm-2 pt-0 label"> الجنس :</label>
              <div >
             <div className="form-check">
            <input  type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
             <label className="form-check-label" for="flexRadioDefault1"> ذكر </label> 
            </div>
            <div className="form-check">
            <input  type="radio" name="flexRadioDefault" id="flexRadioDefault2"  />
             <label className="form-check-label" for="flexRadioDefault2">
             أنثى
              </label>
              </div>
              </div>
              </div>
              <div className="form-row r">
             <label className="label" >  مكان الولادة    :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > تاريخ الميلاد   :</label>
             <input className="input"  id="date" name="date" placeholder="MM/DD/YYY" type="text" />
             </div>
              <div className="form-row r">
             <label className="label" >  اسم الأم  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > العنوان  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > الحالة الاجتماعية  :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "0" > أعزب</option> 
              <option value= "1" > متزوج</option>
              <option value= "2" > خاطب</option>
              <option value= "3" > أرمل</option>
              <option value= "4" > مطلق</option>
              </select>      
                          
              </div>

              <div className="form-row r">
             <label className="label" > الالتزام الأخلاقي    :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "" > ملتزم </option> 
              <option value= "1" > غير ملتزم</option>
              <option value= "2" > متوسط</option>

              </select>
              </div>
             
             
              
          
              </div> 
              </div>
              
              {/*********col ****************************************/}
              <div className = "column border-left">
                <div >
               <center><h5 > إضافة معلومات التعليم </h5> </center> 
               <div className="form-row r">
             <label className="label" > التعليم  :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "0" > أساسي</option> 
              <option value= "1" > ثانوي</option>
              <option value= "2" > جامعة</option>
              <option value= "3" > دراسات عليا</option>
              
              </select>      
                          
              </div>
              <div className="form-row r">
             <label className="label"  >  مصدر الشهادة  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > التعليم  :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "0" > أول أساسي</option> 
              <option value= "1" > ثاني أساسي</option>
              <option value= "2" > ثالث أساسي</option>
              <option value= "3" > رابع أساسي </option>
              <option value= "3" > خامس أساسي </option>
              <option value= "3" > سادس أساسي </option>
              <option value= "3" > سابع أساسي </option>
              <option value= "3" > ثامن أساسي </option>
              <option value= "3" > شهادة التعليم الأساسي </option>
              <option value= "3" > أول ثانوي  </option>
              <option value= "3" > ثاني ثانوي  </option>
              <option value= "3" > شهادة التعليم الثانوي  </option>
              <option value= "3" > سنة أولى جامعة   </option>
              <option value= "3" > سنة ثانية جامعة  </option>
              <option value= "3" > سنة ثالثة جامعة  </option>
              <option value= "3" > سنة رابعة جامعة  </option>
              <option value= "3" > سنة خامسة جامعة  </option>
              <option value= "3" > سنة سادسة جامعة  </option>
              <option value= "3" > سنة سابعة جامعة  </option>
              <option value= "3" > ماستر سنة أولى  </option>
              <option value= "3" > ماستر سنة ثانية  </option>
              <option value= "3" > دكتوراه سنة أولى  </option>
              <option value= "3" > دكتوراه سنة ثانية  </option>
              
              </select>      
                          
              </div>
              <div className="form-row r">
             <label className="label" > الاجتهاد  :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "0" > ممتاز</option> 
              <option value= "1" > جيد جداً</option>
              <option value= "2" > جيد</option>
              <option value= "3" > متوسط </option>
              <option value= "3" > ضعيف </option>
              
              </select>      
                          
              </div>
            
             
              { healthyInfo.map((info, index) => {
   return (
    
     <center>
        <h5 > إضافة المعلومات الصحية </h5>
       <input   className="input-f"
         name="healthProblem"
         placeholder= " المشكلة الصحية "
         value={info.healthProblem}
         onChange={e => handleInputChange(e, index)}
       />
       <input
         className="input-f"
         name="physician"
         placeholder= "الطبيب المعالج"
         value={info.physician}
         onChange={e => handleInputChange(e, index)}
       />
           <input
         className="input-f"
         name="therapy"
         placeholder= "العلاج"
         value={info.therapy}
         onChange={e => handleInputChange(e, index)}
       />
        <input   className="input-f"
         name="monthlyCost"
         placeholder= "التكلفة الشهرية"
         value={info.monthlyCost}
         onChange={e => handleInputChange(e, index)}
       />
        <input   className="input-f"
         name="medicineCost"
         placeholder= "تكلفة الدواء"
         value={info.medicineCost}
         onChange={e => handleInputChange(e, index)}
       />
        <textarea
                name="notes"
                placeholder= "ملاحظات"
                value={info.notes}
                onChange={e => handleInputChange(e, index)}
             />
   
  
     </center>
   );
 })}
               <a href="/" className="next" type="submit">التالي  &raquo; </a>
    
              </div>
              </div>
       </form>
       </div>
       )
 
  }
  export default ChildrenInformation;