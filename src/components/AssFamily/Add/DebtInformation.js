import React, { useState } from "react";
import './PersonalInformation.css';


function DebtInformation() {
  const [depts, setDepts] = useState([{ source : "" , deptValue : "" }]);
  const [IDN, setIDN] = useState('');


  // handle input change
  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...depts];
    list[index][name] = value;
    setDepts(list);
    console.log(depts);
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...depts];
    list.splice(index, 1);
    setDepts(list);
  };

  // handle click event of the Add button
  const handleAddClick = () => {
    setDepts([...depts, { source: "", deptValue: "" }]);
  };

  return (
  
   
           <div className="contain">
                <center>
         <h6 className="h6-h2"> إضافة دين  </h6>
         <div className="p-t" >
       {depts.map((dept, index) => {
        return (
          <div className="box">
            <input   className="input-f"
              name="source"
			  placeholder= "المصدر"
              value={dept.source}
              onChange={e => handleInputChange(e, index)}
            />
            <input
              className="input-f"
              name="deptValue"
			  placeholder= "القيمة"
              value={dept.deptValue}
              onChange={e => handleInputChange(e, index)}
            />
        
              {depts.length !== 1 && 
              <button className="btn btn-primary btn-rounded mr" 
              onClick={() => handleRemoveClick(index)}>حذف</button>}
              {depts.length - 1 === index && 
              <button className="btn btn-primary btn-rounded mr"
               onClick={handleAddClick}>إضافة</button>}
         
          </div>
        );
      })}
     </div>
    
     </center>
     <br />
     <br />
     <a href="/partners" className="next" type="submit">التالي  &raquo; </a>
     <br />
     <br />
     <a href="" className="next" type="submit">تخطي  &raquo; </a>
   
       </div>
     
             
   
   

           
    
   
    
  );
}

export default DebtInformation;