import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import './PersonalInformation.css';
import DatePicker from 'react-date-picker';


class PersonalInformation extends Component{
  
    render(){

       return(
       
           <div className="contain">
              <h6> إضافة المعلومات الشخصية </h6>
             <form>
                <div className = "column border-left">
                <div >
             <div className="form-row r">
             <label className="label" > الاسم الأول  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > اسم الأب  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >  الكنية    :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >  اسم الأم  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > كنية الأم  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >مكان الولادة  :</label>
             <input className="input" />
              </div>


              <div className="form-row r">
              <label className="col-form-label col-sm-2 pt-0 label"> الجنس :</label>
              <div >
             <div className="form-check">
            <input  type="radio" name="flexRadioDefault" id="flexRadioDefault1" />
             <label className="form-check-label" for="flexRadioDefault1"> ذكر </label> 
            </div>
            <div className="form-check">
            <input  type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked />
             <label className="form-check-label" for="flexRadioDefault2">
             أنثى
              </label>
              </div>
              </div>
              </div>

              <div className="form-row r">
             <label className="label" > الجنسية  :</label>
             <input className="input" />
              </div>
             
              <div className="form-row r">
             <label className="label" > رقم الهوية  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > الحالة الاجتماعية  :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "0" > أعزب</option> 
              <option value= "1" > متزوج</option>
              <option value= "2" > خاطب</option>
              <option value= "3" > أرمل</option>
              <option value= "4" > مطلق</option>
              </select>      
                          
              </div>
              </div> 
              </div>
              
              {/*********col ****************************************/}
              <div className = "column border-left">
                <div >
           
              <div className="form-row r">
             <label className="label" > تاريخ الميلاد   :</label>
             <input className="input"  id="date" name="date" placeholder="MM/DD/YYY" type="text" />
             </div>
             <div className="form-row r">
             <label className="label" > الالتزام الأخلاقي    :</label>
             <select //value = {this.state.area_id} 
              //onChange={this.handleChangeSelect} 
              > 
              <option value= "" > ملتزم </option> 
              <option value= "1" > غير ملتزم</option>
              <option value= "2" > متوسط</option>

              </select>
              </div>
              <div className="form-row r">
             <label className="label" > العنوان   :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > تاريخ العائلة  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > العمل الحالي  :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" >  عنوان العمل الحالي :</label>
             <input className="input" />
              </div>
              <div className="form-row r">
             <label className="label" > تاريخ العمل الحالي  :</label>
             <input className="input" />
              </div>
               <a href="/resedunt" className="next" type="submit">التالي  &raquo; </a>
                     
              
              </div>
              </div>
       </form>
       </div>
       )
 }
  }
  export default PersonalInformation;