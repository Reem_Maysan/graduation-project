import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import './PersonalInformation.css';
import DatePicker from 'react-date-picker';
import '../Routes';
import Axios from 'axios';
import * as myConstClass from '../../constantsFile.js';

class PersonalInformation extends Component {

   constructor(props) {
      super(props);
      this.state = {
         firstName: '',
         fatherName: '',
         lastName: '',
         motherFirstName: '',
         motherLastName: '',
         birthPlace: '',
         birthDate: '',
         nationality: '',
         IDN: '',
         maritalStatus: 'أعزب',
         education: '',
         ethicalCommitment: 'ملتزم',
         gender: '',
         address: '',
         familyHistory: '',
         currentWork: '',
         currentWorkAddress: '',
         currentWorkPhone: 0,
      };
   }
   handleChange = (event) => {
      //  const newData = this.state
      // newData[event.target.id] = event.target.value
      this.setState({ [event.target.id]: event.target.value })
      console.log(this.state)
   }
   handleSubmit = (event) => {
      /* fetch('https://thawing-everglades-11191.herokuapp.com/api/v1/addForm/personalInfo'
         //     myConstClass.ADD_FORM_PERSONALINFO_URL, 
         , {
            method: 'POST',
            // We convert the React state to JSON and send it as the POST body
            headers: {
               "Accept": "application/json",
               "Content-Type": "application/json"
            },
            body: JSON.stringify(
               {
                  firstName: this.state.firstName,
                  fatherName: this.state.fatherName,
                  lastName: this.state.lastName,
                  motherFirstName: this.state.motherFirstName,
                  motherLastName: this.state.motherLastName,
                  birthPlace: this.state.birthPlace,
                  birthDate: this.state.birthDate,
                  nationality: this.state.nationality,
                  IDN: this.state.IDN,
                  maritalStatus: this.state.maritalStatus,
                  education: this.state.education,
                  ethicalCommitment: this.state.ethicalCommitment,
                  gender: this.state.gender,
                  address: this.state.address,
                  familyHistory: this.state.familyHistory,
                  currentWork: this.state.currentWork,
                  currentWorkAddress: this.state.currentWorkAddress,
                  currentWorkPhone: this.state.currentWorkPhone,
               }
            ),
         }).then(function (response) {
            console.log(response)
            console.log('we are soooo fucked up')
            return response.json();
         });
 */

      event.preventDefault()
      console.log('hiiiiiiiiiiii')
      console.log(this.state,)
      fetch('https://thawing-everglades-11191.herokuapp.com/api/v1/addForm/personalInfo'
         //  'https://jsonplaceholder.typicode.com/posts'
         , {
            method: 'POST',
            body: JSON.stringify(
               /*  {
                   firstName: "ايمان",
                   fatherName: "حسن",
                   lastName: "منذر",
                   motherFirstName: "مادخلكن",
                   motherLastName: "منذر",
                   birthPlace: "بعلبك",
                   birthDate: "1997-5-13",
                   nationality: "لبناني",
                   IDN: "000027793310",
                   maritalStatus: "أعزب",
                   education: "برمجيات هندسة",
                   ethicalCommitment: "ملتزم",
                   gender: "m",
                   address: "العابدين زين الامام حي شورى مهاجرين",
                   familyHistory: "عائلة عريقة",
                   currentWork: "لااعمل",
                   currentWorkAddress: "لايوجد",
                   currentWorkPhone: 0
                } */
               this.state

            ),
            headers: {
               'Accept': 'application/json',
               'Content-type': 'application/json'
            },
         })
         .then((response) => response.json())
         .then((json) => console.log(json));
      /*  Axios.post('https://thawing-everglades-11191.herokuapp.com/api/v1/addForm/personalInfo',
        //  this.state,
           {
             firstName: 'reem', //this.state.data['firstName'],
             fatherName: '', //this.state.data['fatherName'],
             lastName: '', //this.state.data['lastName'],
             motherFirstName: '',// this.state.data['motherFirstName'],
             motherLastName: '',// this.state.data['motherLastName'],
             birthPlace: '',// this.state.data['birthPlace'],
             birthDate: '', //this.state.data['birthDate'],
             nationality: '',//this.state['nationality'],
             IDN: '', //this.state.data['IDN'],
             maritalStatus: '', //this.state.data['maritalStatus'],
             education: '', //this.state.data['education'],
             ethicalCommitment: '', //this.state.data['ethicalCommitment'],
             gender: '', //this.state.data['gender'],
             address: '', //this.state.data['address'],
             familyHistory: '', //this.state.data['familyHistory'],
             currentWork: '', //this.state.data['currentWork'],
             currentWorkAddress: '', //this.state.data['currentWorkAddress'],
             currentWorkPhone: '',// this.state.data['currentWorkPhone'],
          }, 
          {
             headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
             }
          }
       ).then((res) => {
          console.log(res.data)
       }).catch(error => {
          console.log('we are so fucked up')
          console.log(this.state)
          console.log(error)
       })
  */

      /*    Axios({
            method: "post",
            url: "https://thawing-everglades-11191.herokuapp.com/api/v1/addForm/personalInfo",
            data:
               JSON.stringify(this.state),
   
            headers: {
               "Accept": "application/json",
               "Content-Type": "application/json"
            }
         })
            .then(function (response) {
               //handle success
               console.log(response);
            })
            .catch(function (response) {
               //handle error
               console.log(response);
            }); */
   }
   render() {
      //url: myConstClass.ADD_FORM_PERSONALINFO_URL
      return (

         <div className="contain">

            <h6> إضافة المعلومات الشخصية </h6>

            <form onSubmit={(e) => this.handleSubmit(e)}>
               <div className="column border-left">
                  <div >
                     <div className="form-row r">
                        <label className="label" > الاسم الأول  :</label>
                        <input className="input" onChange={(e) => this.handleChange(e)} id="firstName" />
                     </div>
                     <div className="form-row r">
                        <label className="label" > اسم الأب  :</label>
                        <input className="input" onChange={(e) => this.handleChange(e)} id="fatherName" />
                     </div>
                     <div className="form-row r">
                        <label className="label" >  الكنية    :</label>
                        <input className="input" onChange={(e) => this.handleChange(e)} id="lastName" />
                     </div>
                     <div className="form-row r">
                        <label className="label" >  اسم الأم  :</label>
                        <input className="input" onChange={(e) => this.handleChange(e)} id="motherFirstName" />
                     </div>
                     <div className="form-row r">
                        <label className="label" > كنية الأم  :</label>
                        <input className="input" onChange={(e) => this.handleChange(e)} id="motherLastName" />
                     </div>
                     <div className="form-row r">
                        <label className="label" >مكان الولادة  :</label>
                        <input className="input" onChange={(e) => this.handleChange(e)} id="birthPlace" />
                     </div>


                     <div className="form-row r">
                        <label className="col-form-label col-sm-2 pt-0 label"> الجنس :</label>
                        <div >
                           <div className="form-check">
                              <input type="radio" name="male"
                                 value="m"
                                 //id="flexRadioDefault1"
                                 onChange={(e) => this.handleChange(e)} id="gender" />
                              <label className="form-check-label" > ذكر </label>
                           </div>
                           <div className="form-check">
                              <input type="radio" name="female"
                                 //id="flexRadioDefault2" checked
                                 value="f"
                                 onChange={(e) => this.handleChange(e)} id="gender" />
                              <label className="form-check-label"  >
                                 أنثى
              </label>
                           </div>
                        </div>
                     </div>

                     <div className="form-row r">
                        <label className="label" > الجنسية  :</label>
                        <input className="input"
                           onChange={(e) => this.handleChange(e)} id="nationality" />
                     </div>

                     <div className="form-row r">
                        <label className="label" > رقم الهوية  :</label>
                        <input className="input"
                           onChange={(e) => this.handleChange(e)} id="IDN" />
                     </div>
                     <div className="form-row r">
                        <label className="label" > الحالة الاجتماعية  :</label>
                        <select
                           value={this.state.maritalStatus}
                           onChange={(e) => this.handleChange(e)} id="maritalStatus"
                        >
                           <option value="0" > أعزب</option>
                           <option value="1" > متزوج</option>
                           <option value="2" > خاطب</option>
                           <option value="3" > أرمل</option>
                           <option value="4" > مطلق</option>
                        </select>

                     </div>
                  </div>
               </div>

               {/*********col ****************************************/}
               <div className="column border-left">
                  <div >

                     <div className="form-row r">
                        <label className="label" > تاريخ الميلاد   :</label>
                        <input className="input" id="date" name="date" placeholder="YYYY-MM-DD" type="text"
                           onChange={(e) => this.handleChange(e)} id="birthDate" />
                     </div>
                     <div className="form-row r">
                        <label className="label" >التعليم  :</label>
                        <input className="input" onChange={(e) => this.handleChange(e)} id="education" />
                     </div>
                     <div className="form-row r">
                        <label className="label" > الالتزام الأخلاقي    :</label>
                        <select
                           value={this.state.ethicalCommitment}
                           //onChange={this.handleChangeSelect} 
                           onChange={(e) => this.handleChange(e)} id="ethicalCommitment"
                        >
                           <option value="0" > ملتزم </option>
                           <option value="1" > غير ملتزم</option>
                           <option value="2" > متوسط</option>

                        </select>
                     </div>
                     <div className="form-row r">
                        <label className="label" > العنوان   :</label>
                        <input className="input"
                           onChange={(e) => this.handleChange(e)} id="address" />
                     </div>
                     <div className="form-row r">
                        <label className="label" > تاريخ العائلة  :</label>
                        <input className="input"
                           onChange={(e) => this.handleChange(e)} id="familyHistory" />
                     </div>
                     <div className="form-row r">
                        <label className="label" > العمل الحالي  :</label>
                        <input className="input"
                           onChange={(e) => this.handleChange(e)} id="currentWork" />
                     </div>
                     <div className="form-row r">
                        <label className="label" >  عنوان العمل الحالي :</label>
                        <input className="input"
                           onChange={(e) => this.handleChange(e)} id="currentWorkAddress" />
                     </div>
                     <div className="form-row r">
                        <label className="label" > هاتف العمل الحالي  :</label>
                        <input className="input"
                           onChange={(e) => this.handleChange(e)} id="currentWorkPhone" />
                     </div>
                     <a href="/resedunt"
                        className="next" type="submit">التالي  &raquo; </a>
                     {/* <button>submit</button>  */}
                  </div>
               </div>
            </form>
         </div>
      )
   }
}
export default PersonalInformation;