import { Container } from 'react-bootstrap';
import './PersonalInformation.css';
import DatePicker from 'react-date-picker';
import React, { useState } from "react";
//import useForm from 'react-hook-form';
import '../Routes';

function ResidentInformation() {

  const [rebuildingState, setRebuildingState] = useState([{
    title: "", description: ""

  }]);

  // reemy
  const [dataState, setDataState] = useState({
    residentStatus: "ملك",
    monthlyRent: 0,
    houseArea: 0,
    roomsNumber: 0,
    residentsNumber: 0,
    houseType: "",
    humidity: "لايوجد",
    upholsteryStatus: "ممتاز",
    calddingStatus: "ممتاز",
    residencePhone: 0,
    residenceOwner: "",
    notes: "",
    IDN: "27793310",
    rebuilding: [{
      title: "",
      description: ""
    }]
    /*  title: [],
     description: [], */
  });

  const [titleState, setTitleState] = useState([{
    title: ""
  }]);

  const [descriptionState, setDescriptionState] = useState([{
    description: ""
  }]);

  const handleChange = (e) => {
    // const { id, value } = e.target;
    console.log('hiiiiii');
    console.log(e.target.id);
    console.log(e.target.value);
    setDataState({ ...dataState, [e.target.id]: e.target.value });

    /*    switch (id) {
         case 'residentStatus': {
           setDataState({ ...dataState, residentStatus: value });
           console.log('gggggggggggggggg');
         }
   
           break;
         case 'monthlyRent':
   
           setDataState({ ...dataState, monthlyRent: value });
           break;
         case 'houseArea':
           setDataState({ ...dataState, houseArea: value });
   
           break;
         case 'roomsNumber':
           setDataState({ ...dataState, roomsNumber: value });
   
           break;
         case 'residentsNumber':
           setDataState({ ...dataState, residentsNumber: value });
   
           break;
         case 'houseType':
           setDataState({ ...dataState, houseType: value });
   
           break;
         case 'humidity':
           setDataState({ ...dataState, humidity: value });
   
           break;
         case 'upholsteryStatus':
           setDataState({ ...dataState, upholsteryStatus: value });
   
           break;
         case 'calddingStatus':
           setDataState({ ...dataState, calddingStatus: value });
   
           break;
         case 'residencePhone':
           setDataState({ ...dataState, residencePhone: value });
   
           break;
         case 'residenceOwner':
           setDataState({ ...dataState, residenceOwner: value });
   
           break; case
           'notes':
           setDataState({ ...dataState, notes: value });
   
           break;
        
         case 'title':
           setDataState({ ...dataState, title: value });
   
           break;
         case 'description':
           setDataState({ ...dataState, description: value });
   
           break;
         default:
           break;
       } */

    /*   setDataState({
    [event.target.id]: event.target.value,
  })
  console.log(dataState) */
    //  console.log(dataState)
  }
  // reemy

  // handle input change
  const handleTitleChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...titleState];
    list[index] = value;
    setTitleState(list);
    setDataState({ ...dataState, title: list });
    console.log(titleState);
    console.log(dataState);
  };

  const handleDescriptionChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...descriptionState];
    list[index] = value;
    setDescriptionState(list);
    setDataState({ ...dataState, description: list });
    console.log(descriptionState);
    console.log(dataState);
  };

  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...rebuildingState];
    list[index][name] = value;
    setRebuildingState(list);
    console.log("**************************************");
    setDataState({ ...dataState, rebuilding: rebuildingState });



    /* const titleList = dataState.title;
    titleList[index] = rebuildingState[index].title;
    setTitleState(titleList);
    console.log(titleState);
    // console.log(titleList);


    const descriptionList = dataState.description;
    descriptionList[index] = rebuildingState[index].description;
    setDescriptionState(descriptionList);
    console.log(descriptionState);
    //console.log(descriptionList);


    console.log("**************************************");
    setDataState({ ...dataState, description: descriptionList, title: titleList });
    // console.log(rebuildingState);
    console.log(dataState); */
  };

  // handle click event of the Remove button
  const handleRemoveClick = index => {
    const list = [...rebuildingState];
    list.splice(index, 1);
    setRebuildingState(list);
    setDataState({ ...dataState, rebuilding: rebuildingState });


    /* const titleList = [...titleState];
    const descriptionList = [...descriptionState];
    
    titleList.splice(index, 1);
    descriptionList.splice(index, 1);
   
    setTitleState(titleList);
    setDescriptionState(descriptionList); */

  };

  // handle click event of the Add button
  const handleAddClick = () => {
    setRebuildingState([...rebuildingState, { title: "", description: "" }]);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('\\\\\\\\\\\\handleSubmit//////////')
    console.log(dataState)
    fetch('https://thawing-everglades-11191.herokuapp.com/api/v1/addForm/residenceInfo'
      , {
        method: 'POST',
        body: JSON.stringify(
          /*  {
            residentStatus:"ملك",
     monthlyRent:0,
     houseArea:75,
     roomsNumber:3,
     residentsNumber:4,
     houseType:"شقة",
     humidity:"متوسطة",
     upholsteryStatus:"وسط",
     calddingStatus:"جيد",
     residencePhone:3313882,
     residenceOwner:"-",
     notes:"-",
     IDN:"000027793310",
     title:["a","b","c"],
     description:[ "des1","des2","des3" ]
           } */
          dataState
        ),
        headers: {
          'Accept': 'application/json',
          'Content-type': 'application/json'
        },
      })
      .then((response) => response.json())
      .then((json) => console.log(json));

  }
  return (

    <div className="contain">

      <h6 className="h6-h2"> إضافة معلومات السكن </h6>

      <form onSubmit={e => { handleSubmit(e) }}>
        <div className="column border-left">
          <div >
            <div className="form-row r">
              <label className="label" > حالة العقار   :</label>
              <select //value = {this.state.area_id} 
                //onChange={this.handleChangeSelect} 
                onChange={(e) => handleChange(e)} id="residentStatus"
              //onChange={handleChange}

              >
                <option value="0" > ملك</option>
                <option value="1" > آجار</option>
                <option value="2" > استثمار</option>
              </select>
            </div>

            <div className="form-row r">
              <label className="label" > الآجار الشهري  :</label>
              <input className="input" type="text" pattern="[0-9]*"
                onChange={(e) => handleChange(e)} id="monthlyRent"
              //onChange={handleChange}
              />
            </div>
            <div className="form-row r">
              <label className="label" > مكان المنزل :</label>
              <input className="input" type="text" pattern="[0-9]*"
                onChange={(e) => handleChange(e)} id="houseArea"
              // onChange={handleChange}
              />
            </div>
            <div className="form-row r">
              <label className="label" >  عدد الغرف    :</label>
              <input className="input" type="number"
                onChange={(e) => handleChange(e)}
                //onChange={handleChange}
                id="roomsNumber" />
            </div>
            <div className="form-row r">
              <label className="label" >  نوع المنزل  :</label>
              <input className="input"
                onChange={(e) => handleChange(e)} id="houseType"
              // onChange={handleChange}
              />
            </div>
            <div className="form-row r">
              <label className="label" >  الرطوبة   :</label>
              <select //value = {this.state.area_id} 
                //onChange={this.handleChangeSelect} 
                onChange={(e) => handleChange(e)} id="humidity"
              //onChange={handleChange}
              >
                <option value="0" > لايوجد </option>
                <option value="1" > خفيفة </option>
                <option value="2" > متوسطة </option>
                <option value="2" > عالية </option>
              </select>
            </div>
            <div className="form-row r">
              <label className="label" >  التنجيد   :</label>
              <select //value = {this.state.area_id} 
                //onChange={this.handleChangeSelect} 
                onChange={(e) => handleChange(e)} id="upholsteryStatus"
              //  onChange={handleChange}
              >
                <option value="0" > ممتاز </option>
                <option value="1" > جيد </option>
                <option value="2" > وسط </option>
                <option value="2" > سيء </option>
              </select>
            </div>
            <div className="form-row r">
              <label className="label" >  حالة الكسوة   :</label>
              <select //value = {this.state.area_id} 
                //onChange={this.handleChangeSelect} 
                onChange={(e) => handleChange(e)} id="calddingStatus"
              //  onChange={handleChange}
              >
                <option value="0" > ممتاز </option>
                <option value="1" > جيد </option>
                <option value="2" > وسط </option>
                <option value="2" > سيء </option>
              </select>
            </div>

          </div>
        </div>

        {/*********col ****************************************/}
        <div className="column border-left">
          <div >
            <div className="form-row r">
              <label className="label" > رقم المنزل  :</label>
              <input className="input" type="text" pattern="[0-9]*"
                onChange={(e) => handleChange(e)} id="residentsNumber"
              // onChange={handleChange}
              />
            </div>
            {/*  <div className="form-row r">
              <label className="label" > رقم الهوية  :</label>
              <input className="input" type="text" pattern="[0-9]*"
                onChange={(e) => handleChange(e)} id="IDN"
              // onChange={handleChange}
              />
            </div> */}
            <div className="form-row r">
              <label className="label" > مالك المنزل  :</label>
              <input className="input"
                onChange={(e) => handleChange(e)} id="residenceOwner"
              // onChange={handleChange}
              />
            </div>
            <div className="form-row r">
              <label className="label" > ملاحظات     :</label>
              <textarea className="input" rows="7"
                onChange={(e) => handleChange(e)} id="notes"
              //     onChange={handleChange}
              ></textarea>
            </div>

            <label className="label"  >  الترميم (إن وجد)  :</label>
            {
            /* 
            {titleState.map((rebuild, index) => {

              return (
                <div className="box">
                  <div className="form-row r">
                    <input className="input-f"
                      name="title"
                      placeholder="العنوان"
                      value={rebuild.title}
                      onChange={e => handleTitleChange(e, index)}
                      // onChange={(e) => handleChange(e)}
                      id="title"
                    />
                  </div>
                </div>
              );
            })}
            {descriptionState.map((rebuild, index) => {

              return (
                <div className="box">
                  <div className="form-row r">
                    <textarea
                      name="description"
                      placeholder="الوصف"
                      value={rebuild.description}
                      id="description"
                      onChange={e => handleDescriptionChange(e, index)}
                    //  onChange={(e) => handleChange(e)} id="description"
                    />
                    {titleState.length !== 1 && descriptionState.length !== 1 &&
                      <button className="btn btn-primary btn-rounded mr"
                        onClick={() => handleRemoveClick(index)}>حذف</button>}
                    {titleState.length - 1 === index && descriptionState.length - 1 === index &&
                      <button className="btn btn-primary btn-rounded mr"
                        onClick={handleAddClick}>إضافة</button>}
                  </div>
                </div>
              );
            })} */}


            {rebuildingState.map((rebuild, index) => {
              return (
                <div className="box">
                  <div className="form-row r">
                    <input className="input-f"
                      name="title"
                      placeholder="العنوان"
                      value={rebuild.title}
                      onChange={e => handleInputChange(e, index)}
                      id="title"
                    />
                    <textarea
                      name="description"
                      placeholder="الوصف"
                      value={rebuild.description}
                      id="description"
                      onChange={e => handleInputChange(e, index)}
                      id="description"
                    />

                    {rebuildingState.length !== 1 &&
                      <button className="btn btn-primary btn-rounded mr"
                        onClick={() => handleRemoveClick(index)}>حذف</button>}
                    {rebuildingState.length - 1 === index &&
                      <button className="btn btn-primary btn-rounded mr"
                        onClick={handleAddClick}>إضافة</button>}

                  </div>
                </div>
              );
            })}
          </div>
          <a href="/health" className="next" type="submit">التالي  &raquo; </a>
          {/*  <button>submit</button> */}


        </div>

      </form>
    </div>
  )

}
export default ResidentInformation;