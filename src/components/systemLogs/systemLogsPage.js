import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Checkbox from "@material-ui/core/Checkbox";
import './Systemlog.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../constants';
import '../Routes';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Button } from "@material-ui/core";
import { TrainRounded } from "@material-ui/icons";

import EditIcon from '@material-ui/icons/Edit';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';

import SideBar from '../SideBar/SideBar';
function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            item="tabpanel"
            hidden={value !== index}
            id={`nav-tabpanel-${index}`}
            aria-labelledby={`nav-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    };
}

function LinkTab(props) {
    return (
        <Tab
            component="a"
            onClick={(event) => {
                event.preventDefault();
            }}
            {...props}
        />
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

function SystemLogsPage(props) {

    const [systemLogsInfo, setSystemLogsInfo] = useState({
        loading: false,
        currentPage: "formLogging",
        data: {
            form_logging: [{
                date: "2017-03-01",
                description: "Add personal form",
                user: {
                    firstName: "reem",
                    lastName: "may",
                    fatherName: "ahmad"
                },
            }],
            auth_logging: [{
                date: "2017-03-01",
                description: "manager logged in",
                user: {
                    firstName: "lima",
                    lastName: "wardeh",
                    fatherName: "saeed"
                },
            }],
            Aid_logging: [{
                date: "2017-03-01",
                description: "item description bla bla",
                user: {
                    firstName: "hala",
                    lastName: "haj",
                    fatherName: "hasan"
                },
            }],
        }
    });


    const systemLogsInfoGet = async () => {
        console.log('systemLogsInfoGet systemLogsInfoGet systemLogsInfoGet')
        setSystemLogsInfo({ ...systemLogsInfo, loading: true });
        const res = await axios.get(myConstClass.GET_SystemLogs)
        console.log('json data')
        console.log(res.data)
        setSystemLogsInfo({ ...systemLogsInfo, loading: false, data: res.data });
    }

    useEffect(() => {
        console.log('useEffect')
        systemLogsInfoGet();
    }, []);


    const classes = useStyles();
    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };


    return (
        <div>
             {
                systemLogsInfo.loading === true ?
                    <Backdrop
                        open>
                        <CircularProgress color="inherit" />
                    </Backdrop> :

                    <div className={classes.root}>
                        <SideBar />
                        <AppBar position="static"  >
                            <Tabs className="system_log"
                                variant="fullWidth"
                                value={value}
                                onChange={handleChange}
                                aria-label="nav tabs example"
                            >
                                <LinkTab label="عمليات الاستمارات" href="/form" {...a11yProps(0)} />
                                <LinkTab label="عمليات تسجيل الدخول" href="/auth" {...a11yProps(1)} />
                                <LinkTab label="عمليات المساعدات" href="/item" {...a11yProps(2)} />
                            </Tabs>
                        </AppBar>
                        <TabPanel value={value} index={0}>
                            <div className="container_rectangle">
                                <Box marginLeft={10} marginRight={10} marginTop={5}>
                                    <TableContainer component={Paper} >
                                        <Table aria-label="collapsible table" >
                                            <TableHead >
                                                <TableRow >
                                                    <TableCell align="center">اسم المستخدم</TableCell>
                                                    <TableCell align="center">العملية</TableCell>
                                                    <TableCell align="center">التاريخ</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    systemLogsInfo.data != null ?
                                                        Array.isArray(systemLogsInfo.data.form_logging) ?
                                                            systemLogsInfo.data.form_logging.map((item, index) => (
                                                                <TableRow key={index}>
                                                                    <TableCell align="center">{item.firstName + ' ' + item.fatherName + ' ' + item.lastName}</TableCell>
                                                                    <TableCell align="center">{item.description}</TableCell>
                                                                    <TableCell align="center">{item.date}</TableCell>

                                                                </TableRow>
                                                            )) :
                                                            <TableRow >
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                            </TableRow>
                                                        : <TableRow >
                                                            <TableCell align="center">لا يوجد</TableCell>
                                                            <TableCell align="center">لا يوجد</TableCell>
                                                            <TableCell align="center">لا يوجد</TableCell>
                                                        </TableRow>
                                                }
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Box>
                            </div>
                        </TabPanel>

                        <TabPanel value={value} index={1}>
                            <div className="container_rectangle">
                                <Box marginLeft={10} marginRight={10} marginTop={5}>
                                    <TableContainer component={Paper} >
                                        <Table aria-label="collapsible table" >
                                            <TableHead >
                                                <TableRow >
                                                    <TableCell align="center">اسم المستخدم</TableCell>
                                                    <TableCell align="center">العملية</TableCell>
                                                    <TableCell align="center">التاريخ</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    systemLogsInfo.data != null ?
                                                        Array.isArray(systemLogsInfo.data.auth_logging) ?
                                                            systemLogsInfo.data.auth_logging.map((item, index) => (
                                                                <TableRow key={index}>
                                                                    <TableCell align="center">{item.firstName + ' ' + item.fatherName + ' ' + item.lastName}</TableCell>
                                                                    <TableCell align="center">{item.description}</TableCell>
                                                                    <TableCell align="center">{item.date}</TableCell>

                                                                </TableRow>
                                                            )) :
                                                            <TableRow >
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                            </TableRow>
                                                        : <TableRow >
                                                            <TableCell align="center">لا يوجد</TableCell>
                                                            <TableCell align="center">لا يوجد</TableCell>
                                                            <TableCell align="center">لا يوجد</TableCell>
                                                        </TableRow>
                                                }
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Box>
                            </div>
                        </TabPanel>

                        <TabPanel value={value} index={2}>
                            <div className="container_rectangle">
                                <Box marginLeft={10} marginRight={10} marginTop={5}>
                                    <TableContainer component={Paper} >
                                        <Table aria-label="collapsible table" >
                                            <TableHead >
                                                <TableRow >
                                                    <TableCell align="center">اسم المستخدم</TableCell>
                                                    <TableCell align="center">العملية</TableCell>
                                                    <TableCell align="center">التاريخ</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {
                                                    systemLogsInfo.data != null ?
                                                        Array.isArray(systemLogsInfo.data.Aid_logging) ?
                                                            systemLogsInfo.data.Aid_logging.map((item, index) => (
                                                                <TableRow key={index}>
                                                                    <TableCell align="center">{item.firstName + ' ' + item.fatherName + ' ' + item.lastName}</TableCell>
                                                                    <TableCell align="center">{item.description}</TableCell>
                                                                    <TableCell align="center">{item.date}</TableCell>

                                                                </TableRow>
                                                            )) :
                                                            <TableRow >
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                                <TableCell align="center">لا يوجد</TableCell>
                                                            </TableRow>
                                                        : <TableRow >
                                                            <TableCell align="center">لا يوجد</TableCell>
                                                            <TableCell align="center">لا يوجد</TableCell>
                                                            <TableCell align="center">لا يوجد</TableCell>
                                                        </TableRow>
                                                }
                                            </TableBody>
                                        </Table>
                                    </TableContainer>
                                </Box>
                            </div>

                        </TabPanel>


                    </div>
            } 
        </div>



    );


}
export default SystemLogsPage;
