import react from 'react';
import HomeIcon from '@material-ui/icons/Home';
import NoteIcon from '@material-ui/icons/Note';
import PageviewIcon from '@material-ui/icons/Pageview';
import PlaylistAddCheckIcon from '@material-ui/icons/PlaylistAddCheck';
import VisibilityIcon from '@material-ui/icons/Visibility';
import InfoIcon from '@material-ui/icons/Info';
import SubjectIcon from '@material-ui/icons/Subject';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import SearchIcon from '@material-ui/icons/Search';
import PeopleIcon from '@material-ui/icons/People';
import PeopleOutlineIcon from '@material-ui/icons/PeopleOutline';
import LocalMallIcon from '@material-ui/icons/LocalMall';
import DialerSipIcon from '@material-ui/icons/DialerSip';
import ListIcon from '@material-ui/icons/List';
import * as myConstClass from '../../constants';



export const SideBarDataStudiesSection = [
   {
      title: '  عوائل الجمعية',
      icon: <HomeIcon />,
      link: '/displayAssociation',
   },
   {
      title: 'عوائل الحي',
      icon: <PageviewIcon />,
      link: '/displayFamily',
      // description: [...localStorage.getItem('rolePermissions')]

   },
   {
      title: 'العوائل المرفوضة',
      icon: <NoteIcon />,
      link: '/displayRejectedFamily',
      //   description: [...localStorage.getItem('directPermissions')]
   },
   {
      title: ' العوائل المعلقة',
      icon: <NoteIcon />,
      link: '/displayUnknowFamily',
      //   description: [...localStorage.getItem('directPermissions')]
   },
   {
      title: '  إضافة دور',
      icon: <NoteIcon />,
      link: '/roles',
      //   description: [...localStorage.getItem('directPermissions')]
   },
   {
      title: 'عمليات النظام',
      icon: <NoteIcon />,
      link: '/SystemLogsPage',
      //   description: [...localStorage.getItem('directPermissions')]
   },
   {
      title: '  المساعدات',
      icon: <NoteIcon />,
      link: '/allAids',
      //   description: [...localStorage.getItem('directPermissions')]
   },
]
export const SideBarDataDeposotiry = [

   {
      title: ' معلومات المستودع',
      icon: <InfoIcon />,
      link: '/displayStoreDetails/' + localStorage.getItem("departmentID"),

   },
   {
      title: 'المساعدات العينية',
      icon: <VisibilityIcon />,
      link: '/getConcreteAid',
   },
   {
      title: 'قوائم الصرف',
      icon: <PlaylistAddCheckIcon />,
      link: 'exchangeTabs'
   },
]
export const SideBarDataEdication = [

   {
      title: ' الدورات',
      icon: <SubjectIcon />,
      link: '/courses',
   },
   {
      title: 'المناسبات',
      icon: <EventAvailableIcon />,
      link: '/eventsAndVications',
   },
   {
      title: 'الاستفسارات',
      icon: <SearchIcon />,
      link: '/inquiries',

   },
   {
      title: 'إدارة الطلاب',
      icon: <PeopleIcon />,
      link: '/managestudents',

   },
   {
      title: 'إدارة أولياء الأمور',
      icon: <PeopleOutlineIcon />,
      link: '/managepearents',

   },
   {
      title: 'إدارة المعلمين',
      icon: <LocalMallIcon />,
      link: '/manageteacher',
      //   description: [...localStorage.getItem('directPermissions')]
   },
   {
      title: 'الشكاوى ',
      icon: <DialerSipIcon />,
      link: '/getManagerComplaints',
      //   description: [...localStorage.getItem('directPermissions')]
   },
]
export const SideBarDataFund = [

   {
      title: 'القوائم السنوية',
      icon: <ListIcon />,
      link: '/getFundsMainList',
   },
   {
      title: 'الحسابات',
      icon: <HomeIcon />,
      link: '/getAccountManagementPage',
   },
]


