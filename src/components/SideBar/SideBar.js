import react from 'react';
import './SideBar.css';
import { SideBarData, SideBarDataDeposotiry, SideBarDataFund, SideBarDataStudiesSection } from './SideBarData';
import { SideBarDataEdication } from './SideBarData';
import logo from '../images/logo.jpg';
import Navigation from '../Navbar/Navigation';
import PersonIcon from '@material-ui/icons/Person';
import { useState, useEffect } from "react";

function SideBar(props) {


  return (
    <div>
      <Navigation />
      <div className="Sidebar">
        {(() => {
          switch (localStorage.getItem('role')) {
            case "مدير القسم التعليمي":
              return <ul className="SidebarList">
                <div className="background_im" />
                {SideBarDataEdication.map((val, key) => {
                  return (
                    <li key={key} className="row" onClick={() => window.location.href = val.link} >
                      <div id="icon">{val.icon}</div>
                      <div id="title">{val.title}</div>
                    </li>
                  )
                })}
              </ul>;
            case "المدير التنفيذي":
              return <ul className="SidebarList">
                <div className="background_im" />
                {SideBarDataStudiesSection.map((val, key) => {
                  return (
                    <li key={key} className="row" onClick={() => window.location.href = val.link} >
                      <div id="icon">{val.icon}</div>
                      <div id="title">{val.title}</div>
                    </li>
                  )
                })}
              </ul>;
            case 'مدير الصندوق':
              return <ul className="SidebarList">
                <div className="background_im" />
                {SideBarDataFund.map((val, key) => {
                  return (
                    <li key={key} className="row" onClick={() =>
                      window.location.href = val.link

                    } >
                      <div id="icon">{val.icon}</div>
                      <div id="title">{val.title}</div>
                    </li>
                  )
                })}
              </ul>;
            case "مدير المستودع":
              return <ul className="SidebarList">
                <div className="background_im" />
                {SideBarDataDeposotiry.map((val, key) => {
                  return (
                    <li key={key} className="row" onClick={() => window.location.href = val.link} >
                      <div id="icon">{val.icon}</div>
                      <div id="title">{val.title}</div>
                    </li>
                  )
                })}
              </ul>;
            case "موظف استقبال":
              return <ul className="SidebarList">
                <div className="background_im" />
                {SideBarDataStudiesSection.map((val, key) => {
                  return (
                    <li key={key} className="row" onClick={() => window.location.href = val.link} >
                      <div id="icon">{val.icon}</div>
                      <div id="title">{val.title}</div>
                    </li>
                  )
                })}
              </ul>;
            default: return <p></p>;;
          }
        })()}
        {/* <ul className="SidebarList">
          <div className = "background_im" />
          {SideBarData.map((val, key) => {
              return (
                <li key={key} className="row" onClick={() => window.location.href="/content"} >
                  <div id="icon">{val.icon}</div>
                  <div id="title">{val.title}</div>
                </li>

              )
            }
          )}

        </ul>
          */}

      </div>
    </div>
  )


}

export default SideBar;