import react, { Component } from 'react';
import React from 'react';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import {Pie} from "react-chartjs-2";
import DoneIcon from '@material-ui/icons/Done';
import PriorityHighIcon from '@material-ui/icons/PriorityHigh';
import { Button, ButtonGroup, ButtonToolbar , DropdownButton } from 'reactstrap';
import SideBar from '../SideBar/SideBar';
import Avatar from '@material-ui/core/Avatar';
import './stat.css';
class stat extends Component{


render(){
    return(
        <div>
        <SideBar />
        <div className="stat-con">
        <div className="rows ">
  <div className="col-sm-3">
    <div className="card-all">
    <h5 className="card-title">
        عوائل الحي </h5>
      <center>  <p class="card-text">30%</p></center>
    </div>
  </div>
  <div className="col-sm-3">
    <div className="card-all">
    <h5 className="card-title">
      عوائل الجمعية</h5>
      <center>   <p class="card-text">3%</p></center>
    </div>
  </div>
  <div className="col-sm-3">
    <div className="card-all">
    <h5 className="card-title">
        عدد العوائل الكلي
         </h5>
         <center>  <p class="card-text">3%</p></center>
    </div>
  </div>
  <div className="col-sm-3">
    <div className="card-all">
    <h5 className="card-title">
        العوائل المعلقة</h5>
        <center>    <p class="card-text">13%</p></center>
    </div>
  </div>
  <div className="col-sm-3">
    <div className="card-all">
      <h5 className="card-title">
        العوائل المرفوضة</h5>
        <center>   <p class="card-text">3%</p></center>
    
        
    
        
     
    </div>
  </div>
</div>
{/**************************************سطر */}
              
              <div className="sec-row">
              <div className="col-md-6 ">
               <div className="card-child">
                     <h5 className="card-title">
                    عدد الأولاد الكلي </h5>
                    <div className="child-fam">
                        <p >  أولاد عوائل الجمعية الذكور :</p>
                        <p>  أولاد عوائل الجمعية الإناث :</p>
                        <p >طلاب الجامعات الذكور في عوائل الجمعية : </p>
                        <p >  أولاد عوائل الحي الذكور :</p>
                        <p>  أولاد عوائل الحي الإناث :</p>
                        <p >طلاب الجامعات الإناث في عوائل الحي : </p>
                        <p >طلاب الجامعات الذكور في عوائل الحي : </p>
                        <p >  العدد الكلي لطلاب الجامعات الذكور :</p>
                        <p>  العدد الكلي لطلاب الجامعات   الإناث :</p>
                       
                        </div>
        
    
        
     
    </div>
    </div>
    <div className="col-md-6">
      <div className="rows-t">
          <div className="col-md-9">
          <div className="card-a">
          <h5 className="card-title">
        أعداد الأشخاص الكلية </h5>
        <center>   <p class="card-text">عدد الأشخاص في عوائل الجمعية : </p>
        <p class="card-text">عدد الأشخاص في عوائل الحي : </p>
        <p class="card-text">عدد الأشخاص الكلي :</p>
        </center>
    </div>
          </div>
          <div className="col-md-9">
          <div className="card-a">
          <h5 className="card-title">
         أعداد النساء </h5>
        <center>   <p class="card-text">النساء الأرامل :</p>
        <p class="card-text">النساء المطلقات :</p>
        <p class="card-text">نساء الشهداء :</p>
        </center>
    </div>
    </div>


    </div>
    </div>
    </div>
    {/**********************************3 */}
    <div className="row">

   <div></div>
    <Pie
            data={{
              labels: ["هندسة معلوماتية" , "هندسة عمارة" ],
           
            
            datasets: [{
              label: 'My First Dataset',
              data: [300, 50, 100],
              backgroundColor: [
                'rgb(255, 99, 132)',
                'rgb(54, 162, 235)',
                
              ],
              hoverOffset: 4
            }]
          }}
         
            /> 
   
   {/*         <Pie
            data={{
              labels: ["هندسة معلوماتية" , "هندسة عمارة" ],
           
            
            datasets: [{
              label: 'My First Dataset',
              data: [300, 50, 100],
              backgroundColor: [
                'rgb(255, 99, 132)',
                'rgb(54, 162, 235)',
                
              ],
              hoverOffset: 4
            }]
          }}
          height={400}
          width={400}
            /> 
          </div>
      </div>
     </div>
*/}
     </div>
     </div>
     </div>
    )
}

}
export default stat;