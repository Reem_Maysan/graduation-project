import { Link, Route, Switch, BrowserRouter as Router } from 'react-router-dom';
import Content from './Content/Content';
import PersonalInformation from './StudiesSection/Families/Add/PersonalInformation';
import HealthyInformation from './StudiesSection/Families/Add/HealthyInformation';
import ResidentInformation from './StudiesSection/Families/Add/ResidentInformation';
import OutSalaryInforamation from './StudiesSection/Families/Add/OutSalaryInformation';
import DebtInformation from './StudiesSection/Families/Add/DebtInformation';
import PartnerInformation from './StudiesSection/Families/Add/PartnerInformation';
import PartnerList from './StudiesSection/Families/Add/PartnerList';
import ChildList from './StudiesSection/Families/Add/ChildList';
import ChildrenInformation from './StudiesSection/Families/Add/ChildrenInfo';
import DisplayFamily from './StudiesSection/Families/Display/DisplayPage';
import DisplayAssociation from './StudiesSection/Families/Display/displayAssociation';
import DisplayPerson from "./StudiesSection/Families/Display/DisplayInfo";
import UnAccFam from "./StudiesSection/Families/Display/UnAccFam";
import DisplayRejFam from "./StudiesSection/Families/Display/DisplayRejFam";
//
import SystemLogsPage from './systemLogs/systemLogsPage';
import TabPanel from '../components/Depository/Exchange/exchangeTabs'
import stat from './StudiesSection/statistics/Stat';
import React, { Component } from 'react';

////////////Aids/////////////////
import AllAids from './StudiesSection/aids/AllAids';
import AddFinicialAid from './StudiesSection/aids/AddAids/AddFinancialAid';
import AddConcreteAid from './StudiesSection/aids/AddAids/AddConcreteAid';
import AddMaterialHealthAid from './StudiesSection/aids/AddAids/AddMaterialHealthAid';
import AddFinancialHealthAid from './StudiesSection/aids/AddAids/AddFinancialHealthAid';
import DisplayFinancialAid from './StudiesSection/aids/DisplayAids/DisplayFinancialAid';
import DisplayFinancialHealthAid from './StudiesSection/aids/DisplayAids/DisplayFinancialHealthAid';
import DisplayMaterialAid from './StudiesSection/aids/DisplayAids/DisplayMaterialAid';
import DisplayMaterialHealthAid from './StudiesSection/aids/DisplayAids/DisplayMaterialHealthAid';
import RolesTable from './StudiesSection/Roles/Roles';
import AddFund from './StudiesSection/aids/AddAids/AddFin';
////////////Aids/////////////////

import LogInForm from './LogInForm/LogInForm';

import HomePage from './Home/Home';
import DepositoryList from './Depository/DepositoryList';
import AllStores from './Depository/AllStores/AllStores';
import AddDepository from './Depository/AllStores/addDepository';
import DepositoryDetailes from "./Depository/AllStores/DepositoryDetaies";
import addNewItem from './Depository/AllStores/addNewItem';
import TransformItem from './Depository/AllStores/transformItem';
import viewItem from './Depository/AllStores/viewItem';
import Inventory from './Depository/Inventory/inventory';
import exchangeList from './Depository/Exchange/exchangeList';
import AnnualList from './FundDepartment/Display/yearList';
import MonthlyList from './FundDepartment/Display/monthList';
import MonthlyListDetails from './FundDepartment/Display/monthlyListDetails';
import EdicationSystemList from './EdicationSystem/EdicationSystem';
import CheooseEvent from './EdicationSystem/CoursesAndSubjects/ChooeseEvent';
import getAllEvents from './EdicationSystem/EventAndVications/getAllEvents';
import AddEvent from './EdicationSystem/EventAndVications/addEvent';
import CheooseEventOfStudent from './EdicationSystem/StudentsManagment/ChooeseEventOfStudent';
import AddCourse from './EdicationSystem/CoursesAndSubjects/Courses/addCourse';
import getAllCourses from './EdicationSystem/CoursesAndSubjects/Courses/getAllCourses';
import EventOfSubjects from './EdicationSystem/CoursesAndSubjects/Subjects/ChooeseEventOfSubject';
import AddSubject from './EdicationSystem/CoursesAndSubjects/Subjects/addSubjects';
import getAllInquiries from './EdicationSystem/Inquiry/getAllInquiries';
import getAllParents from './EdicationSystem/Parents/getAllParents';
import getAllComplaints from './EdicationSystem/Complaints/getAllComplaints';
import unAcceptedStudent from './EdicationSystem/StudentsManagment/unAcceptedStudents';
import getAllTeachers from './EdicationSystem/Teachers/getAllTeachers';
import updateCourse from './EdicationSystem/CoursesAndSubjects/Courses/updateCourse';
import getInfoOfEvent from './EdicationSystem/EventAndVications/getInfoOfEvent';
import getInfoOfInquary from './EdicationSystem/Inquiry/getInfoOfInquiry';
import getAllStudents from './EdicationSystem/StudentsManagment/getAllStudents';
import { getInfoOfStudent } from '../constants';
import getInfoOfStudents from './EdicationSystem/StudentsManagment/getInfoOfStudent';
import getInfoOfTeacher from './EdicationSystem/Teachers/getInfoOfTeacher';
import archiveExchangeList from './Depository/Exchange/archiveExchangeList';
import MaterialAids from './Depository/MaterialAids/materialAidsList';
import GetPendingMaterila from './Depository/MaterialAids/getPendingMaterial'; 
import FundHomePage from './FundDepartment/Display/fundHomePage';
import ImportsExports from './FundDepartment/Display/importsExports';
import BaseAccountManagement from './FundDepartment/Display/accountsManagement/accountManagement';
import AccountManagement from './FundDepartment/Display/accountsManagement/accountManagement';
import addInquary from './EdicationSystem/Inquiry/addInquary';

import Pers from './StudiesSection/Families/Add/pers';
import ExchangeTabs from './Depository/Exchange/exchangeTabs';
export default class Routes extends Component {

    render() {

        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={LogInForm} />
                    <Route exact path="/login" component={LogInForm}
                    />

                    <Route exact path="/content" component={HomePage} />
                    <Route exact path="/SystemLogsPage" component={SystemLogsPage} /> 
                    <Route exact path="/displayPerson/" component={DisplayPerson} />
                    <Route exact path="/homePage" component={HomePage} />
                    {/* <Route exact path="/displayUnknownPerson/:id" component={StateOfFam} /> */}

                    <Route exact path="/Store" component={DepositoryList} />
                    <Route exact path="/StudiesSection" component={Content} />
                    <Route exact path="/addPersonalInformation" component={PersonalInformation} />
                    <Route exact path="/addResidentInformation" component={ResidentInformation} />
                    <Route exact path="/addHealthyInformation" component={HealthyInformation} />
                    <Route exact path="/addOutSalaryInforamation" component={OutSalaryInforamation} />
                    <Route exact path="/addDebtInformation" component={DebtInformation} />
                    <Route exact path="/addPartnerList" component={PartnerList} />
                    <Route exact path="/addPartnerInformation" component={PartnerInformation} />
                    <Route exact path="/addChildList" component={ChildList} />
                    <Route exact path="/addCexchangeTabshildrenInformation" component={ChildrenInformation} />
                    <Route exact path="/exchangeTabs" component={TabPanel} />
                    <Route exact path="/addChildrenInformation" component={ChildrenInformation} />
                    <Route exact path="/exchangeTabs" component={ExchangeTabs} />
                    <Route exact path="/displayFamily" component={DisplayFamily} />
                    <Route exact path="/displayAssociation" component={DisplayAssociation} />
                    <Route exact path="/displayRejectedFamily" component={DisplayRejFam} />
                    <Route exact path="/displayUnknowFamily" component={UnAccFam} />
                   //
                    <Route exact path="/roles" component={RolesTable} />

                    /////////AIDS/////////////
                     <Route exact path="/allAids" component={AllAids} />
                    <Route exact path="/addConcreteAid" component={AddConcreteAid} />
                    <Route exact path="/addMaterialHealthAid" component={AddMaterialHealthAid} />
                    <Route exact path="/addFinancialHealthAid" component={AddFinancialHealthAid} />
                    <Route exact path="/getFinicialAid" component={DisplayFinancialAid} />
                    <Route exact path="/getConcreteAid" component={DisplayMaterialAid} />
                    <Route exact path="/getMaterialHealthAid" component={DisplayMaterialHealthAid} />
                    <Route exact path="/getFinancialHealthAid" component={DisplayFinancialHealthAid} />
                    <Route exact path="/allAids" component={AllAids} />
                    <Route exact path="/addFinicialAid" component={AddFund} />
                    /////////AIDS/////////////
                    {/*******************************************stat */}
                    <Route exact path="/stat" component={stat} />
                    {/******************************************************Dipository */}
                    <Route exact path="/allStores" component={AllStores} />
                    <Route exact path="/addStore" component={AddDepository} />
                    <Route exact path="/addNewItem" component={addNewItem} />
                    <Route exact path="/trasItem/:id" component={TransformItem} />
                    <Route exact path="/viewItem/:id" component={viewItem} />
                    <Route exact path="/inventory" component={Inventory} />
                    <Route exact path="/exchange/:id" component={exchangeList} />
                    <Route exact path="/displayStoreDetails/:id" component={DepositoryDetailes} />
                    <Route exact path="/getArchivedList/:id" component={archiveExchangeList} />
                    <Route exact path="/materialAids" component={MaterialAids} />
                    <Route exact path="/getPendingMaterial" component={GetPendingMaterila} />


                    ////////FUNDS///////////

                    <Route exact path="/getAccountManagementPage" component={BaseAccountManagement} />
                    <Route exact path="/getImportsExportsPage" component={ImportsExports} /> 
                    <Route exact path="/getFundHomePage" component={FundHomePage} />
                    <Route exact path="/getFundsMainList" component={AnnualList} /> 
                    <Route exact path="/getMonthlyList" component={MonthlyList} />
                    <Route exact path="/MonthlyListDetails" component={MonthlyListDetails} />

                     ////////FUNDS///////////
                    {/*                     <Route exact path="/" component={AnnualList} />
 */}                     ////////FUNDS///////////


                        {/******************************************************education */}
                    <Route exact path="/edication" component={EdicationSystemList} />
                    <Route exact path="/managecoursesAndSubjects" component={CheooseEvent} />
                    <Route exact path="/eventsAndVications" component={getAllEvents} />
                    <Route exact path="/addEvent" component={AddEvent} />
                    <Route exact path="/managestudents" component={CheooseEventOfStudent} />
                    <Route exact path="/courses" component={getAllCourses} />
                    <Route exact path="/addCourse" component={AddCourse} />
                    <Route exact path="/subjects" component={EventOfSubjects} />
                    <Route exact path="/addSubject" component={AddSubject} />
                    <Route exact path="/inquiries" component={getAllInquiries} />
                    <Route exact path="/addInquary" component={addInquary} />
                    <Route exact path="/managepearents" component={getAllParents} />
                    <Route exact path="/getManagerComplaints" component={getAllComplaints} />
                    <Route exact path="/unAcceptedStudents" component={unAcceptedStudent} />
                    <Route exact path="/manageteacher" component={getAllTeachers} />
                    <Route exact path="/editCourse/:id" component={updateCourse} />
                    <Route exact path="/inquiries" component={getAllInquiries} />
                    <Route exact path="/getFullInfoOfEvent/:id" component={getInfoOfEvent} />
                    <Route exact path="/getFullInfoOfinquary/:id" component={getInfoOfInquary} />
                    <Route exact path="/allStudents" component={getAllStudents} />
                    <Route exact path="/getInfoOfStudent/:id" component={getInfoOfStudents} />
                    <Route exact path="/getInfoOfTeacher/:id" component={getInfoOfTeacher} />
                    <Route exact path="/viewCourse" component={getAllCourses} />
                    {/*     <Route exact path="/addCategory" component={AddCategory} />*/}

                </Switch>
            </Router>

        );
    }
}
