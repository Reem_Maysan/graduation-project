import react, { Component } from 'react';
import axios from 'axios';
import { Table, Button } from 'react-bootstrap';
import '../StudiesSection/Families/Display/DisplayPage';
import { LoopCircleLoading } from 'react-loadingg';
import SideBar from '../SideBar/SideBar';
import { Link } from 'react-router-dom';
import { WindMillLoading } from 'react-loadingg';

import * as myConstClass from '../../constants';
class PendingFamily extends Component {

  constructor(props) {
    super(props);
    this.state = {
      families: []
    }
  }
  componentDidMount() {
    this.refreshList();
  }

  async refreshList() {
    const res = await axios.get(myConstClass.GET_RejectedForms)
    console.log(res.data)
    this.setState({
      families: res.data["data"],
    })
  }

  render() {
    const { families } = this.state;
    return (
      <div>
        <SideBar />
        <div>
          <center>
            <table className="show-table" size="sm" >
              <thead  >
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th>الاسم الثلاثي </th>
                </tr>
              </thead>
              <tbody>
                {(families.length > 0) ? families.map((person, index) => {
                  return (
                    <tr key={index}>
                      <td> {index + 1}</td>
                      <td>
                      <Link to={{
                          pathname: '/displayUnknownPerson/' + person.id,
                          state: {
                            id: person.id
                          }
                        }}>{person.firstName + ' '}{person.fatherName + ' '}{person.lastName + ' '}</Link>
                      </td>
                    </tr>
                  )
                }) : <tr><td colSpan="5">Loading...</td></tr>}
              </tbody>
            </table>
          </center>
        </div>
      </div>
    );
  }
}

export default PendingFamily;