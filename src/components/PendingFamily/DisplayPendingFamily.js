import react, { Component } from 'react';
import axios from 'axios';
import { Table, Button } from 'react-bootstrap';
import SideBar from '../SideBar/SideBar';
import CloudUploadIcon from '@material-ui/icons/CloudUpload';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
//import './UnAcc.css'
import * as myConstClass from '../../constants';

class DisplayPendingFamily extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: props.location.state['id'],
      personalInfo: {},
      healthyInfo: [],
      debtsInfo: [],
      externalSalaryInfo: [],
      residentInfo: {},
      partnersInfo: [],
      childrenInfo: []
    }
  }

  componentDidMount() {
    this.personalInfoGet();
    this.residentInfoGet();
    this.healthyInfoGet();
    this.externalSalaryInfoGet();
    this.debtsInfoGet();
    this.partnersInfoGet();
    this.childrenInfoGet();
  }

  async childrenInfoGet() {
    const res = await axios.get(myConstClass.GET_ChildInfo + this.state.id)
    // console.log(res.data)
    this.setState({
      childrenInfo: res.data['data'],
    })
    console.log('childrenInfo')
    console.log(this.state.childrenInfo)
  }
  async partnersInfoGet() {
    const res = await axios.get(myConstClass.GET_WifeInfo + this.state.id)

    // console.log(res.data)
    this.setState({
      partnersInfo: res.data['data'],
    })
    console.log('partnersInfo')
    console.log(this.state.partnersInfo)
  }
  async healthyInfoGet() {
    const res = await axios.get(myConstClass.GET_HealthyInfo + this.state.id)
    // console.log(res.data)
    this.setState({
      healthyInfo: res.data['data'],
    })
    console.log('healthyInfo')
    console.log(this.state.healthyInfo)
  }
  async personalInfoGet() {
    const res = await axios.get(myConstClass.GET_PersonalInfo + this.state.id)
    //    console.log(res.data)
    this.setState({
      personalInfo: res.data['data'],
    })
    console.log('personalInfo')
    console.log(this.state.personalInfo)
  }
  async debtsInfoGet() {
    const res = await axios.get(myConstClass.GET_DeptsInfo + this.state.id)
    //  console.log(res.data)
    this.setState({
      debtsInfo: res.data['data'],
    })
    console.log('debtsInfo')
    console.log(this.state.debtsInfo)
  }
  async externalSalaryInfoGet() {
    const res = await axios.get(myConstClass.GET_MonthlySalaryInfo + this.state.id)
    //   console.log(res.data)
    this.setState({
      externalSalaryInfo: res.data['data'],
    })
    console.log('externalSalaryInfo')
    console.log(this.state.externalSalaryInfo)
  }
  async residentInfoGet() {
    const res = await axios.get(myConstClass.GET_ResidenceInfo + this.state.id)
    //   console.log(res.data)
    this.setState({
      residentInfo: res.data['data'],
    })
    console.log('residentInfo')
    console.log(this.state.residentInfo)
  }

  render() {/* 

    console.log('//////////-----------/////////////')
    console.log(this.state.id)
    console.log('//////////-----------/////////////') */

    return (

      <div>
        <SideBar />
        <div className="rec-contain">
          <center> <h6>المعلومات الشخصية</h6></center>

          <div className="column border-left">
            <div >
              <div className="form-row r">
                <label className="label-dis" > الاسم الأول  :</label>
                <input className="show-input" placeholder={this.state.personalInfo['firstName']} disabled />
              </div>


              <div className="form-row r">
                <label className="label-dis" >  اسم الأب  :</label>
                <input className="show-input" placeholder={this.state.personalInfo['fatherName']} disabled />
              </div>

              <div className="form-row r">
                <label className="label-dis" > الكنية:</label>
                <input className="show-input" placeholder={this.state.personalInfo['lastName']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" > اسم الأم :</label>
                <input className="show-input" placeholder={this.state.personalInfo['motherFirstName']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" > كنية الأم  :</label>
                <input className="show-input" placeholder={this.state.personalInfo['motherLastName']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" > مكان الولادة :</label>
                <input className="show-input" placeholder={this.state.personalInfo['birthPlace']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" >  الجنس :</label>
                <input className="show-input" placeholder={this.state.personalInfo['gender']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" >   رقم الهوية  :</label>
                <input className="show-input" placeholder={this.state.personalInfo['IDN']} disabled />
              </div>

              <div className="form-row r">
                <label className="label-dis" > الحالة الاجتماعية:</label>
                <input className="show-input" placeholder={this.state.personalInfo['maritalStatus']} disabled />
              </div>


            </div>
          </div>
          <div className="column border-left">
            <div >
              <div className="form-row r">
                <label className="label-dis" > الجنسية  :</label>
                <input className="show-input" placeholder={this.state.personalInfo['nationality']} disabled />
              </div>

              <div className="form-row r">
                <label className="label-dis" > تاريخ الميلاد :</label>
                <input className="show-input" placeholder={this.state.personalInfo['birthDate']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" > الالتزام الأخلاقي  :</label>
                <input className="show-input" placeholder={this.state.personalInfo['ethicalCommitment']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" > العنوان  :</label>
                <input className="show-input" placeholder={this.state.personalInfo['address']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" >  تاريخ العائلة :</label>
                <input className="show-input" placeholder={this.state.personalInfo['familyHistory']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" > العمل الحالي  :</label>
                <input className="show-input" placeholder={this.state.personalInfo['currentWork']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" > عنوان العمل الحالي  :</label>
                <input className="show-input" placeholder={this.state.personalInfo['currentWorkAddress']} disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" >  هاتف العمل الحالي :</label>
                <input className="show-input" placeholder={this.state.personalInfo['currentWorkPhone']} disabled />
              </div>
            </div>
          </div>
        </div>
        <br />
        <br />
        <div className="rec-contain">
          <center> <h6>معلومات السكن</h6></center>

          <div className="column border-left">
            <div >
              <div className="form-row r">
                <label className="label-dis" >  حالة العقار    :</label>
                <input className="show-input" disabled />
              </div>


              <div className="form-row r">
                <label className="label-dis" >  الآجار الشهري  :</label>
                <input className="show-input" disabled />
              </div>

              <div className="form-row r">
                <label className="label-dis" >  مكان المنزل  :</label>
                <input className="show-input" disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" >  عدد الغرف :</label>
                <input className="show-input" disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" >  نوع المنزل  :</label>
                <input className="show-input" disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" >الرطوبة  :</label>
                <input className="show-input" disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" >  التنجيد :</label>
                <input className="show-input" disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" >     حالة الكسوة     :</label>
                <input className="show-input" disabled />
              </div>

            </div>
          </div>
          <div className="column border-left">
            <div >
              <div className="form-row r">
                <label className="label-dis" >  رقم المنزل  :</label>
                <input className="show-input" disabled />
              </div>

              <div className="form-row r">
                <label className="label-dis" >  مالك المنزل    :</label>
                <input className="show-input" disabled />
              </div>
              <div className="form-row r">
                <label className="label-dis" > ملاحظات   :</label>
                {/*   <textarea className="show-input" disabled={true} /> */}
              </div>


            </div>
          </div>
        </div>

        <br />
        <br />
        <div className="rec-contain">

          <center> <h6>معلومات الصحية</h6>
            <table size="sm" >
              <thead  >
                <tr>
                  <th>  المشكلة الصحية </th>
                  <th> الطبيب المعالج </th>
                  <th> العلاج </th>
                  <th>  التكلفة الشهرية</th>
                  <th>تكلفة الدواء   </th>
                  <th> ملاحظات </th>
                </tr>
              </thead>
              <tbody>
                {(this.state.healthyInfo.length > 0) ? this.state.healthyInfo.map((info, index) => {
                  return (
                    <tr key={index}>
                      <td> {info['healthProblem']}  </td>
                      <td> {info['physician']}   </td>
                      <td> {info['therapy']}  </td>
                      <td> {info['monthlyCost']}  </td>
                      <td> {info['medicineCost']}   </td>
                      <td> {info['notes']}  </td>
                    </tr>

                  )
                }) : <tr><td colSpan="5">Loading...</td></tr>}
              </tbody>
            </table>
          </center>
        </div>
        <br />
        <br />
        <div className="rec-contain">

          <center> <h6>معلومات الراتب الخارجي </h6>
            <table size="sm" >
              <thead  >
                <tr>
                  <th> القيمة </th>
                  <th> المصدر </th>
                </tr>
              </thead>
              <tbody>
                {(this.state.externalSalaryInfo.length > 0) ? this.state.externalSalaryInfo.map((info, index) => {
                  return (
                    <tr>
                      <td> {info['salaryValue']}  </td>
                      <td> {info['source']}   </td>
                    </tr>

                  )
                }) : <tr><td colSpan="5">Loading...</td></tr>}
              </tbody>

            </table>
          </center>
        </div>
        <br />
        <br />
        <div className="rec-contain">

          <center> <h6>معلومات  الدين  </h6>
            <table size="sm" >
              <thead  >
                <tr>
                  <th> القيمة </th>
                  <th> المصدر </th>
                </tr>
              </thead>
              <tbody>
                {(this.state.debtsInfo.length > 0) ? this.state.debtsInfo.map((info, index) => {
                  return (
                    <tr key={index}>
                      <td> {info['deptValue']}  </td>
                      <td> {info['source']}   </td>
                    </tr>

                  )
                }) : <tr><td colSpan="5">Loading...</td></tr>}
              </tbody>

            </table>
          </center>
        </div>

        <br />
        <br />
        <div className="rec-contain">
          <center>
            <h6>معلومات  الشريك  </h6>
            <table className="table-t table-bordered">
              <thead>
                <tr>
                  <th scope="col">الرقم</th>
                  <th scope="col"> اسم الشريك</th>
                </tr>

              </thead>
              <tbody>
                {(this.state.healthyInfo.length > 0) ? this.state.healthyInfo.map((info, index) => {
                  return (
                    <tr key={index}>
                      <td> {info['healthProblem']}  </td>
                      <td> {info['physician']}   </td>
                      <td> {info['therapy']}  </td>
                      <td> {info['monthlyCost']}  </td>
                      <td> {info['medicineCost']}   </td>
                      <td> {info['notes']}  </td>
                    </tr>

                  )
                }) : <tr><td colSpan="5">Loading...</td></tr>}
              </tbody>
            </table>


          </center>

        </div>

        <br />
        <br />
        <div className="rec-contain">

          <center>
            <h6>معلومات  الأطفال   </h6>
            <table className="table-t table-bordered">

              <thead>
                <tr>
                  <th scope="col">الرقم</th>
                  <th scope="col"> اسم الطفل</th>

                </tr>

              </thead>
              <tbody>

              </tbody>
            </table>

          </center>
      
        </div>
        <br />
        <br />
        <center>
        <button className="accept-butt"  >قبول</button>
             <button className="rejected-butt" >رفض</button>
             </center>
             <br/>
             <br/>
      </div>

    )
  }
}

export default DisplayPendingFamily;