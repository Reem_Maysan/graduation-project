import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Checkbox from "@material-ui/core/Checkbox";

import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../constants';
import '../../Routes';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Button } from "@material-ui/core";
import { TrainRounded } from "@material-ui/icons";

import EditIcon from '@material-ui/icons/Edit';

function LeviesList() {
    const [pageInfo, setPageInfo] = useState({
        loading: false,
        jsonBody: {
            leviesList: []
        }
    });

    const leviesListInfoGet = async () => {
        console.log('leviesListInfoGet leviesListInfoGet leviesListInfoGet')
        setPageInfo({ ...pageInfo, loading: true });
        const res = await axios.get(myConstClass.GET_AllPermissions)
        console.log('json data')
        console.log(res.data['data'])
        setPageInfo({ ...pageInfo, loading: false, jsonBody: res.data['data'] });
    }

    useEffect(() => {
        console.log('useEffect')
        leviesListInfoGet();
    }, []);

}
export default LeviesList;