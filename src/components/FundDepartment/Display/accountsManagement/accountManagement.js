import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import "./accountManag.css";
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Checkbox from "@material-ui/core/Checkbox";
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../../constants';
import '../../../Routes';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Divider } from "@material-ui/core";

import { SnackbarProvider, useSnackbar } from 'notistack';
import Button from "@material-ui/core/Button";

import { TrainRounded } from "@material-ui/icons";
import EditIcon from '@material-ui/icons/Edit';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';

// import Card from '@material-ui/core/Card';
// import CardContent from '@material-ui/core/CardContent';
// import CardHeader from '@material-ui/core/CardHeader';
// import CardActionArea from '@material-ui/core/CardActionArea';
// import CardActions from '@material-ui/core/CardActions';
// import CardMedia from '@material-ui/core/CardMedia';
// import Image from '@material-ui/core/ImageListItem';
// import Icon from '@material-ui/core/Icon';

import SideBar from '../../../SideBar/SideBar';

import TextField from '@material-ui/core/TextField';
import { Card, Icon, Image } from 'semantic-ui-react';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            item="tabpanel"
            hidden={value !== index}
            id={`nav-tabpanel-${index}`}
            aria-labelledby={`nav-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    };
}

function LinkTab(props) {
    return (
        <Tab
            component="a"
            onClick={(event) => {
                event.preventDefault();
            }}
            {...props}
        />
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

function BaseAccountManagement(props) {

    const [pageInfo, setPageInfo] = useState({
        loading: false,
        token: localStorage.getItem('token'),
        jsonBodyAdd: {
            name: "",
            baseAmount: 0
        },
        jsonBodyGet: {
            data: [
                {
                    id: 0,
                    name: "",
                    baseAmount: "0",
                    created_at: "2021-08-13T00:00:00.000000Z"
                }
            ]
        },
    });

    useEffect(() => {
        console.log('useEffect')
        console.log(localStorage.getItem('token'))
        AccountsInfoGet()
    }, []);


    const classes = useStyles();
    const [value, setValue] = useState(0);
    const { enqueueSnackbar } = useSnackbar();

    const handleClick = () => {
        enqueueSnackbar('I love snacks.');
    };

    const handleClickVariant = (variant) => () => {
        // variant could be success, error, warning, info, or default
        enqueueSnackbar('تمت إضافة الحساب بنجاح!', { variant });
    };

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    const handleInputChange = (event) => {
        console.log('hiiii from handleImports')
        const newJsonBody = pageInfo.jsonBodyAdd;
        newJsonBody[event.target.id] = event.target.value;
        setPageInfo({ ...pageInfo, jsonBodyAdd: newJsonBody })
        console.log(pageInfo)
    }
    const handleAddAccount = (e) => {
        e.preventDefault();
        console.log('\\\\\\\\\\\\handleAddAccount//////////')
        console.log(pageInfo.jsonBodyAdd)
        console.log(myConstClass.ADD_Account)
        setPageInfo({
            ...pageInfo,
            loading: true
        });

        fetch(myConstClass.ADD_Account
            , {
                method: 'POST',
                body: JSON.stringify(
                    pageInfo.jsonBodyAdd
                ),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.log(json)
                // handleClickVariant('success')
                // props.history.push({
                //     pathname: '/getFundsMainList',
                //     state: {
                //         /* 
                //         id: debtsState.id,
                //         edit: debtsState.edit,
                //         IDN: debtsState.jsonBody.IDN,
                //         token: debtsState.token */
                //     }
                // })
                //setOpen(false)
            });
        setPageInfo({
            ...pageInfo,
            loading: false,
        });


    };
    const AccountsInfoGet = async () => {
        console.log('AccountsInfoGet AccountsInfoGet AccountsInfoGet')
        setPageInfo({ ...pageInfo, loading: true });
        const res = await axios.get(myConstClass.GET_AllAccounts)
        console.log('json data')
        console.log(res.data['data'])
        setPageInfo({ ...pageInfo, loading: false, jsonBodyGet: res.data });
    }
    return (
        <div>
            {
                pageInfo.loading === true ?
                    <Backdrop
                        open>
                        <CircularProgress color="inherit" />
                    </Backdrop> :

                    <div className={classes.root}>
                        <SideBar />
                        <AppBar position="static">
                            <Tabs
                                variant="fullWidth"
                                value={value}
                                onChange={handleChange}
                                aria-label="nav tabs example"
                            >
                                <LinkTab label="إضافة حساب" href="/drafts" {...a11yProps(0)} />
                                <LinkTab label="استعراض الحسابات" href="/trash" {...a11yProps(1)} />
                            </Tabs>
                        </AppBar>
                        <TabPanel value={value} index={0}>
                            <center>
                                <Card >
                                    <Card.Content className="content">
                                        <h5>نموذج إضافة حساب جديد </h5>
                                        <Divider ></Divider>
                                        <br />
                                        <Card.Description>
                                            <TextField
                                                autoFocus
                                                margin="dense"
                                                id="name"
                                                align="right"
                                                label="اسم الحساب"
                                                type="text"
                                                fullWidth
                                                onChange={e => handleInputChange(e)}
                                            />
                                            <TextField
                                                inputProps={{ min: 0, style: { textAlign: 'right' } }}
                                                autoFocus
                                                margin="dense"
                                                id="baseAmount"
                                                label="قيمة المبلغ المالي"
                                                type="text"
                                                fullWidth
                                                onChange={e => handleInputChange(e)}
                                            />
                                            <br />
                                            <br />
                                            <Button
                                            className="ReemButton"
                                                variant="outlined"
                                                onClick={e => handleAddAccount(e)} 
                                                color="primary">
                                                إضافة
                                            </Button>
                                        </Card.Description>
                                    </Card.Content>
                                </Card>
                            </center>
                        </TabPanel>

                        <TabPanel value={value} index={1}>
                            <div>
                                {pageInfo.loading === true ?
                                    <Backdrop
                                        open>
                                        <CircularProgress color="inherit" />
                                    </Backdrop> :
                                    <center>
                                        {pageInfo.loading === true ?
                                            <Backdrop open>
                                                <CircularProgress color="inherit" />
                                            </Backdrop> :
                                            <div>
                                                <Box marginRight={30} marginTop={5}>
                                                    <TableContainer component={Paper} >
                                                        <Table aria-label="collapsible table" >
                                                            <TableHead >
                                                                <TableRow >
                                                                    <TableCell align="center">الرقم التسلسلي</TableCell>
                                                                    <TableCell align="center">اسم الحساب</TableCell>
                                                                    <TableCell align="center">رصيد الحساب</TableCell>
                                                                    <TableCell align="center">تاريخ إنشاء الحساب</TableCell>
                                                                </TableRow>
                                                            </TableHead>
                                                            <TableBody>
                                                                {Array.isArray(pageInfo.jsonBodyGet.data) ?
                                                                    pageInfo.jsonBodyGet.data.map((item) => (
                                                                        <TableRow key={item.id}>
                                                                            <TableCell component="th" scope="row" align="center">
                                                                                {item.id}
                                                                            </TableCell>
                                                                            <TableCell align="center">{item.name}</TableCell>
                                                                            <TableCell align="center">{item.baseAmount}</TableCell>
                                                                            <TableCell align="center">
                                                                                {Date.parse(item.created_at)}
                                                                                {/* {Intl.DateTimeFormat('en-US', 
                                                                    {year: 'numeric', month: '2-digit',day: '2-digit', 
                                                                    hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(item.created_at)} */}
                                                                            </TableCell>
                                                                        </TableRow>)) :
                                                                    <TableRow>
                                                                        <TableCell component="th" scope="row" align="center">
                                                                            1
                                                    </TableCell>
                                                                        <TableCell align="center">لا يوجد</TableCell>
                                                                        <TableCell align="center">لا يوجد</TableCell>
                                                                        <TableCell align="center">لا يوجد</TableCell>
                                                                    </TableRow>
                                                                }
                                                            </TableBody>
                                                        </Table>
                                                    </TableContainer>
                                                </Box>
                                            </div>}
                                    </center>
                                }</div>
                        </TabPanel>
                    </div>
            }
        </div>
    );
}

export default function AccountManagement(props) {
    return (
        <SnackbarProvider maxSnack={3}>
            <BaseAccountManagement params={props} />
        </SnackbarProvider>
    );

}
