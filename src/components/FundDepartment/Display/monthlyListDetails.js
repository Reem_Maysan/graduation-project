import React, { useState, useEffect } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Checkbox from "@material-ui/core/Checkbox";

import { Link } from 'react-router-dom';
import axios from 'axios';
import * as myConstClass from '../../../constants';
import '../../Routes';

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Button } from "@material-ui/core";
import { TrainRounded } from "@material-ui/icons";

import EditIcon from '@material-ui/icons/Edit';



function MonthlyListDetails() {

    const [monthlyListDetails, setMonthlyListDetails] = useState({
        loading: false,
        currentPage: "Imports",
    });

    const [displayImports, setDisplayImports] = useState({
        loading: false,

    });

    const [displayExports, setDisplayExports] = useState({
        loading: false,

    });

    const importsInfoGet = async () => {
        console.log('importsInfoGet importsInfoGet importsInfoGet')
        setDisplayImports({ ...displayImports, loading: true });
        //  const res = await axios.get(myConstClass.GET_ImportsFinancialHealthyAidFamilies)
        //  console.log('json data')
        //console.log(res.data['data'])
        //setDisplayImports({ ...displayImports, loading: false, jsonBody: res.data['data'] });
    }
    const exportsInfoGet = async () => {
        console.log('exportsInfoGet exportsInfoGet exportsInfoGet')
        setDisplayExports({ ...displayExports, loading: true });
        //     const res = await axios.get(myConstClass.GET_ExportsFinancialHealthyAidFamilies)
        //     console.log('json data')
        //     console.log(res.data['data'])
        //     setDisplayExports({ ...displayExports, loading: false, jsonBody: res.data['data'] });
        // 
    }
    useEffect(() => {
        console.log('useEffect')
        importsInfoGet();
        exportsInfoGet();
    }, []);

    const handleChange = (event) => {
        console.log('hiiii from handleChange')
        setMonthlyListDetails({ ...monthlyListDetails, currentPage: event.target.value })
        console.log(monthlyListDetails)
    }

    return (
        <center><br /> <br />
            <div>
                <input type="radio" name="Imports"
                    value="Imports"
                    checked={monthlyListDetails.currentPage === "Imports"}
                    onChange={(e) => handleChange(e)} id="currentPage"
                />
                <label className="form-check-label" > الواردات </label>
                        &nbsp; &nbsp; &nbsp;
                <input type="radio" name="Exports"
                    value="Exports"
                    checked={monthlyListDetails.currentPage === "Exports"}
                    onChange={(e) => handleChange(e)} id="currentPage"
                />
                <label className="form-check-label"  > الصادرات </label>
                <br />
                <br />
            </div>
            {
                monthlyListDetails.currentPage === "Imports" ?
                    displayImports.loading === true ?
                        <Backdrop open>
                            <CircularProgress color="inherit" />
                        </Backdrop> :
                        <center>
                            <Box marginLeft={10} marginRight={10} marginTop={5}>
                                <TableContainer component={Paper} >
                                    <Table aria-label="collapsible table" >
                                        <TableHead >
                                            <TableRow >{/* 
                                                <TableCell align="center">رقم</TableCell>
                                                <TableCell align="center">وصف المساعدة</TableCell> */}
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                /* Array.isArray(displayImportsAid.ImportsJsonBody['data']) ?
                                                    displayImportsAid.ImportsJsonBody['data'].map((fainancialAid, index) => (
                                                        <TableRow key={index}>
                                                            <TableCell align="center">{index + 1}</TableCell>
                                                            <TableCell align="center">{fainancialAid.aidDescription}</TableCell>
                                                        </TableRow>
                                                    )) :
                                                    <TableRow >
                                                        <TableCell align="center">1</TableCell>
                                                        <TableCell align="center">لا يوجد</TableCell>
                                                    </TableRow> */
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Box>
                        </center> :
                    displayExports.loading === true ?
                        <Backdrop open>
                            <CircularProgress color="inherit" />
                        </Backdrop> :
                        <center>
                            <Box marginLeft={10} marginRight={10} marginTop={5}>
                                <TableContainer component={Paper} >
                                    <Table aria-label="collapsible table" >
                                        <TableHead >
                                            <TableRow >{/* 
                                                <TableCell align="center">رقم المساعدة</TableCell>
                                                <TableCell align="center">وصف المساعدة</TableCell> */}
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {{/* 
                                                Array.isArray(displayExportsAid.ExportsJsonBody['data']) ?
                                                    displayExportsAid.ExportsJsonBody['data'].map((fainancialAid, index) => (
                                                        <TableRow key={index}>
                                                            <TableCell align="center">{index + 1}</TableCell>
                                                            <TableCell align="center">{fainancialAid.aidDescription}</TableCell>
                                                        </TableRow>
                                                    )) :
                                                    <TableRow >
                                                        <TableCell align="center">1</TableCell>
                                                        <TableCell align="center">لا يوجد</TableCell>
                                                    </TableRow> */}
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Box>
                        </center>
            }
        </center>
    );
}
export default MonthlyListDetails;