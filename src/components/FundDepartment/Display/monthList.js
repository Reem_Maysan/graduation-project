import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import * as myConstClass from '../../../constants';

import Box from '@material-ui/core/Box';
import { Link } from 'react-router-dom';
import axios from 'axios';
import '../../Routes';
import React, { useState, useEffect } from "react";

import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';



function MonthlyList(props) {

    const useStyles = makeStyles({
        root: {
            minWidth: 275,
        },
        bullet: {
            display: 'inline-block',
            margin: '0 2px',
            transform: 'scale(0.8)',
        },
        title: {
            fontSize: 14,
        },
        pos: {
            marginBottom: 12,
        },

    });

    const [monthListInfo, setmonthListInfo] = useState({
        loading: false,
        data: [{
            id: 0,
            list_name: "",
        }
        ],
        jsonData: {
            yearListID: props.location.state['yearId'],
            month: 0
        }
    });

    const monthListInfoGet = async () => {
        console.log('monthListInfoGet monthListInfoGet monthListInfoGet')
        var id = monthListInfo.jsonData['yearListID']
        console.log(id)
        setmonthListInfo({ ...monthListInfo, loading: true });
        const res = await axios.get(myConstClass.GET_FundsMonthList + id)
        console.log('json data')
        console.log(res.data['data'])
        setmonthListInfo({ ...monthListInfo, loading: false, data: res.data['data'] });
    }

    useEffect(() => {
        console.log('useEffect')
        console.log(monthListInfo)
        monthListInfoGet();
    }, []);

    const classes = useStyles();

    return (
        <div>{
            monthListInfo.loading === true ?
                <Backdrop open>
                    <CircularProgress color="inherit" />
                </Backdrop> :
                <Box marginLeft={10} marginRight={10} marginTop={5}>
                    
                </Box>
        }
        </div>
    );
}

export default MonthlyList;


/*  <center>
                        <h3 > القوائم الشهرية </h3>
                        {monthListInfo.data.map((item) => (
                            <div className="rows">
                                <div className="col-sm-4">
                                    <div className="card">
                                        <div className="card-body">
                                            <h5 className="card-title">   القائمة السنوية لعام </h5>
                                            <h6 className="card-title">   {item.year} </h6>
                                            <center>
                                                <a class="btn btn-primary" href="/StudiesSection" role="button"> التفاصيل </a>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </center> */