import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import '../../Routes';
import React, { useState, useEffect } from "react";
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Paper from '@material-ui/core/Paper';
import TableContainer from '@material-ui/core/TableContainer';
import axios from 'axios';
import * as myConstClass from '../../../constants';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';

import { Link } from 'react-router-dom';

import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CloseIcon from '@material-ui/icons/Close';


import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';


import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';

const options = [
    'None',
    'Atria',
    'Callisto',
    'Dione',
    'Ganymede',
    'Hangouts Call',
    'Luna',
    'Oberon',
    'Phobos',
    'Pyxis',
    'Sedna',
    'Titania',
    'Triton',
    'Umbriel',
];

const useRowStyles = makeStyles({
    insideHeads: {
        background: 'linear-gradient(45deg, #82E0AA 30%, #82E0AA 90%)',
        border: 0,
        borderRadius: 3,
        color: 'white',
    },
    head: {
        background: 'linear-gradient(45deg, #FFFFFF 30%, #FFFFFF 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px ',
        color: 'white',
    },
    root: {
        background: 'linear-gradient(45deg, #C4DBFF 30%,  #66A2FF 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px ',
        color: 'white',
        height: 48,
        padding: '0 30px',
        '& > *': {
            borderBottom: 'unset',
        },
    },
});

function Row(props) {
    const { row } = props;
    const [open, setOpen] = useState(false);
    const classes = useRowStyles();

    const [exportsInfo, setExportsInfo] = useState({
        loading: false,
        jsonData: {
            exportedData: 0,
            direction: 'لا أحد'
        },
    });

    const [importsInfo, setImportsInfo] = useState({
        loading: false,
        jsonData: {
            importedData: 0,
            direction: 'لا أحد'
        },
    });

    const [monthListInfo, setmonthListInfo] = useState({
        loading: false,
        //  parentState: row.state,
        data: [{
            id: 0,
            list_name: "",
        }
        ],
        jsonData: {
            yearListID: row.id,
            month: 0
        },
        // parentLoading: props.parent.loading

    });

    const monthListInfoGet = async (yearIdProp) => {
        console.log('monthListInfoGet monthListInfoGet monthListInfoGet')
        console.log(yearIdProp)
        console.log(monthListInfo.parentLoading)
        const res = await axios.get(myConstClass.GET_FundsMonthList + yearIdProp)
        console.log('json data')
        console.log(res.data['data'])
        setmonthListInfo({ ...monthListInfo, loading: false, data: res.data['data'] });
    }
    const handleAddMonth = (e) => {
        e.preventDefault();
        console.log('\\\\\\\\\\\\handleAddMonth//////////')
        console.log(monthListInfo.jsonData)
        console.log(myConstClass.ADD_MonthlyList)
        setmonthListInfo({
            ...monthListInfo,
            loading: true
        });
        fetch(myConstClass.ADD_MonthlyList
            , {
                method: 'POST',
                body: JSON.stringify(
                    monthListInfo.jsonData
                ),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.log(json)
                // setmonthListInfo({
                //     ...monthListInfo,
                //     loading: false,
                // });
                monthListInfoGet(monthListInfo.jsonData.yearListID)
                setOpenDialog(false);

            });
    };

    const [openDialog, setOpenDialog] = useState(false);// add new month list dialog


    const handleClickOpen = () => {
        setOpenDialog(true);
    };
    const handleClose = () => {
        setOpenDialog(false);
    };

    // when we add new month, text field handler
    const changeMonth = (event) => {
        console.log('hiiii from changeMonth')
        const newJsonBody = monthListInfo.jsonData;
        newJsonBody[event.target.id] = Number(event.target.value);
        setmonthListInfo({ ...monthListInfo, jsonData: newJsonBody })
        console.log(monthListInfo)
    }

    return (
        <React.Fragment >
            <TableRow className={classes.root} >
                <TableCell align="center">{row.id}</TableCell>
                <TableCell align="center">{row.year}</TableCell>
                <TableCell align="center">
                    <IconButton aria-label="expand row" size="small" onClick={async () => {
                        monthListInfoGet(row.id);
                        setOpen(!open)
                    }}>
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
            </TableRow>
            <TableRow className={classes.head}>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <br />
                        <Typography variant="h6" gutterBottom component="div" align="right">
                            القوائم الشهرية للسنة الحالية:
              </Typography>
                        <br />
                        <Box margin={1}>
                            <Table size="small" aria-label="purchases" >
                                <TableHead>
                                    <TableRow className={classes.insideHeads}>
                                        <TableCell align="center" >الرقم التسلسلي للشهر</TableCell>
                                        <TableCell align="center"  >الشهر</TableCell>
                                        <TableCell align="center"  >التفاصيل</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody align="right"  >
                                    {Array.isArray(monthListInfo.data) ?
                                        monthListInfo.data.map((item, index) => (
                                            <TableRow key={item.id}>
                                                <TableCell component="th" scope="row" align="center">
                                                    {index + 1}
                                                </TableCell>
                                                <TableCell align="center">{item.list_name}</TableCell>
                                                <TableCell align="center">
                                                    <Link
                                                        color="primary"
                                                        role="button"
                                                        to={{
                                                            pathname: '/getImportsExportsPage',
                                                            state: {
                                                                token: localStorage.getItem('token'),
                                                                monthId: item.id,
                                                                yearId: row.id,
                                                                year: row.year

                                                            }
                                                        }}> عرض </Link>
                                                </TableCell>
                                            </TableRow>
                                        ))
                                        :
                                        <TableRow>
                                            <TableCell component="th" scope="row" align="center">
                                                لا يوجد
                                            </TableCell>
                                            <TableCell align="center">لا يوجد</TableCell>
                                            <TableCell align="center">لا يوجد</TableCell>
                                        </TableRow>}
                                </TableBody>
                            </Table>
                        </Box>
                        <br />
                        <Button variant="outlined" color="primary" onClick={handleClickOpen} className="ReemButton"
                        paddingTop={5}>
                            إضافة قائمة شهرية </Button>
                        <Dialog open={openDialog} onClose={handleClose}
                            align="right"
                            aria-labelledby="form-dialog-title">
                            <DialogTitle
                                align="center"
                                id="form-dialog-title" dir="auto" pos="">
                                الرجاء إدخال معلومات القائمة الشهرية الجديدة:</DialogTitle>
                            <DialogContent>
                                <TextField
                                    autoFocus
                                    margin="dense"
                                    id="month"
                                    label="ادخل الشهر"
                                    type="text"
                                    fullWidth
                                    onChange={e => changeMonth(e)}
                                />
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose} color="primary" className="ReemButton">
                                    إلغاء </Button>
                                <Button
                                className="ReemButton"
                                    onClick={(event) => handleAddMonth(event)}
                                    color="primary">
                                    إضافة  </Button>
                            </DialogActions>
                        </Dialog>

                        <br /> <br />

                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

function AnnualList() {

    const useStyles = makeStyles({
        root: {
            minWidth: 275,
        },
        bullet: {
            display: 'inline-block',
            margin: '0 2px',
            transform: 'scale(0.8)',
        },
        title: {
            fontSize: 14,
        },
        pos: {
            marginBottom: 12,
        },

    });

    const [yearListInfo, setyearListInfo] = useState({
        loading: false,
        token: localStorage.getItem('token'),
        data: [{
            id: 0,
            year: 0,
        }],
        jsonData: {
            fundID: 40,
            // props.location.state['fundId'],
            year: 0,
        }
    });

    const childLoad = () => {
        console.log('childLoad childLoad childLoad childLoad childLoad')
        console.log(yearListInfo.loading)
        var load = !yearListInfo.loading
        console.log(load)
        setyearListInfo({ ...yearListInfo, loading: load });
        console.log(yearListInfo.loading)
    }

    const yearListInfoGet = async () => {
        console.log('yearListInfoGet yearListInfoGet yearListInfoGet')
        var id = yearListInfo.jsonData['fundID']
        console.log(id)
        setyearListInfo({ ...yearListInfo, loading: true });
        const res = await axios.get(myConstClass.GET_FundsYearsList + id)
        console.log('json data')
        console.log(res.data['data'])
        setyearListInfo({ ...yearListInfo, loading: false, data: res.data['data'] });
    }

    useEffect(() => {
        console.log('useEffect')
        yearListInfoGet();
    }, []);

    const classes = useStyles();

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const changeYear = (event) => {
        console.log('hiiii from changeYear')
        const newJsonBody = yearListInfo.jsonData;
        newJsonBody[event.target.id] = event.target.value;
        setyearListInfo({ ...yearListInfo, jsonBody: newJsonBody })
        console.log(yearListInfo)
    }

    const handleAddYear = (e) => {
        e.preventDefault();
        console.log('\\\\\\\\\\\\handleAddYear//////////')
        setyearListInfo({
            ...yearListInfo,
            loading: true
        });
        fetch(myConstClass.ADD_YearList
            , {
                method: 'POST',
                body: JSON.stringify(
                    yearListInfo.jsonData
                ),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.log(json)
                yearListInfoGet()
                setOpen(false);
            });
        setOpen(false);
    };

    return (
        yearListInfo.loading === true ?
            <Backdrop open>
                <CircularProgress color="inherit" />
            </Backdrop> :
            <center>
                <Box marginLeft={10} marginRight={10} marginTop={5}>
                <h3 className="h4-Reemon"> القوائم السنوية </h3>
                    <br />
                    <TableContainer component={Paper} >
                        <br />

                        <Button variant="outlined" color="primary" onClick={handleClickOpen} className="ReemButton">
                            إضافة قائمة سنوية</Button>
                        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                            <DialogTitle
                                align="right"
                                id="form-dialog-title" dir="auto" pos="">
                                الرجاء إدخال معلومات القائمة السنوية الجديدة:</DialogTitle>
                            <DialogContent>
                                <TextField
                                    autoFocus
                                    margin="dense"
                                    id="year"
                                    label="ادخل السنة"
                                    type="text"
                                    fullWidth
                                    onChange={e => changeYear(e)}
                                />
                            </DialogContent>
                            <DialogActions>
                                <Button onClick={handleClose} color="primary">
                                    إلغاء </Button>
                                <Button
                                    onClick={(event) => handleAddYear(event)}
                                    color="primary">
                                    إضافة  </Button>
                            </DialogActions>
                        </Dialog>
                        <br /> <br />

                        <Table aria-label="collapsible table" >
                            <TableHead >
                                <TableRow >
                                    <TableCell align="center">رقم السنة التسلسلي</TableCell>
                                    <TableCell align="center">السنة</TableCell>
                                    <TableCell align="center">استعراض</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    yearListInfo.data.map((item, index) => (
                                        <Row key={index} row={item}
                                            state={yearListInfo}
                                            token={localStorage.getItem('token')}
                                            parent={childLoad} />
                                    ))
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Box>
            </center>
    );
}

export default AnnualList;
