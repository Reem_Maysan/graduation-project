import { makeStyles, Theme, createStyles, useTheme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import '../../Routes';
import React, { useState, useEffect } from "react";
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Paper from '@material-ui/core/Paper';
import TableContainer from '@material-ui/core/TableContainer';
import axios from 'axios';
import * as myConstClass from '../../../constants';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Divider from '@material-ui/core/Divider';
//import Select from 'react-multiple-select-dropdown-lite'
import MenuItem from "@material-ui/core/MenuItem";
import clsx from 'clsx';

import ListItemText from '@material-ui/core/ListItemText';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CloseIcon from '@material-ui/icons/Close';


import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';


import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';

import { Link } from 'react-router-dom';


import InputLabel from '@material-ui/core/InputLabel';

const options = [
    'عائلات',
    'حسابات',
];

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    margin: {
        margin: theme.spacing(1),
    },
}));

function ImportsExports(props) {

    const classes = useStyles();
    const theme = useTheme();

    const myData = props.location.state;

    const [importsExportsInfo, setImportsExportsInfo] = useState({
        loading: false,
        token: localStorage.getItem('token'),
        monthId: props.location.state['monthId'],
        yearId: props.location.state['yearId'],
        year: props.location.state['year'],
        data: {
            listResult: [
                {
                    transaction: {
                        id: 0,
                        amount: "0",
                        transaction_type: "import",
                        created_at: "2021-08-13T00:00:00.000000Z",
                        account_transaction: [
                            {
                                account_id: 0,
                                account: {
                                    id: 0,
                                    name: ""
                                }
                            }
                        ]
                    },
                    account: {
                        id: 0,
                        name: ""
                    }
                }
            ],
            fundAmount: {
                id: 0,
                amount: "0"
            }
        },
        jsonData: {
        }
    });

    const transactionsListGet = async () => {
        console.log('transactionsListGet transactionsListGet transactionsListGet')
        setImportsExportsInfo({ ...importsExportsInfo, loading: true });
        console.log(importsExportsInfo.monthId)
        const res = await axios.get(myConstClass.GET_ListTransactions + importsExportsInfo.monthId)
        console.log('json data')
        console.log(res.data['data'])
        setImportsExportsInfo({ ...importsExportsInfo, loading: false, data: res.data['data'] });
    }

    const [exportsInfo, setExportsInfo] = useState({
        loading: false,
        directionValue: "عائلات",
        directionSelected: "",
        direction:
        {
            data: [
                {
                    id: 0,
                    name: "",
                    baseAmount: "",
                    created_at: ""
                }
            ],
        },
        jsonData: {
            amount: 0,
            transactionType: "export",
            listID: myData['monthId'],
            accountID: 0,
            type: "",
        },
    });

    const [importsInfo, setImportsInfo] = useState({
        loading: false,
        directionValue: "عائلات",
        directionSelected: "",
        direction:
        {
            data: [
                {
                    id: 0,
                    name: "",
                    baseAmount: "",
                    created_at: ""
                }
            ],
        },
        jsonData: {
            amount: 0,
            transactionType: "import",
            listID: myData['monthId'],
            accountID: 0,
            type: "",
        },
    });

    const [openImportsDialog, setOpenImportsDialog] = React.useState(false);// month's Imports dialog

    const [openExportsDialog, setOpenExportsDialog] = React.useState(false);// month's Exports dialog

    const [openDialog, setOpenDialog] = React.useState(false);// details dialog

    const handleOpenDialog = () => {
        setOpenDialog(true);
    };
    const handleCloseDialog = () => {
        setOpenDialog(false);
    };
    const handleClickOpenImportsDialog = () => {
        setOpenImportsDialog(true);
    };
    const handleCloseImportsDialog = async () => {
        console.log('\\\\\\\\\\\\handleAddYear//////////')
        setImportsExportsInfo({ ...importsExportsInfo, loading: true });
        fetch(myConstClass.ADD_Transaction
            , {
                method: 'POST',
                body: JSON.stringify(
                    importsInfo.jsonData
                ),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json',
                    'Authorization': importsExportsInfo.token
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.log(json)
                transactionsListGet()
                setOpenImportsDialog(false);
            });
    };
    const handleClickOpenExportsDialog = () => {
        setOpenExportsDialog(true);
    };
    const handleCloseExportsDialog = () => { 
        console.log('\\\\\\\\\\\\handleAddYear//////////')
        setImportsExportsInfo({ ...importsExportsInfo, loading: true });
        fetch(myConstClass.ADD_Transaction
            , {
                method: 'POST',
                body: JSON.stringify(
                    exportsInfo.jsonData
                ),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json',
                    'Authorization': importsExportsInfo.token
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.log(json)
                transactionsListGet()
                setOpenExportsDialog(false);
            });
    };
    const handleExports = (event) => {
        console.log('hiiii from handleExports')
        const newJsonBody = exportsInfo.jsonData;
        newJsonBody[event.target.id] = event.target.value;
        setExportsInfo({ ...exportsInfo, jsonData: newJsonBody })
        console.log(exportsInfo)
    }
    const handleImports = (event) => {
        console.log('hiiii from handleImports')
        const newJsonBody = importsInfo.jsonData;
        newJsonBody[event.target.id] = event.target.value;
        setImportsInfo({ ...importsInfo, jsonData: newJsonBody })
        console.log(importsInfo)
    }
    const handleSelectChange = async (event) => {
        console.log('hiiii from handleSelectChange')
        if ([event.target.value] == 'حسابات') {
            console.log('حسابات')
            const res = await axios.get(myConstClass.GET_AllAccounts)
            console.log('json data')
            console.log(res.data['data'])
            //imports
            const val = importsInfo.direction
            const list = res.data['data']
            val.data = [...list]
            const newJsonBody = importsInfo.jsonData;
            newJsonBody['type'] = "accounts";
            setImportsInfo({ ...importsInfo, direction: val, jsonData: newJsonBody });
            console.log(importsInfo.direction)
            //exports
            const val2 = exportsInfo.direction
            const list2 = res.data['data']
            val2.data = [...list2]
            const newJsonBody2 = exportsInfo.jsonData;
            newJsonBody2['type'] = "accounts";
            setExportsInfo({ ...exportsInfo, direction: val2, jsonData: newJsonBody2 });
            console.log(exportsInfo.direction)
        } else if ([event.target.value] == 'عائلات') {
            console.log('عائلات')
            //imports
            const newJsonBody = importsInfo.jsonData;
            newJsonBody['type'] = "forms";
            setImportsInfo({ ...importsInfo, jsonData: newJsonBody });
            //exports 
            const newJsonBody2 = exportsInfo.jsonData;
            newJsonBody2['type'] = "forms";
            setExportsInfo({ ...exportsInfo, jsonData: newJsonBody2 });
        }
        setImportsInfo({ ...importsInfo, directionValue: [event.target.value] })
        setExportsInfo({ ...exportsInfo, directionValue: [event.target.value] })
        console.log(importsInfo)
        console.log(exportsInfo)
    }
    useEffect(() => {
        console.log('useEffect')
        transactionsListGet()
        setImportsInfo({ ...importsInfo, directionValue: "عائلات" })
    }, []);

    return (
        importsExportsInfo.loading === true ?
            <Backdrop open>
                <CircularProgress color="inherit" />
            </Backdrop> :
            <center>
                <Box marginLeft={10} marginRight={10} marginTop={5}>
                    <h3 > قائمة سنة {importsExportsInfo.year} الشهر {importsExportsInfo.monthId} </h3>
                    <br />
                    <h6>المبلغ الكلي</h6>
                    <h5>{
                        importsExportsInfo.data.fundAmount != null ?
                            importsExportsInfo.data.fundAmount['amount'] :
                            0.0
                    }</h5>
                    <TableContainer component={Paper} >
                        <br />
                        <Button
                            variant="outlined"
                            onClick={handleClickOpenImportsDialog}
                            color="primary">
                            إضافة دفعة مالية  </Button>
                        <Button
                            variant="outlined"
                            onClick={handleClickOpenExportsDialog}
                            color="primary">
                            تصدير دفعة مالية  </Button>
                        <Dialog onClose={handleCloseImportsDialog}
                            aria-labelledby="customized-dialog-title" open={openImportsDialog}>
                            <DialogTitle id="customized-dialog-title"
                                align="right"
                                onClose={handleCloseImportsDialog}>
                                نموذج إضافة دفعة مالية جديدة </DialogTitle>
                            <DialogContent dividers="light">
                                <TextField
                                    align="right"
                                    dir="rtl"
                                    autoFocus
                                    margin="dense"
                                    id="amount"
                                    label="قيمة المبلغ المضاف"
                                    type="text"
                                    fullWidth
                                    onChange={e => handleImports(e)}
                                />
                                <br />
                                <br />
                                <br />
                                <center>
                                    <FormControl variant="outlined" >
                                        <InputLabel id="demo-simple-select-outlined-label">الجهة</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-outlined-label"
                                            id="directionValue"
                                            value={importsInfo.directionValue}
                                            onChange={e => handleSelectChange(e)}
                                            label="الجهة"
                                        >
                                            <MenuItem selected value="عائلات">
                                                عائلات
                                            </MenuItem>
                                            <MenuItem value="حسابات">
                                                حسابات
                                            </MenuItem>
                                        </Select>
                                    </FormControl>
                                    <br />
                                    <br /> <br />
                                    <br />
                                    {
                                        importsInfo.directionValue != "عائلات" ?
                                            //     <FormControl
                                            //     //  className={classes.formControl}
                                            //     >
                                            //         <InputLabel shrink htmlFor="select-multiple-native">
                                            //             الجهات التي يمكن اختيارها
                                            // </InputLabel>
                                            //         <Select
                                            //             multiple
                                            //             native
                                            //             // value={personName}
                                            //             onChange={handleChangeMultiple}
                                            //             inputProps={{
                                            //                 id: 'select-multiple-native',
                                            //             }}
                                            //         >
                                            //             {importsInfo.direction.data.map((item) => (
                                            //                 <option key={item.id} value={item.id}>
                                            //                     {item.name}
                                            //                 </option>
                                            //             ))}
                                            //         </Select>
                                            //     </FormControl>
                                            <FormControl className={classes.margin}>
                                                <InputLabel htmlFor="demo-customized-select-native">الجهة</InputLabel>
                                                <NativeSelect
                                                    id="accountID"
                                                    value={importsInfo.jsonData.accountID}
                                                    onChange={handleImports}
                                                // input={<BootstrapInput />}
                                                >
                                                    {importsInfo.direction.data.map((item) => (
                                                        <option key={item.id} value={item.id}>
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </NativeSelect>
                                            </FormControl>
                                            :
                                            <TextField
                                                align="right"
                                                dir="rtl"
                                                autoFocus
                                                margin="dense"
                                                id="accountID"
                                                label="ادخل الرقم الوطني"
                                                type="text"
                                                fullWidth
                                                onChange={e => handleImports(e)}
                                            />
                                    }
                                </center>
                            </DialogContent>
                            <DialogActions>
                                <Button autoFocus onClick={handleCloseImportsDialog} color="primary">
                                    حفظ التغييرات </Button>
                            </DialogActions>
                        </Dialog>
                        <Dialog onClose={handleCloseExportsDialog}
                            aria-labelledby="customized-dialog-title" open={openExportsDialog}>
                            <DialogTitle id="customized-dialog-title"
                                align="right"
                                onClose={handleCloseExportsDialog}>
                                نموذج تصدير دفعة مالية جديدة </DialogTitle>
                            <DialogContent dividers>
                                <TextField
                                    align="right"
                                    dir="rtl"
                                    autoFocus
                                    margin="dense"
                                    id="amount"
                                    label="قيمة المبلغ الصادر"
                                    type="text"
                                    fullWidth
                                    onChange={e => handleExports(e)}
                                />
                                <br />
                                <br />
                                <br />
                                <center>
                                    <FormControl variant="outlined" >
                                        <InputLabel id="demo-simple-select-outlined-label">الجهة</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-outlined-label"
                                            id="directionValue"
                                            value={exportsInfo.directionValue}
                                            onChange={e => handleSelectChange(e)}
                                            label="الجهة"
                                        >
                                            <MenuItem selected value="عائلات">
                                                عائلات
                                            </MenuItem>
                                            <MenuItem value="حسابات">
                                                حسابات
                                            </MenuItem>
                                        </Select>
                                    </FormControl>
                                    <br />
                                    <br /> <br />
                                    <br />
                                    {
                                        exportsInfo.directionValue != "عائلات" ?
                                            <FormControl className={classes.margin}>
                                                <InputLabel htmlFor="demo-customized-select-native">الجهة</InputLabel>
                                                <NativeSelect
                                                    id="accountID"
                                                    value={exportsInfo.jsonData.accountID}
                                                    onChange={handleExports}
                                                >
                                                    {exportsInfo.direction.data.map((item) => (
                                                        <option key={item.id} value={item.id}>
                                                            {item.name}
                                                        </option>
                                                    ))}
                                                </NativeSelect>
                                            </FormControl>
                                            :
                                            <TextField
                                                align="right"
                                                dir="rtl"
                                                autoFocus
                                                margin="dense"
                                                id="accountID"
                                                label="ادخل الرقم الوطني"
                                                type="text"
                                                fullWidth
                                                onChange={e => handleExports(e)}
                                            />
                                    }
                                </center>
                            </DialogContent>
                            <DialogActions>
                                <Button autoFocus onClick={handleCloseExportsDialog} color="primary">
                                    حفظ التغييرات </Button>
                            </DialogActions>
                        </Dialog>
                        <br /> <br />
                        <Table aria-label="collapsible table" >
                            <TableHead >
                                <TableRow >
                                    <TableCell align="center">رقم المناقلة التسلسلي</TableCell>
                                    <TableCell align="center">نوع المناقلة</TableCell>
                                    <TableCell align="center">المبلغ المالي</TableCell>
                                    <TableCell align="center">التفاصيل</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    Array.isArray(importsExportsInfo.data.listResult) ?
                                        importsExportsInfo.data.listResult.map((item, index) => (
                                            <TableRow key={index}>
                                                <TableCell align="center">{item.transaction.id}</TableCell>
                                                <TableCell align="center">{item.transaction.transaction_type}</TableCell>
                                                <TableCell align="center">{item.transaction.amount}</TableCell>
                                                <TableCell align="center">
                                                    <Button onClick={handleOpenDialog} >
                                                        عرض</Button></TableCell>
                                                <Dialog open={openDialog} onClose={handleCloseDialog}
                                                    align="right"
                                                    aria-labelledby="form-dialog-title">
                                                    <DialogTitle
                                                        align="center"
                                                        id="form-dialog-title" dir="auto" pos="">
                                                        تفاصيل المناقلة المالية</DialogTitle>
                                                    <DialogContent dividers>
                                                        <label className="label">نوع العملية : </label>
                                                        <label>{item.transaction.transaction_type}</label>
                                                        <br />
                                                        <label className="label">تاريخ العملية : </label>
                                                        <label>{item.transaction.created_at}</label>
                                                        <br />
                                                        <label className="label">الجهات المستقبلة: </label>
                                                        {
                                                            Array.isArray(item.transaction.account_transaction) ?
                                                                item.transaction.account_transaction.map((accountItem) => (
                                                                    <div>
                                                                        <label>{accountItem.account.name}</label>
                                                                        <br />
                                                                    </div>
                                                                )) :
                                                                <div>
                                                                    <label>لا يوجد جهات مستقبلة</label>
                                                                    <br /></div>
                                                        }
                                                    </DialogContent>
                                                    <DialogActions>
                                                        <Button onClick={handleCloseDialog} color="primary">
                                                            إغلاق    </Button>
                                                    </DialogActions>
                                                </Dialog>
                                            </TableRow>
                                        )) :
                                        <TableRow>
                                            <TableCell align="center">0</TableCell>
                                            <TableCell align="center">لا يوجد</TableCell>
                                            <TableCell align="center">لا يوجد</TableCell>
                                            <TableCell align="center">لا يوجد</TableCell>
                                        </TableRow>
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Box>
            </center>
    );


}
export default ImportsExports;
