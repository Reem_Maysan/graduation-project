import react from "react";
import TextArea from './Buttons/textArea';
import Input from './Buttons/input';
import Select from "./Buttons/Select";
import RadioButton from './Buttons/RadioButton';
import Checkbox from './Buttons/Checkbox';
import DatePicker from './Buttons/DatePicker'
function FormikControl(props){
   const {control , ...rest} = props
   switch(control){
       case 'input' : return < Input {...rest}/>
       case 'textarea': return < TextArea {...rest}/>
       case 'select' : return < Select {...rest}/>
        case 'radio': return<RadioButton {...rest} />
        case 'checkbox': return<Checkbox {...rest} />
        case 'date' : return<DatePicker {...rest} />
        default : return null

   }
}
export default FormikControl;