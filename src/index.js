import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import axios from 'axios';
import Routes from './components/Routes';
import { Router } from 'react-router';
import 'bootstrap/dist/css/bootstrap.min.css';
import SideBar from './components/SideBar/SideBar';


import * as myConstClass from './constants';

axios.defaults.baseURL = myConstClass.Basic_Auth_Url;
axios.defaults.headers.common['Authorization'] =  'Bearer ' + localStorage.getItem('token');
ReactDOM.render(
  <React.StrictMode>

    <Routes />
    
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
