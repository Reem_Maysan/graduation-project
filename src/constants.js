export const Basic_Url = "https://thawing-everglades-11191.herokuapp.com/api/v1/"
export const Basic_Auth_Url = "https://thawing-everglades-11191.herokuapp.com/api/"



//////////////////////AUTHENTICATION/////////////////////
export const LOG_IN = "login";
export const LOG_OUT = "";


//////////////////////////ADD///////////////////////////
///Studies///
export const ADD_PersonalInfo = Basic_Url + "addForm/personalInfo";
export const ADD_HealthyInfo = Basic_Url + "addForm/healthyInfo";
export const ADD_MonthlySalaryInfo = Basic_Url + "addForm/monthlySalary";
export const ADD_ResidenceInfo = Basic_Url + "addForm/residenceInfo";
export const ADD_DeptsInfo = Basic_Url + "addForm/deptsInfo";
export const ADD_ChildInfo = Basic_Url + "addForm/childrenInfo";
export const ADD_WifeInfo = Basic_Url + "addForm/wifeInfo";
export const ADD_AddRole = Basic_Url + "addRule";
export const Accept_Form = Basic_Url + "acceptForm"; 
export const Reject_Form = Basic_Url + "rejectForm";
//

////Aids////
export const ADD_AddFinancialAid = Basic_Url + "addAid/addFinancialAid";
export const ADD_AddMaterialAid = Basic_Url + "addAid/addMaterialAid";
export const ADD_AddMaterialHealthyAid = Basic_Url + "addAid/addMaterialHealthyAid";
export const ADD_AddFinancialHealthyAid = Basic_Url + "addAid/addFinancialHealthyAid";
export const ADD_RejectFinancialAid = Basic_Url + "getAid/rejectFinancialAid";
export const ADD_AcceptFinancialAid = Basic_Url + "getAid/acceptFinancialAid";
export const ADD_AcceptMaterialAid = Basic_Url + "acceptMaterialAid";
export const ADD_RejectMaterialAid = Basic_Url + "rejectMaterialAid";

////FUNDS////
export const ADD_Fund = Basic_Url + "fundDep/funds/addFund";
export const ADD_YearList = Basic_Url + "fundDep/lists/addYearList";
export const ADD_MonthlyList = Basic_Url + "fundDep/lists/addMonthList";
export const ADD_Transaction = Basic_Url + "fundDep/transactions/addTransactions";
export const ADD_Account = Basic_Url + "fundDep/accounts/addAccount";


//////////////////////////GET///////////////////////////
///Studies///
export const GET_PersonalInfo = Basic_Url + "getForm/getPersonalInfo/";
export const GET_HealthyInfo = Basic_Url + "getForm/getHealthyInfo/";
export const GET_MonthlySalaryInfo = Basic_Url + "getForm/getMonthlySalary/";
export const GET_ResidenceInfo = Basic_Url + "getForm/getResidenceInfo/";
export const GET_DeptsInfo = Basic_Url + "getForm/getDeptsInfo/";
export const GET_ExpensesInfo = Basic_Url + "getForm/getExpensesInfo/";
export const GET_RebuildingInfo = Basic_Url + "getForm/getRebuildingInfo/";
export const GET_ChildInfo = Basic_Url + "getForm/getChildrenInfo/";
export const GET_WifeInfo = Basic_Url + "getForm/getWifeInfo/";
export const GET_WivesInfo = Basic_Url + "getForm/getWives/";
export const GET_RejectedForms = Basic_Url + "getForm/getRejectedForms";
export const GET_AssociationForms = Basic_Url + "getForm/getAssociationForms";
export const GET_GeneralForms = Basic_Url + "getForm/getGeneralForms";
export const GET_PendingForms = Basic_Url + "getForm/getPendingForms"; 
export const GET_AllRoles = Basic_Url + "getRoles";
export const GET_AllPermissions = Basic_Url + "getPermissions";
export const GET_UsersRoles = Basic_Url + "getUsersRoles";
export const GET_SystemLogs = Basic_Url + "getSystemLogs";


//response: data:"no data"
export const GET_ArchiveFinancialAid = Basic_Url + "getAid/getArchiveFinancialAids";
export const GET_ArchiveMaterialAid = Basic_Url + "getAid/getArchiveMaterialAids";

/// response: data:[{lsfkvnkjvnsnv}]
export const GET_PendingMaterialAidFamilies = Basic_Url + "getAid/getPendingMaterialAidFamilies";
//no response
export const GET_PendingFinancialAidFamilies = Basic_Url + "getAid/getPendingFinancialAidFamilies";

//response: data:"no data"
export const GET_PendingMaterialHealthyAidFamilies = Basic_Url + "getAid/getPendingMaterialHealthyAidFamilies";
export const GET_PendingFinancialHealthyAidFamilies = Basic_Url + "getAid/getPendingFinancialHealthyAidFamilies";

//no response
export const GET_ArchiveMaterialHealthyAidFamilies = Basic_Url + "getAid/getArchiveMaterialHealthyAids";
export const GET_ArchiveFinancialHealthyAidFamilies = Basic_Url + "getAid/getArchiveFinancialHealthyAids";

////////FUNDS/////////
export const GET_FundsYearsList = Basic_Url + "fundDep/lists/getYearsLists/";
export const GET_FundsMonthList = Basic_Url + "fundDep/lists/getMonthsList/";
export const GET_AllFunds = Basic_Url + "fundDep/funds/getFunds";
export const GET_FundDetails = Basic_Url + "fundDep/funds/getFundDetails/";
export const GET_ListTransactions = Basic_Url + "fundDep/lists/getListTransactions/";
export const GET_AllAccounts = Basic_Url + "fundDep/accounts/getAccounts";
export const GET_FinancialMovementControl = Basic_Url + "fundDep/accounts/financialMovementControl/";
//fundDep/accounts/addAccount


export const GET_ALL_Warehouses = Basic_Url + "warehouseDep/warehouses/getAllWarehouses";
export const GET_WareHousesDeatailes = Basic_Url + "warehouseDep/warehouses/getWarehouseDetails/";
export const GET_WareHousesDeatailesItem = Basic_Url + "warehouseDep/warehouses/getWareHouseItems/";
export const GET_WareHousesItem = Basic_Url + "warehouseDep/items/getItem/";
export const GET_PendingExchangeLists= Basic_Url + "warehouseDep/exchangeList/getPendingExchangeLists/";
export const GET_ArchiveExchangeLists= Basic_Url + "warehouseDep/exchangeList/getArchivedExchangeLists/";
export const GET_WareHouseCategories = Basic_Url + "warehouseDep/warehouses/getWareHouseCategories/";
export const POST_WareHouseCategories = Basic_Url + "warehouseDep/items/exportItems";
export const POST_AddItemCategories = Basic_Url + "warehouseDep/items/reservingItems";
export const GET_PendingMaterialAid = Basic_Url + "getAid/getPendingMaterialAidFamilies";
export const POST_AcceptMaterialAid = Basic_Url + "acceptMaterialAid"
//////////////////////////EDIT///////////////////////////
export const Edit_PersonalInfo = Basic_Url + "editInfo/editPersonalInfo/";
export const Edit_HealthyInfo = Basic_Url + "editInfo/editHealthyInfo/";
export const Edit_MonthlySalaryInfo = Basic_Url + "editInfo/editMonthlySalaryInfo/";
export const Edit_ResidenceInfo = Basic_Url + "editInfo/editResidenceInfo/";
export const Edit_DeptsInfo = Basic_Url + "editInfo/editDeptsInfo/";
export const Edit_ExpensesInfo = Basic_Url + "editInfo/editExpensesInfo/";
export const Edit_RebuildingInfo = Basic_Url + "editInfo/editRebuildingInfo/";
export const Edit_ChildInfo = Basic_Url + "editInfo/editChildInfo/";
export const Edit_WifeInfo = Basic_Url + "editInfo/editWifeInfo/";
export const Edit_RolePer = Basic_Url + "editRulePermissions";
export const Edit_UserRole = Basic_Url + "editUserRole";

// ROLES
// export const GET_UsersRole = Basic_Url + "getUsersRoles";
// export const GET_AllRoles = Basic_Url + "getRoles";
// export const GET_AllPermissions = Basic_Url + "getPermissions";
  
//////////////////////////Warnings////////////////////////
export const Invalid_Email = "البريد الإلكتروني غير صالح";
export const Invalid_Password = "كلمة المرور يجب ان تكون 6 محارف على الأقل";
export const Less_10 = "عدد المحارف يجب أن يكون 10 محرف أو أقل ";
export const Less_15 = "عدد المحارف يجب أن يكون 15 محرف أو أقل ";
export const Less_20 = "عدد المحارف يجب أن يكون 20 محرف أو أقل ";
export const Less_30 = "عدد المحارف يجب أن يكون 30 محرف أو أقل ";
export const Date_Format = "التاريخ يجب أن يكون من الشكل YYYY-MM-DD";
export const Required = "مطلوب";

/*********************************edication********************************** */
export const UrlEdication = "https://thawing-everglades-11191.herokuapp.com/";
export const educationHref = "api/v1/educationDep/";
export const baseUrlEdication = UrlEdication + educationHref;
export const addCategory = "categories/addCategory";
export const addCategoryOfEducation = baseUrlEdication + addCategory;
export const getCategories = "categories/getCategories/0";
export const getCategoriesOfEducation = baseUrlEdication + getCategories;
export const getCategorieyFullInfoOfEducation  ="categories/getCategoryFullInfo/";
export const getInquiries = "communication/getInquiries";
export const getInquiriesOfEducation = baseUrlEdication + getInquiries;
export const addCourse = "courses/addCourse";
export const addCourseOfEducation = baseUrlEdication + addCourse;
export const addEvent = "communication/addEvent";
export const addEventfEducation = baseUrlEdication + addEvent;
export const getEvent = "communication/getEvents";
export const getEventsOfEducation = baseUrlEdication + getEvent;
export const getEventInfoOfEducation = baseUrlEdication + "communication/getEvent/";
export const getInquaryInfoOfEducation = baseUrlEdication + "communication/getInquiry/";
export const getParent = "parent/getParents" ;
export const getParentsOfEducation = baseUrlEdication + getParent;
export const getComplaint = "communication/getManagerComplaints"
export const getComplaintsOfEducation = baseUrlEdication + getComplaint ;
export const getPendingStudents = "student/getPendingStudents";
export const getPendingStudentsOfEducation = baseUrlEdication + getPendingStudents ; 
export const getTeacher = "teacher/getTeachers";
export const getTeachersOfEducation = baseUrlEdication + getTeacher ; 
export const getSubjects = "courses/getCourses";
export const getSubjectsOfEducation = baseUrlEdication + getSubjects ; 
export const addPaymentOfStudent = baseUrlEdication + "student/addStudentPayment" ; 
export const getAllStudentOfEdication = baseUrlEdication + "student/students" ; 
export const getInfoStudent = baseUrlEdication + "student/browseStudentInfo/";
export const getInfoTeacher = baseUrlEdication + "teacher/getTeacherFullInfo/";
export const postAddInquary = baseUrlEdication + "communication/addEnquiry";